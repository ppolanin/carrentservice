const clientName = document.querySelector('#client-name');
const clientSurname = document.querySelector('#client-surname');
const clientEmail = document.querySelector('#client-email');
const clientLogin = document.querySelector('#client-username');
const clientPassword = document.querySelector('#client-password');
const clientConfirmationPassword = document.querySelector('#client-confirmation-password');
const registerButton = document.querySelector('#client-register-button');
const cancelButton = document.querySelector('#client-register-cancel-button');
const incorrectFieldValue = 'Pole wymagane';
const clientNamePattern = '^[\\p{L}]{2,}$';
const incorrectClientNameMessageId = 'incorrect-client-name';
const incorrectClientNameMessageValue = 'Pole "Imię" powinno zawierać co najmniej dwa znaki alfabetyczne';
const incorrectClientSurnameMessageId = 'incorrect-client-surname';
const incorrectClientSurnameMessageValue = 'Pole "Nazwisko" powinno zawierać co najmniej dwa znaki alfabetyczne';
const clientEmailPattern = '.+@[a-z0-9]+\\.[a-z]{2,}';
const incorrectClientEmailMessageId = 'incorrect-client-email';
const incorrectClientEmailMessageValue = 'Pole "E-mail" ma nieprawidłowy format';
const clientLoginPattern = '^[a-zA-Z\\.\\_\\-\\d]{2,}$';
const incorrectClientLoginMessageId = 'incorrect-client-login';
const incorrectClientLoginMessageValue = 'Pole "Login" powinno zawierać co najmniej 2 znaki alfabetyczne: a-z, A-Z, dopuszczalne znaki numeryczne: 0-9 i specjalne: ".", "_", "-"';
const clientPasswordPattern = '(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*-_])(?=.{8,})';
const incorrectClientPasswordMessageId = 'incorrect-client-password';
const incorrectClientPasswordMessageValue = 'Pole "Hasło" powinno zawierać co najmniej 8 znaków w tym dużą literę i cyfrę, dopuszczalne znaki specjalne: "!", "@", "#", "$", "%", "^", "&", "*", "-", "_"';
const incorrectClientConfirmationPasswordMessageId = 'incorrect-client-confirmation-password';
const incorrectClientConfirmationPasswordMessageValue = 'Pole "Potwierdź Hasło" powinno zawierać co najmniej 8 znaków w tym dużą literę i cyfrę, dopuszczalne znaki specjalne: "!", "@", "#", "$", "%", "^", "&", "*", "-", "_"';
const passwordMismatchMessageId = 'client-password-mismatch';
const passwordMismatchMessageValue = 'Hasła nie są zgodne';
const successClientRegistrationMessage = 'Konto zostało założone';
const existClientLoginMessage = 'Konto o podanym loginie już istnieje';
const clientRegistrationMessageId = 'login message';
const clientRegisterFormId = 'user-register-form';
const registerPopupMessageClassName = 'register-popup-message';

registerButton.addEventListener('click', register);
cancelButton.addEventListener('click', function() {
    location.href = '/main';
});

function register() {
    removeElementById(incorrectClientNameMessageId);
    removeElementById(incorrectClientSurnameMessageId);
    removeElementById(incorrectClientEmailMessageId);
    removeElementById(incorrectClientLoginMessageId);
    removeElementById(incorrectClientPasswordMessageId);
    removeElementById(incorrectClientConfirmationPasswordMessageId);
    removeElementById(passwordMismatchMessageId);

    if (clientPassword.value !== clientConfirmationPassword.value) {
        displayMessage(cancelButton, passwordMismatchMessageValue, passwordMismatchMessageId);
    }

    if (!isUserDataValid(clientConfirmationPassword.value, clientPasswordPattern)) {
        displayMessage(cancelButton, incorrectClientConfirmationPasswordMessageValue, incorrectClientConfirmationPasswordMessageId);
    }

    if (!isUserDataValid(clientPassword.value, clientPasswordPattern)) {
        displayMessage(cancelButton, incorrectClientPasswordMessageValue, incorrectClientPasswordMessageId);
    }

    if (!isUserDataValid(clientLogin.value, clientLoginPattern)) {
        displayMessage(cancelButton, incorrectClientLoginMessageValue, incorrectClientLoginMessageId);
    }

    if (!isUserDataValid(clientEmail.value, clientEmailPattern)) {
        displayMessage(cancelButton, incorrectClientEmailMessageValue, incorrectClientEmailMessageId);
    }

    if (!isUserDataValid(clientSurname.value, clientNamePattern)) {
        displayMessage(cancelButton, incorrectClientSurnameMessageValue, incorrectClientSurnameMessageId);
    }

    if (!isUserDataValid(clientName.value, clientNamePattern)) {
        displayMessage(cancelButton, incorrectClientNameMessageValue, incorrectClientNameMessageId);
    }

    if (isUserDataValid(clientName.value, clientNamePattern) && isUserDataValid(clientSurname.value, clientNamePattern)
        && isUserDataValid(clientEmail.value, clientEmailPattern) && isUserDataValid(clientLogin.value, clientLoginPattern)
        && isUserDataValid(clientPassword.value, clientPasswordPattern) && isUserDataValid(clientConfirmationPassword.value, clientPasswordPattern)
        && clientPassword.value === clientConfirmationPassword.value) {
        fetch('http://localhost:8080/client/register/' + clientLogin.value)
            .then(response => response.json())
            .then(response => {
                if (response === false) {
                    fetch('http://localhost:8080/client/register', {
                        method: 'POST',
                        body: JSON.stringify({
                            name: clientName.value,
                            surname: clientSurname.value,
                            contactDetails: {
                                email: clientEmail.value
                            },
                            password: clientPassword.value,
                            username: clientLogin.value
                        }),
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    });
                    displayPopupMessage(clientRegisterFormId, clientRegistrationMessageId, successClientRegistrationMessage, registerPopupMessageClassName);
                    setTimeout(() => removeElementById(clientRegistrationMessageId), 2000);
                    clearRegisterFormContent();
                    setTimeout(() => location.href='/client/login', 3000);
                } else {
                    displayPopupMessage(clientRegisterFormId, clientRegistrationMessageId, existClientLoginMessage, registerPopupMessageClassName);
                    setTimeout(() => removeElementById(clientRegistrationMessageId), 2000);
                }
            })
    }
}

function isUserDataValid(fieldValue, pattern) {
    if (fieldValue != null && pattern != null) {
        let regExPattern = XRegExp(pattern);
        return (regExPattern.test(fieldValue));
    }
    return false;
}

function displayMessage(refElement, message, messageId) {
    if (refElement != null && message != null && messageId != null) {
        if (message !== '') {
            let messageElement = document.createElement('p');
            messageElement.id = messageId;
            messageElement.classList.add('error-message');
            messageElement.innerHTML = message;
            refElement.parentNode.insertBefore(messageElement, refElement.nextSibling);
        }
    }
}

function clearRegisterFormContent() {
    clientName.value = '';
    clientSurname.value = '';
    clientEmail.value = '';
    clientLogin.value = '';
    clientPassword.value = '';
}

function togglePasswordField() {
    let passwordField = document.querySelector('#client-password');
    let confirmationPasswordField = document.querySelector('#client-confirmation-password');
    if(passwordField.type === 'password' || confirmationPasswordField.type === 'password') {
        passwordField.type = 'text';
        confirmationPasswordField.type = 'text';
    } else {
        passwordField.type = 'password';
        confirmationPasswordField.type = 'password';
    }
}