const addCarBrandInput = document.getElementById("add-car-brand");
const addCarModelInput = document.getElementById("add-car-model");
const addCarBodyTypeSelect = document.getElementById("add-car-body-type");
const addCarYearOfProductionInput = document.getElementById("add-car-year-production");
const addCarColorInput = document.getElementById("add-car-color");
const addCarDailyFeeInput = document.getElementById("add-car-daily-fee");
const addCarMileageInput = document.getElementById("add-car-mileage");
const addCarStatusSelect = document.getElementById("add-car-status");
const addCarButton = document.getElementById("add-car-button");
const carUpdateForm = document.querySelector("#update-car-form");
const updateCarIdInput = carUpdateForm.querySelector("#update-car-id");
const updateCarBrandInput = carUpdateForm.querySelector("#update-car-brand");
const updateCarModelInput = carUpdateForm.querySelector("#update-car-model");
const updateCarBodyTypeSelect = carUpdateForm.querySelector("#update-car-body-type");
const updateCarYearOfProductionInput = carUpdateForm.querySelector("#update-car-year-production");
const updateCarColorInput = carUpdateForm.querySelector("#update-car-color");
const updateCarDailyFeeInput = carUpdateForm.querySelector("#update-car-daily-fee");
const updateCarMileageInput = carUpdateForm.querySelector("#update-car-mileage");
const updateCarStatusSelect = carUpdateForm.querySelector("#update-car-status");
const updateCarButton = document.querySelector("#update-car-button");
const cancelUpdateCarButton = document.querySelector("#cancel-update-car-button");

hideCarEditForm();
getCarBodyTypes();
getCarStatus();
getCars();

cancelUpdateCarButton.addEventListener('click', function() {
    setDefaultFieldValues(updateCarBrandInput, updateCarModelInput, updateCarBodyTypeSelect, updateCarYearOfProductionInput, updateCarColorInput, updateCarDailyFeeInput, updateCarMileageInput, updateCarStatusSelect, updateCarIdInput);
    clearFieldValue(updateCarIdInput);
    hideCarEditForm();
});

addCarButton.addEventListener('click', function () {
    if(isCarDataValid(addCarBrandInput, addCarModelInput, addCarYearOfProductionInput, addCarColorInput, addCarDailyFeeInput, addCarMileageInput)) {
        fetch('http://localhost:8080/car/add', {
            method: 'post',
            body: JSON.stringify({
                'brand': addCarBrandInput.value.trim(),
                'model': addCarModelInput.value.trim(),
                'bodyType': addCarBodyTypeSelect.value,
                'yearOfProduction': addCarYearOfProductionInput.value,
                'color': addCarColorInput.value.trim(),
                'dailyFee': addCarDailyFeeInput.value,
                'mileage': addCarMileageInput.value,
                'carStatus': addCarStatusSelect.value
            }),
            headers: {
                'Content-type': 'application/json'
            }
        }).then(getCars);
        setDefaultFieldValues(addCarBrandInput, addCarModelInput, addCarBodyTypeSelect, addCarYearOfProductionInput, addCarColorInput, addCarDailyFeeInput, addCarMileageInput, addCarStatusSelect);
    }
});

function getCars() {
    fetch('http://localhost:8080/car')
        .then(response => response.json())
        .then(displayCarTable)
}

function getCarBodyTypes() {
    fetch('http://localhost:8080/car/getBodyTypes?sortBy=plDescription')
        .then(response => response.json())
        .then(response => createCarBodyTypeOption(response, addCarBodyTypeSelect, updateCarBodyTypeSelect));
}

function createCarBodyTypeOption(carBodyTypes, ...selectElements) {
    if(carBodyTypes != null && Array.isArray(carBodyTypes) && carBodyTypes.length > 0 &&  selectElements != null && selectElements.length > 0) {
        let optionContent = '';
        for (i=0; i<carBodyTypes.length; i++) {
            let option = '<option value=' + carBodyTypes[i].value + '>' + carBodyTypes[i].plDescription + '</option>';
            optionContent += option;
        }
        optionContent += '<option value=ALL>wszystkie</option>';
        for (j=0; j<selectElements.length; j++) {
            selectElements[j].innerHTML = optionContent;
            let selectElement = selectElements[j];
            for (let k=0; j<selectElement.length; k++) {
                if(selectElement[k].value == 'HATCHBACK') {
                    selectElement[k].selected = true;
                    break;
                }
            }
        }
    }
}

function getCarStatus() {
    fetch('http://localhost:8080/car/getStatus?sortBy=plStatus')
        .then(response => response.json())
        .then(response => createCarStatusOption(response, addCarStatusSelect, updateCarStatusSelect));
}

function createCarStatusOption(carStatus, ...selectElements) {
    if(carStatus != null && Array.isArray(carStatus) && carStatus.length > 0 &&  selectElements != null && selectElements.length > 0) {
        let optionContent = '';
        for (let i=0; i<carStatus.length; i++) {
            let option = '<option value=' + carStatus[i].value + '>' + carStatus[i].plStatus + '</option>';
            optionContent += option;
        }
        for(let j=0; j<selectElements.length; j++) {
            selectElements[j].innerHTML = optionContent;
            selectElement = selectElements[j];
            for(let k=0; k<selectElement.length; k++) {
                if(selectElement[k].value == 'AVAILABLE') {
                    selectElement[k].selected = true;
                    break;
                }
            }
        }
    }
}

function displayCarTable(cars) {
    const tableBody = document.querySelector('tbody');
    tableBody.innerHTML = '';
    for (let i = 0; i < cars.length; i++) {
        const car = cars[i];
        const tableRow = document.createElement('tr');
        tableRow.innerHTML = `<td id="tableCarId"> ${car.id} </td>
                              <td id="tableCarBrand">  ${car.brand}</td>
                              <td id="tableCarModel"> ${car.model}</td>
                              <td id="tableCarBody"> ${car.bodyType.plDescription}</td>
                              <td id="tableCarYearOfProduction"> ${car.yearOfProduction}</td>
                              <td id="tableCarColor"> ${car.color}</td>
                              <td id="tableCarMileage">  ${car.mileage}</td>
                              <td id="tableCarStatus"> ${car.carStatus.plStatus}</td>
                              <td id="tableCarDailyFee"> ${car.dailyFee}</td>
                              <input type="button" value="Edytuj" id="car-edit-button">
                              <input type="button" value="Usuń" id="car-remove-button">`;

        const carRemoveButton = tableRow.querySelector("#car-remove-button");
        carRemoveButton.addEventListener('click', function () {
            fetch('http://localhost:8080/car/' + car.id, {
                method: 'delete'})
                .then(getCars)
        });

        const carEditButton = tableRow.querySelector("#car-edit-button");
        carEditButton.addEventListener('click', function () {
            showCarEditForm();
            updateCarIdInput.value = car.id;
            updateCarBrandInput.value = car.brand;
            updateCarModelInput.value = car.model;
            updateCarBodyTypeSelect.value = car.bodyType.value;
            updateCarYearOfProductionInput.value = car.yearOfProduction;
            updateCarColorInput.value = car.color;
            updateCarDailyFeeInput.value = car.dailyFee;
            updateCarStatusSelect.value = car.carStatus.value;
            updateCarMileageInput.value = car.mileage;
        });
        tableBody.appendChild(tableRow);
    }
}

updateCarButton.addEventListener('click', function() {
    if(isCarDataValid(updateCarBrandInput, updateCarModelInput, updateCarYearOfProductionInput, updateCarColorInput, updateCarDailyFeeInput, updateCarMileageInput)) {
        fetch('http://localhost:8080/car/' + updateCarIdInput.value, {
            method: 'put',
            body: JSON.stringify({
                'brand': updateCarBrandInput.value.trim(),
                'model': updateCarModelInput.value.trim(),
                'bodyType': updateCarBodyTypeSelect.value,
                'yearOfProduction': updateCarYearOfProductionInput.value,
                'color': updateCarColorInput.value.trim(),
                'dailyFee': updateCarDailyFeeInput.value,
                'mileage': updateCarMileageInput.value,
                'carStatus': updateCarStatusSelect.value
            }),
            headers: {
                'Content-type':'application/json'
            }
            }).then(getCars);
        setDefaultFieldValues(updateCarBrandInput, updateCarModelInput, updateCarBodyTypeSelect, updateCarYearOfProductionInput, updateCarColorInput, updateCarDailyFeeInput, updateCarMileageInput, updateCarStatusSelect);
        clearFieldValue(updateCarIdInput);
        hideCarEditForm();
    }
});

function setDefaultFieldValues(brandField, modelField, bodyTypeField, yearOfProductionField, colorField, dailyFeeField, mileageField, statusField) {
    brandField.value = '';
    modelField.value = '';
    bodyTypeField.value = 'HATCHBACK';
    yearOfProductionField.value = '';
    colorField.value = '';
    dailyFeeField.value = '';
    mileageField.value = '';
    statusField.value = 'AVAILABLE';
}

function clearFieldValue(field) {
    field.value = '';
}

function hideCarEditForm() {
    document.getElementById("update-car-form").style.display = 'none';
}

function showCarEditForm() {
    document.getElementById("update-car-form").style.display = 'block';
}

function isCarDataValid(brandField, modelField, yearOfProductionField, colorField, dailyFeeField, mileageField) {
    let incorrectNameMessageValue = 'Wypełnij pole (limit do 25 znaków alfabetycznych)';
    let incorrectCarBrandNameMessageId = 'incorrect-car-brand-name';
    let incorrectCarModelNameMessageId = 'incorrect-car-model-name';
    let incorrectCarYearProductionMessageId = 'incorrect-car-year-production';
    let incorrectCarColorNameMessageId = 'incorrect-car-color-name';
    let incorrectCarDailyFeeMessageId = 'incorrect-car-daily-fee';
    let incorrectCarMileageMessageId = 'incorrect-car-mileage';
    let namePattern = '^[a-z\\sA-Z]{1,25}$';
    let modelPattern = '^[\\s\\w]{1,25}$';
    let yearPattern = '^\\d{4}$';
    let dailyFeePattern = '^\\d{1,3}$';
    let mileagePattern = '^\\d{1,6}$';

    removeElementById(incorrectCarBrandNameMessageId);
    removeElementById(incorrectCarModelNameMessageId);
    removeElementById(incorrectCarYearProductionMessageId);
    removeElementById(incorrectCarColorNameMessageId);
    removeElementById(incorrectCarDailyFeeMessageId);
    removeElementById(incorrectCarMileageMessageId);

    if (!isCarFieldValueValid(brandField.value, namePattern)) {
        displayMessage(brandField, incorrectNameMessageValue, incorrectCarBrandNameMessageId);
    }
    if (!isCarFieldValueValid(modelField.value, modelPattern)) {
        displayMessage(modelField, 'Wypełnij pole (limit do 25 znaków)', incorrectCarModelNameMessageId);
    }
    if (!isCarFieldValueValid(yearOfProductionField.value, yearPattern)) {
        displayMessage(yearOfProductionField, 'Wypełnij pole (limit 4 cyfr)', incorrectCarYearProductionMessageId);
    }
    if (!isCarFieldValueValid(colorField.value, namePattern)) {
        displayMessage(colorField, incorrectNameMessageValue, incorrectCarColorNameMessageId);
    }
    if (!isCarFieldValueValid(dailyFeeField.value, dailyFeePattern)) {
        displayMessage(dailyFeeField, 'Wypełnij pole (limit do 3 cyfr)', incorrectCarDailyFeeMessageId);
    }
    if (!isCarFieldValueValid(mileageField.value, mileagePattern)) {
        displayMessage(mileageField, 'Wypełnij pole (limit do 6 cyfr)', incorrectCarMileageMessageId);
    }

    return isCarFieldValueValid(brandField.value, namePattern) &&
           isCarFieldValueValid(modelField.value, modelPattern) &&
           isCarFieldValueValid(yearOfProductionField.value, yearPattern) &&
           isCarFieldValueValid(colorField.value, namePattern) &&
           isCarFieldValueValid(dailyFeeField.value, dailyFeePattern) &&
           isCarFieldValueValid(mileageField.value, mileagePattern);
}

function displayMessage(refElement, message, messageId) {
    if (refElement != null && message != null && messageId != null) {
        if (message !== '') {
            let messageElement = document.createElement('p');
            messageElement.id = messageId;
            messageElement.classList.add('car-form-error-message');
            messageElement.innerHTML = message;
            refElement.parentNode.insertBefore(messageElement, refElement.nextSibling);
        }
    }
}

function isCarFieldValueValid(fieldValue, pattern) {
    if (fieldValue != null && pattern != null) {
        let regExPattern = new RegExp(pattern);
        return (regExPattern.test(fieldValue));
    }
    return false;
}
