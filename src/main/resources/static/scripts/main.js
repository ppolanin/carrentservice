const findCarButton = document.querySelector('#find-car-button');
const findCarByBrandInput = document.querySelector('#car-brand');
const findCarByBodyTypeInput = document.querySelector('#car-body-type');
const reservationFromDate = document.getElementById('reservation-date-from');
const reservationToDate = document.getElementById('reservation-date-to');
const pickupCarLocation = document.querySelector('#pick-up-car-location');
const returnCarLocation = document.querySelector('#return-car-location');
const carBodyType = document.getElementById('car-body-type');
const clearQueryButton = document.getElementById('clear-query-button');
const mainContentId = 'main-content';
const emptyDateInputMessage = 'Wybierz datę odbioru i zwrotu';
const emptyDateInputMessageId = 'empty-date-input-message';
const emptyDateMessageClassName = 'date-popup-message';

getLocations();
getCarBodyTypes();

function getCars() {
    fetch('http://localhost:8080/car')
        .then(response => response.json())
        .then(response => displayCarsTableForClients(response));
}

function getLocations() {
    fetch('http://localhost:8080/location/getAll')
        .then(response => response.json())
        .then(response => createLocationOption(response, pickupCarLocation, returnCarLocation));
}

function getCarBodyTypes() {
    fetch('http://localhost:8080/car/getBodyTypes?sortBy=plDescription')
        .then(response => response.json())
        .then(response => createCarBodyTypeOption(response, carBodyType));
}

function createLocationOption(locations, ...selectElements) {
    if(locations != null && Array.isArray(locations) && locations.length > 0 &&  selectElements != null && selectElements.length > 0) {
        let optionContent = '';
        for (i=0; i<locations.length; i++) {
            let option = '<option value=' + locations[i].id + '>' + locations[i].description + '</option>';
            optionContent += option;
        }
        for (j=0; j<selectElements.length; j++) {
            selectElements[j].innerHTML = optionContent;
        }
    }
}

function createCarBodyTypeOption(carBodyTypes, ...selectElements) {
    if(carBodyTypes != null && Array.isArray(carBodyTypes) && carBodyTypes.length > 0 &&  selectElements != null && selectElements.length > 0) {
        let optionContent = '';
        for (i=0; i<carBodyTypes.length; i++) {
            let option = '<option value=' + carBodyTypes[i].value + '>' + carBodyTypes[i].plDescription + '</option>';
            optionContent += option;
        }
        optionContent += '<option value=ALL>wszystkie</option>';
        for (j=0; j<selectElements.length; j++) {
            selectElements[j].innerHTML = optionContent;
        }
    }
}

function displayCarsTableForClients(cars) {
    const tableBody = document.querySelector('tbody');
    tableBody.innerHTML = "";
    for (let i = 0; i < cars.length; i++) {
        const car = cars[i];
        const tableRow = document.createElement('tr');
        tableRow.innerHTML = `<td> ${car.brand} </td>
                               <td> ${car.model} </td>
                               <td> ${car.color} </td>
                               <td> ${car.bodyType.plDescription} </td>
                               <td> <input type='button' value='wybierz' id="selection-car-button"> </td>`;
        let selectionCarButton = tableRow.querySelector('#selection-car-button');
        selectionCarButton.addEventListener('click', reserveCar(car.id, reservationFromDate.value, reservationToDate.value, pickupCarLocation.value, returnCarLocation.value));
        tableBody.appendChild(tableRow);
    }
}

function reserveCar(carId, startDate, endDate, pickupCarLocationId, returnCarLocationId) {
    return function () {
        if (startDate != '' || endDate != '') {
            location.href = 'http://localhost:8080/reservation/initial?carId=' + carId + '&startDate=' + startDate + '&pickupLocationId=' + pickupCarLocationId + '&endDate=' +  endDate + '&returnLocationId=' + returnCarLocationId;
        } else {
            showMessageForEmptyDateField();
        }
    }
}

findCarButton.addEventListener('click', function () {
    if(reservationFromDate.value != '' || reservationToDate.value != '') {
        if (findCarByBrandInput.value != "") {
            if(findCarByBodyTypeInput.value != "ALL") {
                fetch("http://localhost:8080/car/byBookingDurationAndBrandAndBodyType/" + reservationFromDate.value + "/" + reservationToDate.value + "/" + findCarByBrandInput.value + "/" + findCarByBodyTypeInput.value)
                    .then(response => response.json())
                    .then(response => {
                        displayCarsTableForClients(response);
                    });
            } else {
                fetch("http://localhost:8080/car/byBookingDurationAndBrand/" + reservationFromDate.value + "/" + reservationToDate.value + "/" + findCarByBrandInput.value)
                    .then(response => response.json())
                    .then(response => {
                        displayCarsTableForClients(response);
                    });
            }
        } else {
            if (findCarByBodyTypeInput.value == 'ALL') {
                fetch("http://localhost:8080/car/byBookingDuration/" + reservationFromDate.value + "/" + reservationToDate.value)
                    .then(response => response.json())
                    .then(response => {
                        displayCarsTableForClients(response)
                    });
            } else {
                fetch("http://localhost:8080/car/byBookingDurationAndBodyType/" + reservationFromDate.value + "/" + reservationToDate.value + "/" + findCarByBodyTypeInput.value)
                    .then(response => response.json())
                    .then(response => {
                        displayCarsTableForClients(response)
                    });
            }
        }
    } else {
        showMessageForEmptyDateField();
    }
});

clearQueryButton.addEventListener('click', function() {
    reservationFromDate.value = '';
    reservationToDate.value = '';
    findCarByBodyTypeInput.selectedIndex = 0;
    findCarByBrandInput.value = '';
    getCars();
});

function setCurrentDate() {
    let currentDate = new Date();
    let nextDate = new Date() + 1;
    $(reservationFromDate).handleDtpicker('setDate', currentDate);
    $(reservationFromDate).handleDtpicker('setDate', nextDate);
}

function showMessageForEmptyDateField() {
    displayPopupMessage(mainContentId, emptyDateInputMessageId, emptyDateInputMessage, emptyDateMessageClassName);
    setTimeout(() => removeElementById(emptyDateInputMessageId), 2000);
}

$(function() {
    $(".datepicker").appendDtpicker({
    "current": null,
    "dateFormat": "DD.MM.YYYY hh:mm",
    "locale": "pl",
    "animation": true,
    "minuteInterval": 15,
    "firstDayOfWeek": 1,
    "closeOnSelected": true,
    // enable/disable auto scroll
    "timelist<a href=\"https://www.jqueryscript.net/tags.php?/Scroll/\">Scroll</a>": true,
    "calendarMouseScroll": true,
    "todayButton": true,
    "closeButton": true,
    "dateOnly": false,
    "timeOnly": false,
    "futureOnly": true,
    "autodateOnStart": false,
    "minTime": "08:00",
    "maxTime": "21:15",
    "allowWdays": false,
    "amPmInTimeList": false,
    "externalLocale": null,
    });
});

$('#reservation-date-from').change((function() {
    let minStartDate = new Date($('#reservation-date-from').handleDtpicker('getDate'));
    minStartDate.setDate(minStartDate.getDate() + 1);
    $("#reservation-date-to").handleDtpicker('setMinDate', minStartDate);
    $('#reservation-date-to').handleDtpicker('setDate', minStartDate);
}));