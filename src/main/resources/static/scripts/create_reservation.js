const successReservationMessage = 'Rezerwacja została założona';
const popupMessagePaneId = 'popup-message-pane';
const initialReservationTable = document.querySelector('#client-initial-reservation-table');
const confirmationButton = document.querySelector('#reservation-confirmation-button');
const cancelButton = document.querySelector('#reservation-cancel-button');
const carDetailsButton = document.querySelector('#car-details-button');
const pickupLocationDetailsButton = document.querySelector('#pick-up-location-details-button');
const returnLocationDetailsButton = document.querySelector('#return-location-details-button');
const loginForm = document.querySelector('#login-form');
const usernameInput = document.querySelector('#username');
const passwordInput = document.querySelector('#password');
const loginSubmitButton = document.querySelector('#login-submit-button');
const loginCancelButton = document.querySelector('#login-cancel-button');
const requiredFieldMessage = 'Pole wymagane';
const popupLoginFieldErrorMessageNameClass = 'popup-login-field-error-message';
const errorLoginPopupMessageNameClass = 'error-login-popup-message';
const invalidLoginMessage = 'Nieprawidłowy login lub hasło';
const invalidLoginMessageId = 'invalid-login-message';
const startDateReservation = document.querySelector('#reservation-from-date');
const endDateReservation = document.querySelector('#reservation-to-date');
const dayFee = document.querySelector('#day-fee');
const completeFee = document.querySelector('#complete-fee');
confirmationButton.addEventListener('click', confirmReservation);
cancelButton.addEventListener('click', function() {location.href='/main'});
carDetailsButton.addEventListener('click', function(){displayCarDetails(carDetailsButton.name)});
loginSubmitButton.addEventListener('click', confirmReservationThroughLogin);
loginCancelButton.addEventListener('click', restoreLoginFieldsState);


function confirmReservation() {
     let url =  window.location.href.replace('initial', 'create');
     fetch('http://localhost:8080/client/authentication')
         .then(response => {
             response.text().then(response => {
                 let clientId = response;
                 if (clientId != '') {
                     $.ajax({
                        method: 'POST',
                        url: url + '&clientId=' + clientId,
                        success: function() {
                            disableButtons(carDetailsButton, confirmationButton, cancelButton);
                            redirect(initialReservationTable, popupMessagePaneId, successReservationMessage);
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            console.log('reservation error {error code: ' + jqXHR.status + '; text status: ' + textStatus + '; error: ' + errorThrown + '}');
                        }
                     });
                     disableButtons(carDetailsButton, confirmationButton, cancelButton);
                     redirect(initialReservationTable, popupMessagePaneId, successReservationMessage);
                 } else {
                     showElement(loginForm);
                 }
             });
         });
}

function confirmReservationThroughLogin() {
    let url =  window.location.href.replace('initial', 'create');
    if (isEachFieldFilled(requiredFieldMessage, popupLoginFieldErrorMessageNameClass, usernameInput, passwordInput)) {
        fetch('http://localhost:8080/client/login-authentication', {
            method: 'POST',
            headers: {'Authorization': 'Basic ' + btoa(usernameInput.value + ":" + passwordInput.value)}})
                .then(response => {
                    if(response.ok) {
                        response.text().then(response => {
                            let clientId = response;
                            hideElement(loginForm);
                            clearLoginFormContent();
                            $.ajax({
                                method: 'POST',
                                url: url + '&clientId=' + clientId,
                                success: function() {
                                    disableButtons(carDetailsButton, confirmationButton, cancelButton);
                                    redirect(initialReservationTable, popupMessagePaneId, successReservationMessage);
                                },
                                error: function(jqXHR, textStatus, errorThrown) {
                                    console.log('reservation error {error code: ' + jqXHR.status + '; text status: ' + textStatus + '; error: ' + errorThrown + '}');
                                }
                            });
                        });
                    } else {
                        displayPopupMessage(loginForm.id, invalidLoginMessageId, invalidLoginMessage, errorLoginPopupMessageNameClass);
                        clearLoginFormContent();
                        setTimeout(() => removeElementById(invalidLoginMessageId), 2000);
                    }
                });
    }
}

function clearLoginFormContent() {
    usernameInput.value = '';
    passwordInput.value = '';
}

function redirect(refElement, redirectPopupPaneId, successReservationMessage) {
    removeElementById(redirectPopupPaneId);
    let redirectPopupPane = document.createElement('div');
    redirectPopupPane.id = redirectPopupPaneId;
    redirectPopupPane.className = 'reservation-popup-message';
    let clientAccountLink = document.createElement('a');
    clientAccountLink.id = 'client-account-link';
    clientAccountLink.href = '/client/data';
    clientAccountLink.innerHTML = 'Moje konto';
    let mainPageLink = document.createElement('a');
    mainPageLink.id = 'main-link';
    mainPageLink.innerHTML = 'Strona główna';
    mainPageLink.href = '/main';
    let statusReservationMessage = document.createElement('p');
    statusReservationMessage.innerHTML = successReservationMessage;
    let redirectMessage = document.createElement('p');
    redirectMessage.innerHTML = 'Przejdź do:';
    redirectPopupPane.appendChild(statusReservationMessage);
    redirectPopupPane.appendChild(redirectMessage);
    redirectPopupPane.appendChild(clientAccountLink);
    redirectPopupPane.appendChild(mainPageLink);
    insertElementAfter(redirectPopupPane, refElement);
}

function disableButtons(...buttons) {
    if(buttons.length > 0) {
        for(let i = 0; i < buttons.length; i++) {
            buttons[i].disabled = true;
        }
    }
}

function restoreLoginFieldsState() {
    hideElement(loginForm);
    usernameInput.style.border = '1px solid black';
    passwordInput.style.border = '1px solid black';
    removeElementByClassName('login-form', 'popup-login-field-error-message');
    clearLoginFormContent();
}
