createViewLinks(document.querySelector("#main-content"));

function logout() {
    fetch('http://localhost:8080/client/account/logout', {method: 'POST'});
    location.href="/main";
}

function createViewLinks(referenceElement) {
    let container = document.createElement('div');
    let clientDataLink = document.createElement('a');
    clientDataLink.id = 'client-data';
    clientDataLink.innerHTML = 'Dane konta';
    clientDataLink.href = '/client/data';
    let clientReservationsLink = document.createElement('a');
    clientReservationsLink.id = 'client-reservations';
    clientReservationsLink.innerHTML = 'Rezerwacje';
    clientReservationsLink.href = '/client/reservations';
    let mainSiteLink = document.createElement('a');
    mainSiteLink.id = 'main-site';
    mainSiteLink.innerHTML = 'Strona główna';
    mainSiteLink.href = '/main';
    let clientLogoutButton = document.createElement('input');
    clientLogoutButton.setAttribute('type', 'button');
    clientLogoutButton.setAttribute('id', 'client-logout-button');
    clientLogoutButton.setAttribute('value', 'Wyloguj');
    clientLogoutButton.addEventListener('click', logout);

    container.appendChild(clientLogoutButton);
    container.appendChild(document.createElement('br'));
    container.appendChild(clientDataLink);
    container.appendChild(document.createElement('br'));
    container.appendChild(clientReservationsLink);
    container.appendChild(document.createElement('br'));
    container.appendChild(mainSiteLink);
    insertElementBefore(container, referenceElement);
}
