const reservationIdInput = document.querySelector("#reservation-id");
const reservationStatusInput = document.querySelector("#reservation-status");
const remarkArea = document.querySelector("#remark");
const confirmReservationButton = document.querySelector("#confirm-reservation-button");
const cancelReservationButton = document.querySelector("#cancel-reservation-button");
const startReservationDate = document.querySelector("#reservation-start-date");
const endReservationDate = document.querySelector("#reservation-end-date");
const rentalDate = document.querySelector("#rental-date");
const rentalDetailsBlock = document.querySelector("#rental-details");
const reservationForm = document.querySelector("#reservation-form");
rentalDate.value = startReservationDate.value;
const minStartDate = parseDateTime(startReservationDate.value);
const maxStartDate = getShiftedRentalDateTime(startReservationDate.value, endReservationDate.value, 2, 4);

hideRentalElements('nowa');
setStateOfReservationFormElements('nowa');
confirmReservationButton.addEventListener('click', createRental);
cancelReservationButton.addEventListener('click', displayCancelReservationPopupWindow);

function createRental() {
    if(isDateTimeValid(rentalDate.value)) {
        if(validateRentalDate(minStartDate, maxStartDate)) {
            fetch('http://localhost:8080/employee/authentication')
                .then(response => {
                    response.text().then(response => {
                        let employeeId = response;
                        if (employeeId != '') {
                            fetch('http://localhost:8080/rental/add?date=' + rentalDate.value + '&employeeId=' + employeeId + '&reservationId=' + reservationIdInput.value + '&remark=' + remarkArea.value, {
                                method: 'POST'
                             });
                            location.href='/employee/panel';
                        } else {
                            throw 'Incorrect employee id';
                        }
                    });
                });
        } else {
            displayPopupMessage(reservationForm, 'Data wypożyczenia może znajdować się w przedziale do 2 dni od daty odbioru pojazdu');
        }
    } else {
        displayPopupMessage(reservationForm, 'Nieprawidłowy format daty wypożyczenia (dd.mm.rrrr gg:mm)');
    }
}

function cancelReservation() {
    fetch('http://localhost:8080/employee/authentication')
        .then(response => response.text())
        .then(response => {
            let employeeId = response;
            if (employeeId != '') {
                fetch('http://localhost:8080/reservation/cancel/' + reservationIdInput.value, {method: 'POST'});
                location.href='/employee/panel';
            } else {
                throw 'Incorrect employee id';
            }
        });
}

function isDateTimeValid(stringDateTime) {
    if(stringDateTime != null && stringDateTime.length > 0) {
        let dateTimeRegEx = new RegExp('(\\d{2})\\.(\\d{2})\\.(\\d{4}) (\\d{2}):(\\d{2})');
        return dateTimeRegEx.test(stringDateTime);
    }
    return false;
}

function validateRentalDate(startDate, endDate) {
    let currentDate = new Date();
    return currentDate >= startDate && currentDate <= endDate;
}

function getShiftedRentalDateTime(stringStartDateTime, stringEndDateTime, dayShift, hoursShift) {
    let parsedStartDateTime = parseDateTime(stringStartDateTime);
    let parsedEndDateTime = parseDateTime(stringEndDateTime);
    if(parsedEndDateTime.getDate() - parsedStartDateTime.getDate() >= dayShift) {
        return new Date(parsedStartDateTime.getFullYear(), parsedStartDateTime.getMonth(), parsedStartDateTime.getDate() + dayShift, parsedStartDateTime.getHours(), parsedStartDateTime.getMinutes());
    } else {
        return new Date(parsedStartDateTime.getFullYear(), parsedStartDateTime.getMonth(), parsedStartDateTime.getDate(), parsedStartDateTime.getHours() + hoursShift, parsedStartDateTime.getMinutes());
    }
}

function parseDateTime(stringDateTime) {
    if(stringDateTime != null && stringDateTime.length > 0) {
        let dateTimeRegEx = new RegExp('(\\d{2})\\.(\\d{2})\\.(\\d{4}) (\\d{2}):(\\d{2})');
        if(dateTimeRegEx.test(stringDateTime)) {
            let result = stringDateTime.match(dateTimeRegEx);
            return new Date(result[3] + '-' + result[2] + '-' + result[1] + ' ' + result[4] + ':' + result[5]);
        } else {
            throw 'Incorrect time date data';
        }
    } else {
        throw 'Parameter cannot be null or empty';
    }
}

$(function() {
	$(".datepicker").appendDtpicker({
	"current": null,
	"dateFormat": "DD.MM.YYYY hh:mm",
	"locale": "pl",
	"animation": true,
	"minuteInterval": 15,
	"firstDayOfWeek": 1,
	"closeOnSelected": true,
	// enable/disable auto scroll
	"timelist<a href=\"https://www.jqueryscript.net/tags.php?/Scroll/\">Scroll</a>": true,
	"calendarMouseScroll": true,
	"todayButton": true,
	"closeButton": true,
	"dateOnly": false,
	"timeOnly": false,
	"futureOnly": true,
	"autodateOnStart": true,
	"minDate": minStartDate,
	"maxDate": maxStartDate,
	"minTime": "08:00",
	"maxTime": "21:15",
	"allowWdays": false,
	"amPmInTimeList": false,
	"externalLocale": null
	});
});

function setStateOfReservationFormElements(state) {
    if(reservationStatusInput.value != state) {
        confirmReservationButton.disabled = true;
        cancelReservationButton.disabled = true;
        rentalDate.readOnly = true;
        remarkArea.readOnly = true;
    } else {
        confirmReservationButton.disabled = false;
        cancelReservationButton.disabled = false;
        rentalDate.readOnly = false;
        remarkArea.readOnly = false;
    }
}

function hideRentalElements(state) {
    if(reservationStatusInput.value != state) {
        rentalDetailsBlock.style.display = 'none';
    } else {
        rentalDetailsBlock.style.display = 'block';
    }
}

function hideRentalElements(state) {
     if(reservationStatusInput.value != state) {
         rentalDetailsBlock.style.display = 'none';
     } else {
         rentalDetailsBlock.style.display = 'block';
     }
}

function displayPopupMessage(referenceElement, messageContent) {
    let popupMessageId = 'error-rental-message';
    let popupMessage = document.createElement('div');
    popupMessage.setAttribute('class', 'error-rental-message');
    popupMessage.id = popupMessageId;
    let messageElement = document.createElement('p');
    messageElement.innerHTML = messageContent;
    let closePopupMessageButton = document.createElement('input');
    closePopupMessageButton.id = 'close-message-button';
    closePopupMessageButton.type = 'button';
    closePopupMessageButton.value = 'Zamknij';
    popupMessage.appendChild(messageElement);
    popupMessage.appendChild(closePopupMessageButton);
    insertElementAfter(popupMessage, referenceElement);
    closePopupMessageButton.addEventListener('click', function() {
        removeElementById(popupMessageId);
    });
}

function displayCancelReservationPopupWindow() {
    let popupWindowId = 'confirmation-cancel-reservation';
    let popupWindow = document.createElement('div');
    popupWindow.setAttribute('class', 'cancel-reservation-popup-message');
    popupWindow.id = popupWindowId;
    let message = document.createElement('p');
    message.innerHTML = 'Czy anulować rezerwację?';
    let cancelReservationButton = document.createElement('input');
    cancelReservationButton.id = 'cancel-button';
    cancelReservationButton.type = 'button';
    cancelReservationButton.value = 'Potwierdź';
    let closeWindowButton = document.createElement('input');
    closeWindowButton.id = 'close-window-button';
    closeWindowButton.type = 'button';
    closeWindowButton.value = 'Zamknij';
    popupWindow.appendChild(message);
    popupWindow.appendChild(cancelReservationButton);
    popupWindow.appendChild(closeWindowButton);
    insertElementAfter(popupWindow, reservationForm);
    cancelReservationButton.addEventListener('click', function() {
        cancelReservation();
        removeElementById(popupWindowId);
    });
    closeWindowButton.addEventListener('click', function() {
        removeElementById(popupWindowId);
    });
}
