const changePasswordForm = document.querySelector('#change-password-form');
const clientOldPassword = document.querySelector('#client-old-password');
const clientNewPassword = document.querySelector('#client-new-password');
const clientNewConfirmationPassword = document.querySelector('#client-new-confirmation-password');
const confirmationButton = document.querySelector('#confirmation-button');
const cancelButton = document.querySelector('#cancel-button');
confirmationButton.addEventListener('click', changePassword);
cancelButton.addEventListener('click', hideChangePasswordForm);
const className = 'changing-password-form-error-message';

getClientData();

async function authentication() {
    let clientId = await fetch('http://localhost:8080/client/authentication');
    return clientId.text();
}

async function getClientData() {
    let clientId = await authentication();
    if(clientId != '') {
        fetch('http://localhost:8080/client/' + clientId)
            .then(response => response.json())
            .then(response => displayClientDataTable(response));
    }
}

function displayClientDataTable(client) {
    let tableBody = document.querySelector('tbody');
    tableBody.innerHTML = '';
    let tableRow = document.createElement('tr');
    if(client != null) {
        tableRow.innerHTML = `<td class="borderless-form-table"> ${client.name} </td>
                              <td class="borderless-form-table"> ${client.surname} </td>
                              <td class="borderless-form-table"> ${client.username} </td>
                              <td class="borderless-form-table"> ${client.contactDetails.email} </td>
                              <td class="borderless-form-table"> <button type="button" id="change-data-button" onclick="location.href='/client/update-form/' + ${client.id}"> Zmień </button> </td>
                              <td class="borderless-form-table"> <button type="button" id="change-password-button"> Zmień </button> </td>`;
        let changePasswordButton = tableRow.querySelector('#change-password-button');
        changePasswordButton.addEventListener('click', showChangePasswordForm);
        let changeDataButton = tableRow.querySelector('#change-data-button');
        tableBody.appendChild(tableRow);
    }
}

function changePassword() {
    if(validatePasswordFields()) {
        fetch('http://localhost:8080/client/changePassword', {
            method: 'POST',
            body: JSON.stringify({
                oldPassword: clientOldPassword.value,
                newPassword: clientNewPassword.value
            }),
            headers: {'Content-Type': 'application/json'}})
        .then(response => response.json())
        .then(response => {
            if(response) {
                hideChangePasswordForm();
                displayPopupMessage(changePasswordForm, 'Hasło zostało zmienione');
            } else {
                displayPopupMessage(changePasswordForm, 'Aktualne hasło jest nieprawidłowe');
                clearFields(clientOldPassword, clientNewPassword, clientNewConfirmationPassword);
            }
        });
    }
}

function validatePasswordFields() {
    let clientPasswordPattern = '(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*-_])(?=.{8,})';
    let incorrectClientPasswordMessage = 'Pole powinno zawierać co najmniej 8 znaków w tym dużą literę i cyfrę, dopuszczalne znaki specjalne: "!", "@", "#", "$", "%", "^", "&", "*", "-", "_"';
    removeElementByClassName(changePasswordForm.id, className);

    if(!isPasswordFieldValueValid(clientOldPassword.value, clientPasswordPattern)) {
        displayFieldMessage(clientOldPassword, incorrectClientPasswordMessage, className);
    }

    if(!isPasswordFieldValueValid(clientNewPassword.value, clientPasswordPattern)) {
        displayFieldMessage(clientNewPassword, incorrectClientPasswordMessage, className);
    }

    if(!isPasswordFieldValueValid(clientNewConfirmationPassword.value, clientPasswordPattern)) {
        displayFieldMessage(clientNewConfirmationPassword, incorrectClientPasswordMessage, className);
    }

    if(clientNewPassword.value !== clientNewConfirmationPassword.value) {
        displayFieldMessage(clientNewConfirmationPassword, 'Nowe hasło i potwierdzenie hasła nie są zgodne', className);
    }

    if(isPasswordFieldValueValid(clientOldPassword.value, clientPasswordPattern) && isPasswordFieldValueValid(clientNewPassword.value, clientPasswordPattern) &&
       isPasswordFieldValueValid(clientNewConfirmationPassword.value, clientPasswordPattern) && clientNewPassword.value === clientNewConfirmationPassword.value) {
       return true;
    }
    return false;
}

function displayFieldMessage(refElement, message, className) {
    if (refElement != null && message != null && className !== '') {
        if (message !== '') {
            let messageElement = document.createElement('p');
            messageElement.classList.add(className);
            messageElement.innerHTML = message;
            refElement.parentNode.insertBefore(messageElement, refElement.nextSibling);
        }
    }
}

function isPasswordFieldValueValid(fieldValue, pattern) {
    if (fieldValue != null && pattern != null) {
        let regExPattern = new RegExp(pattern);
        return (regExPattern.test(fieldValue));
    }
    return false;
}

function hideChangePasswordForm() {
    changePasswordForm.style.display = 'none';
    removeElementByClassName(changePasswordForm.id, className);
    clearFields(clientOldPassword, clientNewPassword, clientNewConfirmationPassword);
}

function showChangePasswordForm() {
    changePasswordForm.style.display = 'block';
}

function displayPopupMessage(referenceElement, messageContent) {
    let popupMessageId = 'changing-password-status';
    let popupMessage = document.createElement('div');
    popupMessage.setAttribute('class', 'changing-password-status-message');
    popupMessage.id = popupMessageId;
    let messageElement = document.createElement('p');
    messageElement.innerHTML = messageContent;
    let closePopupMessageButton = document.createElement('input');
    closePopupMessageButton.id = 'close-message-button';
    closePopupMessageButton.type = 'button';
    closePopupMessageButton.value = 'Zamknij';
    popupMessage.appendChild(messageElement);
    popupMessage.appendChild(closePopupMessageButton);
    insertElementAfter(popupMessage, referenceElement);
    closePopupMessageButton.addEventListener('click', function() {
        removeElementById(popupMessageId);
    });
}

function clearFields(...elements) {
    if(elements.length > 0) {
        for(let i=0; i < elements.length; i++) {
            elements[i].value = '';
        }
    }
}