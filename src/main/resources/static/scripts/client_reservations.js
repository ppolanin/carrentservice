const noBookingMessage = 'brak rezerwacji';

getReservations();

async function authentication() {
    let clientId = await fetch('http://localhost:8080/client/authentication');
    return clientId.text();
}

async function getReservations() {
    let clientId = await authentication();
    if(clientId != '') {
        fetch('http://localhost:8080/reservation/client/' + clientId)
            .then(response => response.json())
            .then(response => displayClientReservationsTable(response));
    } else {
        throw 'Incorrect client id or client is not logged in';
    }
}

function displayClientReservationsTable(reservations) {
    const tableBody = document.querySelector('tbody');
    tableBody.innerHTML = '';
    if (reservations.length > 0) {
        for(let i=0; i < reservations.length; i++) {
            let tableRow = document.createElement('tr');
            tableRow.innerHTML = `
                                  <td> ${formatLocalDateTimeToString(reservations[i].reservationDate)} </td>
                                  <td> ${formatLocalDateTimeToString(reservations[i].reservationFromDate)} </td>
                                  <td> ${formatLocalDateTimeToString(reservations[i].reservationToDate)} </td>
                                  <td> ${reservations[i].status} </td>
                                  <input type='button' id='car-details-button' value='Szczegóły' name=${reservations[i].car.id}>
                                  <td> ${reservations[i].days} </td>
                                  <td> ${reservations[i].costs} </td>
                                  <input type='button' id='cancel-reservation-button' value='Anuluj' name=${reservations[i].id}>`;

            let carDetailsButton = tableRow.querySelector('#car-details-button');
            carDetailsButton.addEventListener('click', function() {
                displayCarDetails(reservations[i].car.id)
            });
            let cancelReservationButton = tableRow.querySelector('#cancel-reservation-button');
            if(reservations[i].status === 'anulowana') {
                cancelReservationButton.disabled = true;
            }
            cancelReservationButton.addEventListener('click', function() {
                displayCancelReservationPopupWindow(reservations[i].id, cancelReservationButton);
            });

            tableBody.appendChild(tableRow);
        }
    } else {
        let message = document.createElement('p');
        message.innerHTML = noBookingMessage;
        message.classList.add("message");
        let reservationTable = document.querySelector('#client-reservations-table');
        insertElementAfter(message, reservationTable);
    }
}

function formatLocalDateTimeToString(date) {
    let dateRegEx = new RegExp('(\\d{4})-(\\d{2})-(\\d{2})T(\\d{2}:\\d{2}):\\d{2}');
    if(dateRegEx.test(date)) {
        let dateMatch = date.match(dateRegEx);
        return dateMatch[3] + '.' + dateMatch[2] + '.' + dateMatch[1] + ' ' + dateMatch[4];
    } else {
        throw 'Error during formatting date';
    }
}

function cancelReservation(reservationId, buttonToDisable) {
    fetch('http://localhost:8080/reservation/cancelByClient/' + reservationId, {method:'POST'})
        .then(response => response.json())
        .then(response => {
               let result = response;
               buttonToDisable.disabled = true;
               if(result) {
                    displayMessage('Rezerwacja została anulowana');
               } else {
                    displayMessage('Upłynął okres możliwości anulowania rezerwacji');
               }
            });
}

function displayCancelReservationPopupWindow(reservationId, buttonToDisable) {
    let popupWindowId = 'confirmation-cancel-reservation';
    let popupWindow = document.createElement('div');
    popupWindow.setAttribute('class', 'cancel-reservation-popup-message');
    popupWindow.id = popupWindowId;
    let message = document.createElement('p');
    message.innerHTML = 'Czy anulować rezerwację?';
    let cancelReservationButton = document.createElement('input');
    cancelReservationButton.id = 'cancel-reservation-button';
    cancelReservationButton.type = 'button';
    cancelReservationButton.value = 'Potwierdź';
    let closeWindowButton = document.createElement('input');
    closeWindowButton.id = 'close-window-button';
    closeWindowButton.type = 'button';
    closeWindowButton.value = 'Zamknij';
    popupWindow.appendChild(message);
    popupWindow.appendChild(cancelReservationButton);
    popupWindow.appendChild(closeWindowButton);
    insertElementAfter(popupWindow, document.querySelector('#client-reservations-table'));
    cancelReservationButton.addEventListener('click', function() {
        cancelReservation(reservationId, buttonToDisable);
        removeElementById(popupWindowId);
    });
    closeWindowButton.addEventListener('click', function() {
        removeElementById(popupWindowId);
    });
}

function displayMessage(messageContent) {
    let popupMessageId = 'cancel-reservation-status';
    let popupMessage = document.createElement('div');
    popupMessage.setAttribute('class', 'cancel-reservation-status-message');
    popupMessage.id = popupMessageId;
    let messageElement = document.createElement('p');
    messageElement.innerHTML = messageContent;
    let closePopupMessageButton = document.createElement('input');
    closePopupMessageButton.id = 'close-message-button';
    closePopupMessageButton.type = 'button';
    closePopupMessageButton.value = 'Zamknij';
    popupMessage.appendChild(messageElement);
    popupMessage.appendChild(closePopupMessageButton);
    insertElementAfter(popupMessage, document.querySelector('#client-reservations-table'));
    closePopupMessageButton.addEventListener('click', function() {
        removeElementById(popupMessageId);
    });
}
