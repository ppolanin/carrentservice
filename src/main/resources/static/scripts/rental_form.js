const rentalIdInput = document.querySelector("#rental-id");
const rentalStatusInput = document.querySelector("#rental-status");
const remarkArea = document.querySelector("#return-remark");
const confirmReturnButton = document.querySelector("#confirm-return-button");
const returnDetailsBlock = document.querySelector("#return-details");
const rentalForm = document.querySelector("#rental-form");
const reservationStartDate = parseDateTime(document.querySelector("#reservation-start-date").value);
const returnDateInput = document.querySelector("#return-date");

hideReturnElements('nowe');
setStateOfRentalFormElements('nowe');
confirmReturnButton.addEventListener('click', createReturn);

function createReturn() {
    if(isDateTimeValid(returnDateInput.value)) {
        if(parseDateTime(returnDateInput.value) > reservationStartDate) {
            $.ajax({
                url: 'http://localhost:8080/employee/authentication',
                method: 'GET'
            })
                .done(function(response) {
                    let employeeId = response;
                    $.ajax({
                        url: 'http://localhost:8080/return/add?date=' + returnDateInput.value + '&employeeId=' + employeeId + '&rentalId=' + rentalIdInput.value + '&remark=' + remarkArea.value,
                        method: 'POST'
                    })
                        .done(function() {
                            location.href='/return/edit-view/' + rentalIdInput.value;
                        })
                        .fail(function(jqXHR, textStatus, errorThrown) {
                            console.log('return error {error code: ' + jqXHR.status + '; text status: ' + textStatus + '; error: ' + errorThrown + '}');
                        });
                })
                .fail(function(jqXHR, textStatus, errorThrown) {
                    console.log('authentication error {error code: ' + jqXHR.status + '; text status: ' + textStatus + '; error: ' + errorThrown + '}');
                    });
        } else {
            displayPopupMessage(rentalForm, 'Data zwrotu powinna być po dacie odbioru pojazdu');
        }
    } else {
        displayPopupMessage(rentalForm, 'Nieprawidłowy format daty wypożyczenia (dd.mm.rrrr gg:mm)');
    }
}

$(function() {
	$(".datepicker").appendDtpicker({
	"current": null,
	"dateFormat": "DD.MM.YYYY hh:mm",
	"locale": "pl",
	"animation": true,
	"minuteInterval": 15,
	"firstDayOfWeek": 1,
	"closeOnSelected": true,
	// enable/disable auto scroll
	"timelist<a href=\"https://www.jqueryscript.net/tags.php?/Scroll/\">Scroll</a>": true,
	"calendarMouseScroll": true,
	"todayButton": true,
	"closeButton": true,
	"dateOnly": false,
	"timeOnly": false,
	"futureOnly": true,
	"autodateOnStart": true,
    "minDate": reservationStartDate,
	"minTime": "08:00",
	"maxTime": "21:15",
	"allowWdays": false,
	"amPmInTimeList": false,
	"externalLocale": null
	});
});

function setStateOfRentalFormElements(state) {
    if(rentalStatusInput.value != state) {
        confirmReturnButton.disabled = true;
        remarkArea.readOnly = true;
    } else {
        confirmReturnButton.disabled = false;
        remarkArea.readOnly = false;
    }
}

function hideReturnElements(state) {
     if(rentalStatusInput.value != state) {
         returnDetailsBlock.style.display = 'none';
     } else {
         returnDetailsBlock.style.display = 'block';
     }
}

function parseDateTime(stringDateTime) {
    if(stringDateTime != null && stringDateTime.length > 0) {
        let dateTimeRegEx = new RegExp('(\\d{2})\\.(\\d{2})\\.(\\d{4}) (\\d{2}):(\\d{2})');
        if(dateTimeRegEx.test(stringDateTime)) {
            let result = stringDateTime.match(dateTimeRegEx);
            return new Date(result[3] + '-' + result[2] + '-' + result[1] + ' ' + result[4] + ':' + result[5]);
        } else {
            throw 'Incorrect time date data';
        }
    } else {
        throw 'Parameter cannot be null or empty';
    }
}

function isDateTimeValid(stringDateTime) {
    if(stringDateTime != null && stringDateTime.length > 0) {
        let dateTimeRegEx = new RegExp('(\\d{2})\\.(\\d{2})\\.(\\d{4}) (\\d{2}):(\\d{2})');
        return dateTimeRegEx.test(stringDateTime);
    }
    return false;
}

function displayPopupMessage(referenceElement, messageContent) {
    let popupMessageId = 'error-return-message';
    let popupMessage = document.createElement('div');
    popupMessage.setAttribute('class', 'error-return-message');
    popupMessage.id = popupMessageId;
    let messageElement = document.createElement('p');
    messageElement.innerHTML = messageContent;
    let closePopupMessageButton = document.createElement('input');
    closePopupMessageButton.id = 'close-message-button';
    closePopupMessageButton.type = 'button';
    closePopupMessageButton.value = 'Zamknij';
    popupMessage.appendChild(messageElement);
    popupMessage.appendChild(closePopupMessageButton);
    insertElementAfter(popupMessage, referenceElement);
    closePopupMessageButton.addEventListener('click', function() {
        removeElementById(popupMessageId);
    });
}

