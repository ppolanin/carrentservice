function isEachFieldFilled(message, messageClass, ...fields) {
    if (fields.length > 0) {
        let results = [];
        let messageElementId = 'empty-field-message';
        for (let i = 0; i < fields.length; i++) {
            if (fields[i].value === '') {
                results.push(message);
                fields[i].style.border = '1px solid red';
                if(fields[i].nextElementSibling.id !== messageElementId) {
                    let messageElement = document.createElement('p');
                    messageElement.id = messageElementId;
                    messageElement.classList.add(messageClass);
                    messageElement.innerHTML = message;
                    fields[i].parentNode.insertBefore(messageElement, fields[i].nextSibling);
                }
            } else {
                fields[i].style.border = '1px solid black';
                if(fields[i].nextElementSibling.id == messageElementId) {
                    fields[i].nextElementSibling.remove();
                }
            }
        }
        for (let i = 0; i < results.length; i++) {
            if(results[i] == message) {
                return false;
            }
        }
        return true;
    } else {
        return false;
    }
}

function clearField(element) {
    return function () {
        element.value = '';
    }
}

function removeElementByClassName(parentElementId, className) {
    if(parentElementId != null && className != null) {
        if(parentElementId != '' && className != '') {
            let parentElement = document.getElementById(parentElementId);
            let elements = parentElement.getElementsByClassName(className);
            for(let i = elements.length - 1; i >= 0; i--) {
                elements[i].remove();
            }
        } else {
            throw 'Parameters cannot be empty';
        }
    } else {
        throw 'Parameters cannot be null';
    }
}

function removeElementById(id) {
    let elementById = document.getElementById(id);
    if(document.contains(elementById)) {
        elementById.remove();
    }
}

function displayPopupMessage(referenceIdElement, uniqueIdToSet, message, className) {
    if(referenceIdElement != null && uniqueIdToSet != null && message != null && className != null && className !== '') {
        let popUpMessage = document.createElement('div');
        let referenceElement = document.getElementById(referenceIdElement);
        popUpMessage.setAttribute('class', className);
        popUpMessage.id = uniqueIdToSet;
        popUpMessage.innerHTML = '<p>' + message + '</p>';
        referenceElement.appendChild(popUpMessage);
    } else {
        throw 'Error during displaying message';
    }
}

function insertElementBefore(insertElement, referenceElement) {
    referenceElement.parentNode.insertBefore(insertElement, referenceElement);
}

function insertElementAfter(insertElement, referenceElement) {
    referenceElement.parentNode.insertBefore(insertElement, referenceElement.nextSibling)
}

function showElement(element) {
    if(element != null) {
        if(element.style.display === 'none') {
            element.style.display = 'block';
        }
    }
}

function hideElement(element) {
    if(element != null) {
        if(element.style.display === 'block') {
            element.style.display = 'none';
        }
    }
}

function showHideElement(element) {
    if(element != null) {
        if(element.style.display === 'none') {
            element.style.display = 'block';
        } else {
            element.style.display = 'none';
        }
    }
}

function isElementHide(element) {
    if(element != null) {
        if(element.style.display === 'none') {
            return true;
        }
        return false;
    }
}

function formatDateToString(date) {
    if(date && Object.prototype.toString.call(date) === "[object Date]" && !isNaN(date)) {
        let stringMonth = date.getMonth() + 1;
        let stringDate = getStringNumberWithLeadingZero(date.getDate()) + '.' + getStringNumberWithLeadingZero(stringMonth) + '.' + date.getFullYear() + ' ' + getStringNumberWithLeadingZero(date.getHours()) + ':' + getStringNumberWithLeadingZero(date.getMinutes());
        return stringDate;
    } else {
        throw 'Error during formatting date';
    }
}

function getStringNumberWithLeadingZero(number) {
    if(typeof(number) === 'number') {
        if(number <= 9) {
            return '0' + number;
        } else {
            return number.toString();
        }
    } else {
        throw 'Parameter is not a number';
    }
}

function displayCarDetails(carId) {
    let infoCarId = 'info-car';
    removeElementById(infoCarId);
    let infoCar = document.createElement('div');
    infoCar.setAttribute('class', 'popup-message');
    infoCar.id = infoCarId;
    let infoCarTable = document.createElement('table');
    infoCarTable.id = 'info-car-table';
    let infoCarHeadTable = document.createElement('thead');
    let infoCarBodyTable = document.createElement('tbody');
    let infoCarButton = document.createElement('input');
    infoCarButton.id = 'info-car-button';
    infoCarButton.type = 'button';
    infoCarButton.value = 'Zamknij';
    infoCarHeadTable.innerHTML = `<thead>
                                     <tr>
                                        <th> marka</th>
                                        <th> model</th>
                                        <th> kolor</th>
                                        <th> typ nadwozia</th>
                                        <th> rok produkcji</th>
                                     </tr>
                                 </thead>`;
    fetch('http://localhost:8080/car/' + carId)
            .then(response => response.json())
            .then(car => {
                infoCarBodyTable.innerHTML = `<td> ${car.brand} </td>
                                              <td> ${car.model} </td>
                                              <td> ${car.color} </td>
                                              <td> ${car.bodyType.plDescription} </td>
                                              <td> ${car.yearOfProduction} </td>`;
            });

    infoCarTable.appendChild(infoCarHeadTable);
    infoCarTable.appendChild(infoCarBodyTable);
    infoCar.appendChild(infoCarTable);
    infoCar.appendChild(infoCarButton);
    insertElementAfter(infoCar, document.querySelector('#main-content'));
    infoCarButton.addEventListener('click', function() {infoCar.style.display = 'none'});
}