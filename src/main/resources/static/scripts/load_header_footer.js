const mainContent = document.querySelector("#main-content");
const headerTitle = "Wypożyczalnia samochodów SILESIA CARS";
const footerTitle = "Pomoc & Informacje";
const aboutUsFooterLink = document.createElement('a');
aboutUsFooterLink.href = "";
aboutUsFooterLink.text = 'O nas';
aboutUsFooterLink.id = 'aboutUs-footer-link';

const contactFooterLink = document.createElement('a');
contactFooterLink.href = "";
contactFooterLink.text = 'Kontakt';
contactFooterLink.id = 'contact-footer-link';

const mainSiteButton = document.createElement('a');
mainSiteButton.id = 'main-site-button';
mainSiteButton.href = '#';
mainSiteButton.className = 'btn btn-primary icon-save';
mainSiteButton.spa = 'glyphicon glyphicon-home';
mainSiteButton.text = 'Strona główna';

const redirectLoginButton = document.createElement('a');
redirectLoginButton.id = 'redirect-login-button';
redirectLoginButton.href = '#';
redirectLoginButton.className = 'btn btn-primary icon-save';
redirectLoginButton.text = 'Zaloguj się';

const configPanelButton = document.createElement('a');
configPanelButton.id = 'config-panel-button';
configPanelButton.href = '#';
configPanelButton.className = 'btn btn-primary icon-save';
configPanelButton.text = 'Panel admin';

loadImage("/images/rentcarservice-background-image.jpg", 1495, 400, mainContent);
loadHeader(mainContent, headerTitle, 'web-header');
loadFooter(mainContent, footerTitle, aboutUsFooterLink, contactFooterLink);

//loadButtonPanel(getElement('web-header'), mainSiteButton, redirectLoginButton, configPanelButton);

function loadHeader(referenceElement, headerText, idToSet) {
    let header = document.createElement('div');
    header.innerHTML = "<h1>" + headerText + "<h1>";
    header.id = idToSet;
    insertElementBefore(header, referenceElement);
}

function getElement(id) {
    if (id != '' && id !=null) {
        let element = document.getElementById(id);
        return element;
    }
}

function loadFooter(referenceElement, footerText, ...elements) {
    let footer = document.createElement('div');
    footer.innerHTML = "<h6>" + footerText + "</h6>" + "<br>";
    footer.id = 'web-footer';
    for (let i=0; i < elements.length; i++) {
        let breakLine = document.createElement('br');
        let element = elements[i];
        element.append(breakLine);
        footer.append(element);
    }
    insertElementAfter(footer, referenceElement);
}

function loadButtonPanel(beforeReferenceElement, ...elements) {
    let buttonPanel = document.createElement('div');
    buttonPanel.id = 'buttons-panel';
    for (let i=0; i < elements.length; i++) {
        buttonPanel.append(elements[i]);
    }
    insertElementBefore(buttonPanel, beforeReferenceElement);
}

function loadImage(url, width, height, beforeReferenceElement) {
    let imageContainer = document.createElement('div');
    imageContainer.innerHTML = "<img src=" + url + " width=" + width +  " height=" + height + ">";
    insertElementBefore(imageContainer, beforeReferenceElement);
}
