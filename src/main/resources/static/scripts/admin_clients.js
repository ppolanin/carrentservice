getClientsData();

function getClientsData() {
    fetch('http://localhost:8080/client/all')
        .then(response => response.json())
        .then(response => createTable(response));
}

function createTable(data) {
    $(document).ready(function() {
        let clientsTable = $('#clients-table').DataTable({
            "data": data,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Polish.json"
            },
            "columns": [
                {"data": "id"},
                {"data": "name"},
                {"data": "surname"},
                {"data": "username"},
                {"data": "contactDetails.country"},
                {"data": "contactDetails.city"},
                {"data": "contactDetails.street"},
                {"data": "contactDetails.addressNumber"},
                {"data": "contactDetails.zipCode"},
                {"data": "contactDetails.email"},
                {"data": "contactDetails.landlinePhoneNumber"},
                {"data": "contactDetails.mobilePhoneNumber"}
            ],
            "columnDefs": [{
                "targets": 12,
                "data": null,
                "render": function(data, type, row, meta) {
                   return '<button type="button" class="table-button" name="update-client-data" onclick="location.href=\'/client/update-form/' + row.id + '\'"> Edytuj </button> <button type="button" class="table-button" name="remove-client-data"> Usuń </button>';
                }
            }]
        });

        $('#clients-table tbody').on('click', 'button', function() {
        	let selectedTableRow = $(this).closest('tr');
        	let clientId = clientsTable.row(selectedTableRow).data().id;
          	let button = $(this).closest('button')[0];
        	if(button.name === 'remove-client-data') {
        	    displayConfirmationPopupWindowToRemoveData(clientsTable, button, clientId, 'Czy usunąć dane?', 'remove-client-data-popup-message');
        	}
        });
    });
}

function displayConfirmationPopupWindowToRemoveData(table, selectedButton, clientId, messageContent, className) {
    let popupWindowId = 'popup-window';
    let popupWindow = document.createElement('div');
    popupWindow.setAttribute('class', className);
    popupWindow.id = popupWindowId;
    let message = document.createElement('p');
    message.innerHTML = messageContent;
    let confirmationButton = document.createElement('input');
    confirmationButton.id = 'cancel-button';
    confirmationButton.type = 'button';
    confirmationButton.value = 'Potwierdź';
    let closeWindowButton = document.createElement('input');
    closeWindowButton.id = 'close-window-button';
    closeWindowButton.type = 'button';
    closeWindowButton.value = 'Zamknij';
    popupWindow.appendChild(message);
    popupWindow.appendChild(confirmationButton);
    popupWindow.appendChild(closeWindowButton);
    insertElementAfter(popupWindow, table.table().node());
    confirmationButton.addEventListener('click', function() {
        $.ajax({
                url: 'http://localhost:8080/client/remove/' + clientId,
                success: function() {
                    table.row(selectedButton.closest('tr')).remove();
                    table.draw();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log('remove client error {error code: ' + jqXHR.status + '; text status: ' + textStatus + '; error: ' + errorThrown + '}');
                }
        });
        removeElementById(popupWindowId);
    });
    closeWindowButton.addEventListener('click', function() {
        removeElementById(popupWindowId);
    });
}