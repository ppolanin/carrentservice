getLocation();

function getLocation() {
    fetch('http://localhost:8080/location/getAll')
        .then(response => response.json())
        .then(response => createTable(response));
}

function createTable(data) {
    $(document).ready(function() {
        let locationTable = $('#location-table').DataTable({
            "data": data,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Polish.json"
            },
            "columns": [
                {"data": "id"},
                {"data": "name"},
                {"data": "distance"},
                {"data": "costIdentifier.plDescription"},
                {"data": "cost"},
                {"data": "description"},
                {"data": "contactDetails.country"},
                {"data": "contactDetails.city"},
                {"data": "contactDetails.street"},
                {"data": "contactDetails.addressNumber"},
                {"data": "contactDetails.zipCode"},
                {"data": "contactDetails.landlinePhoneNumber"}
            ],
            "columnDefs": [{
                "targets": 12,
                "data": null,
                "render": function(data, type, row, meta) {
                   return '<button type="button" class="table-button" name="update-location" onclick="location.href=\'/location/update-form/' + row.id + '\'"> Edytuj </button> <button type="button" class="table-button" name="remove-location"> Usuń </button>';
                }
            }]
        });

        $('#location-table tbody').on('click', 'button', function() {
        	let selectedTableRow = $(this).closest('tr');
        	let locationId = locationTable.row(selectedTableRow).data().id;
          	let button = $(this).closest('button')[0];
        	if(button.name === 'remove-location') {
        	    displayConfirmationPopupWindowToRemoveData(locationTable, button, locationId, 'Czy usunąć dane?', 'remove-location-popup-message');
        	}
        });
    });
}

function displayConfirmationPopupWindowToRemoveData(table, selectedButton, locationId, messageContent, className) {
    let popupWindowId = 'popup-window';
    let popupWindow = document.createElement('div');
    popupWindow.setAttribute('class', className);
    popupWindow.id = popupWindowId;
    let message = document.createElement('p');
    message.innerHTML = messageContent;
    let confirmationButton = document.createElement('input');
    confirmationButton.id = 'cancel-button';
    confirmationButton.type = 'button';
    confirmationButton.value = 'Potwierdź';
    let closeWindowButton = document.createElement('input');
    closeWindowButton.id = 'close-window-button';
    closeWindowButton.type = 'button';
    closeWindowButton.value = 'Zamknij';
    popupWindow.appendChild(message);
    popupWindow.appendChild(confirmationButton);
    popupWindow.appendChild(closeWindowButton);
    insertElementAfter(popupWindow, table.table().node());
    confirmationButton.addEventListener('click', function() {
        $.ajax({
                url: 'http://localhost:8080/location/remove/' + locationId,
                success: function() {
                    table.row(selectedButton.closest('tr')).remove();
                    table.draw();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log('remove location error {error code: ' + jqXHR.status + '; text status: ' + textStatus + '; error: ' + errorThrown + '}');
                }
        });
        removeElementById(popupWindowId);
    });
    closeWindowButton.addEventListener('click', function() {
        removeElementById(popupWindowId);
    });
}