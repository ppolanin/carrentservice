package pl.polaninp.rentcarservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.polaninp.rentcarservice.model.CarRental;

public interface CarRentalRepository extends JpaRepository<CarRental, Long> {
}
