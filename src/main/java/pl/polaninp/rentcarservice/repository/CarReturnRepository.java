package pl.polaninp.rentcarservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.polaninp.rentcarservice.model.CarReturn;

public interface CarReturnRepository extends JpaRepository<CarReturn, Long> {
}
