package pl.polaninp.rentcarservice.repository;

import org.springframework.data.repository.CrudRepository;
import pl.polaninp.rentcarservice.model.Car;
import pl.polaninp.rentcarservice.model.CarBodyType;
import pl.polaninp.rentcarservice.model.CarStatus;

public interface CarRepository extends CrudRepository<Car, Long> {
    Iterable<Car> findCarsByBrand(String brand);

    Iterable<Car> findCarsByBrandAndCarStatus(String brand, CarStatus status);

    Iterable<Car> findCarsByBodyTypeAndCarStatus(CarBodyType bodyType, CarStatus status);

    Iterable<Car> findCarsByBrandAndBodyTypeAndCarStatus(String brand, CarBodyType bodyType, CarStatus status);

    Iterable<Car> findCarsByCarStatus(CarStatus status);
}
