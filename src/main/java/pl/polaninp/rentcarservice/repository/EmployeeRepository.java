package pl.polaninp.rentcarservice.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import pl.polaninp.rentcarservice.model.Employee;

import java.util.Optional;

public interface EmployeeRepository extends CrudRepository<Employee, Long> {
    Optional<Employee> findEmployeeByUsername(String username);
    @Query("SELECT e.id FROM Employee e where e.username = :username and e.password = :password")
    Long getClientIdByUsernameAndPassword(@Param("username") String username, @Param("password") String password);
}
