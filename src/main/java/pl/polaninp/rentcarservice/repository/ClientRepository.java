package pl.polaninp.rentcarservice.repository;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import pl.polaninp.rentcarservice.model.Client;

import java.util.Optional;

public interface ClientRepository extends CrudRepository<Client, Long> {

    Optional<Client> findClientByUsername(String username);

    @Query("SELECT c.id FROM Client c where c.username = :username and c.password = :password")
    Long getClientIdByUsernameAndPassword(@Param("username") String username, @Param("password") String password);

    boolean existsClientByNameAndSurnameAndUsername(String name, String surname, String username);

    boolean existsClientByUsername(String username);

}
