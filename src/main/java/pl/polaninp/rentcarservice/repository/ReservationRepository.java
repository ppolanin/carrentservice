package pl.polaninp.rentcarservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.polaninp.rentcarservice.model.Reservation;
import pl.polaninp.rentcarservice.model.ReservationStatus;

import java.time.LocalDateTime;

public interface ReservationRepository extends JpaRepository<Reservation, Long> {

    @Query("FROM Reservation r WHERE r.client.id = :clientId")
    Iterable<Reservation> getReservationsByClientId(@Param("clientId") Long clientId);

    @Query("FROM Reservation r WHERE r.car.id = :carId")
    Iterable<Reservation> getReservationByCarId(@Param("carId") Long carId);

    @Query("SELECT r.car.id FROM Reservation r WHERE r.reservationFromDate BETWEEN :startDate AND :endDate OR r.carAccessibilityDate BETWEEN :startDate AND :endDate " +
            "OR r.reservationFromDate <= :startDate AND r.carAccessibilityDate >= :endDate")
    Iterable<Long> getCarIdsByBookingDuration(@Param("startDate") LocalDateTime startDate, @Param("endDate") LocalDateTime endDate);

    @Query("FROM Reservation r WHERE r.reservationFromDate BETWEEN :startDate AND :endDate OR r.carAccessibilityDate BETWEEN :startDate AND :endDate " +
            "OR r.reservationFromDate <= :startDate AND r.carAccessibilityDate >= :endDate")
    Iterable<Reservation> getReservationsByBookingDuration(@Param("startDate") LocalDateTime startDate, @Param("endDate") LocalDateTime endDate);

    @Query("FROM Reservation r WHERE r.status = :status ORDER BY r.reservationFromDate")
    Iterable<Reservation> getReservationsByStatusAndOrderByStartReservationDate(@Param("status") ReservationStatus status);
}
