package pl.polaninp.rentcarservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.polaninp.rentcarservice.model.Location;

public interface LocationRepository extends JpaRepository<Location, Long> {

    @Query("SELECT l.description FROM Location l WHERE l.description = :description")
    Location getLocationByDescription(@Param("description") String description);
}
