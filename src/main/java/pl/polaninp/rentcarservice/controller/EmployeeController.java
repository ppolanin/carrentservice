package pl.polaninp.rentcarservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.polaninp.rentcarservice.model.Employee;
import pl.polaninp.rentcarservice.service.EmployeeService;

import javax.validation.Valid;
import java.util.Collection;

@Controller
public class EmployeeController {

    private final EmployeeService employeeService;
    private static final String REDIRECT_EMPLOYEES_PANEL_VIEW = "redirect:/admin/employees-panel";
    private static final String EMPLOYEES_PANEL_VIEW = "admin_employees_panel";
    private static final String ADD_EMPLOYEE_FORM_VIEW = "admin_add-employee";
    private static final String UPDATE_EMPLOYEE_FORM_VIEW = "admin_update-employee";

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @PostMapping("/admin/employee/add")
    public String addEmployee(@Valid Employee employee, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return ADD_EMPLOYEE_FORM_VIEW;
        } else {
            employeeService.addEmployee(employee);
            model.addAttribute("employees", employeeService.getEmployees());
            return REDIRECT_EMPLOYEES_PANEL_VIEW;
        }
    }

    @PostMapping("/admin/employee/add/all")
    @ResponseStatus(HttpStatus.CREATED)
    public void addEmployees(@RequestBody Collection<Employee> employees) {
        employeeService.addEmployees(employees);
    }

    @PostMapping("/admin/employee-test/add")
    @ResponseStatus(HttpStatus.CREATED)
    public void addEmployeeTest(@RequestBody Employee employee) {
        employeeService.addEmployee(employee);
    }

    @GetMapping("/admin/employee/all")
    @ResponseBody
    public Iterable<Employee> getEmployees() {
        return employeeService.getEmployees();
    }

    @GetMapping("/admin/employee/update-form/{id}")
    public String updateEmployeeFormView(@PathVariable Long id, Model model) {
        model.addAttribute("employee", employeeService.getEmployeeById(id));
        return UPDATE_EMPLOYEE_FORM_VIEW;
    }

    @PostMapping("/admin/employee/update/{id}")
    public String updateEmployee(@Valid Employee employee, BindingResult result) {
        if (result.hasErrors()) {
            return UPDATE_EMPLOYEE_FORM_VIEW;
        } else {
            employeeService.updateEmployee(employee);
            return REDIRECT_EMPLOYEES_PANEL_VIEW;
        }
    }

    @GetMapping("/admin/employee/remove/{id}")
    public String removeEmployee(@PathVariable Long id) {
        employeeService.removeEmployeeById(id);
        return REDIRECT_EMPLOYEES_PANEL_VIEW;
    }

    @GetMapping("/admin/employees-panel")
    public String adminEmployeePanelView(Model model) {
        model.addAttribute("employees", employeeService.getEmployees());
        return EMPLOYEES_PANEL_VIEW;
    }

    @GetMapping("/employee/authentication")
    @ResponseBody
    public Long authentication(Authentication authentication) {
        return employeeService.authentication(authentication);
    }

}
