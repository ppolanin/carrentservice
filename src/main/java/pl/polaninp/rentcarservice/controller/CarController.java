package pl.polaninp.rentcarservice.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pl.polaninp.rentcarservice.model.Car;
import pl.polaninp.rentcarservice.model.CarBodyType;
import pl.polaninp.rentcarservice.model.CarStatus;
import pl.polaninp.rentcarservice.service.CarService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/car")
public class CarController {

    private final CarService carService;

    @Autowired
    public CarController(CarService carService) {
        this.carService = carService;
    }

    @GetMapping
    public Iterable<Car> getAllCars() {
        return carService.getAllCars();
    }

    @GetMapping("{id}")
    public Car getCar(@PathVariable Long id) {
        return carService.getCarById(id);
    }

    @GetMapping("/byBrand/{brand}")
    public Iterable<Car> getCarsByBrand(@PathVariable String brand) {
        return carService.getCarsByBrand(brand);
    }

    @GetMapping("/byStatus/{status}")
    public Iterable<Car> getCarsByStatus(@PathVariable CarStatus status) {
        return carService.getCarsByStatus(status);
    }

    @GetMapping("/byBrandAndStatus/{brand}/{carStatus}")
    public Iterable<Car> getCarsByBrandAndStatus(@PathVariable String brand, @PathVariable CarStatus carStatus) {
        return carService.getCarsByBrandAndStatus(brand, carStatus);
    }

    @GetMapping("/byBodyTypeAndStatus/{bodyType}/{status}")
    public Iterable<Car> getCarsByBodyTypeAndStatus(@PathVariable CarBodyType bodyType, @PathVariable CarStatus status) {
        return carService.getCarsByBodyTypeAndStatus(bodyType, status);
    }

    @GetMapping("/byBrandAndBodyTypeAndStatus/{brand}/{bodyType}/{status}")
    public Iterable<Car> getCarsByBrandAndBodyTypeAndStatus(@PathVariable String brand, @PathVariable CarBodyType bodyType, @PathVariable CarStatus status) {
        return carService.getCarsByBrandAndBodyTypeAndStatus(brand, bodyType, status);
    }

    @GetMapping("/byBookingDuration/{startDate}/{endDate}")
    public Iterable<Car> getAvailableCarsByBookingDuration(@PathVariable String startDate, @PathVariable String endDate) {
        return carService.getAvailableCarsByBookingPeriod(startDate, endDate);
    }

    @GetMapping("/byBookingDurationAndBrand/{startDate}/{endDate}/{brand}")
    public Iterable<Car> getAvailableCarsByBookingPeriodAndBrand(@PathVariable String startDate, @PathVariable String endDate, @PathVariable String brand) {
        return carService.getAvailableCarsByBookingPeriodAndBrand(startDate, endDate, brand);
    }

    @GetMapping("/byBookingDurationAndBodyType/{startDate}/{endDate}/{bodyType}")
    public Iterable<Car> getAvailableCarsByBookingPeriodAndBodyType(@PathVariable String startDate, @PathVariable String endDate, @PathVariable CarBodyType bodyType) {
        return carService.getAvailableCarsByBookingPeriodAndBodyType(startDate, endDate, bodyType);
    }

    @GetMapping("/byBookingDurationAndBrandAndBodyType/{startDate}/{endDate}/{brand}/{bodyType}")
    public Iterable<Car> getAvailableCarsByBookingPeriodAndBrandAndBodyType(@PathVariable String startDate, @PathVariable String endDate, @PathVariable String brand, @PathVariable CarBodyType bodyType) {
        return carService.getAvailableCarsByBookingPeriodAndBrandAndBodyType(startDate, endDate, brand, bodyType);
    }

    @PostMapping("/add")
    public void addCar(@RequestBody Car car) {
        carService.addCar(car);
    }

    @PostMapping("/addAll")
    public void addCars(@RequestBody Iterable<Car> cars) {
        carService.addCars(cars);
    }

    @DeleteMapping("{id}")
    public void removeCar(@PathVariable Long id) {
        carService.removeCarById(id);
    }

    @PutMapping("{id}")
    public void updateCar(@PathVariable Long id, @Validated @RequestBody Car car) {
        carService.updateCar(id, car);
    }

    @GetMapping("/getBodyTypes")
    public CarBodyType[] getCarBodyTypes(@RequestParam(name = "sortBy", required = false) String type) {
        return carService.getCarBodyTypes(type);
    }

    @GetMapping("/getStatus")
    public CarStatus[] getCarStatus(@RequestParam(name = "sortBy") String langStatus) {
        return carService.getCarStatus(langStatus);
    }

}
