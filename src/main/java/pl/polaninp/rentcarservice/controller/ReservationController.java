package pl.polaninp.rentcarservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.polaninp.rentcarservice.model.Reservation;
import pl.polaninp.rentcarservice.service.ReservationService;

import java.util.List;

@Controller
@RequestMapping("/reservation")
public class ReservationController {

    private final ReservationService reservationService;
    private static final String INITIAL_RESERVATION_VIEW = "initial_reservation";
    private static final String REDIRECT_INITIAL_RESERVATION_VIEW = "redirect:/reservation/initial-view";
    private static final String RESERVATION_VIEW = "employee_reservation";
    private static final String NEW_RESERVATIONS_VIEW = "employee_new_reservations";
    private static final String RESERVATIONS_VIEW = "employee_reservations";
    private static final String REDIRECT_EMPLOYEE_PANEL = "redirect:/employee/panel";

    @Autowired
    public ReservationController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public void addReservation(@RequestParam String carId, @RequestParam String startDate, @RequestParam String pickupLocationId, @RequestParam String endDate, @RequestParam String returnLocationId, @RequestParam String clientId) {
        reservationService.createReservation(Long.valueOf(carId), Long.valueOf(clientId), startDate, Long.valueOf(pickupLocationId), endDate, Long.valueOf(returnLocationId));
    }

    @GetMapping("/initial")
    public String addInitialReservation(@RequestParam String carId, @RequestParam String startDate, @RequestParam String pickupLocationId, @RequestParam String endDate, @RequestParam String returnLocationId, Model model) {
        Reservation initialReservation = reservationService.createInitialReservation(Long.valueOf(carId), startDate, Long.valueOf(pickupLocationId), endDate, Long.valueOf(returnLocationId));
        model.addAttribute("initialReservation", initialReservation);
        model.addAttribute("reservationCosts", initialReservation.getCostCalculator().sumCosts());
        return INITIAL_RESERVATION_VIEW;
    }

    @GetMapping("/remove/{id}")
    @ResponseStatus(HttpStatus.OK)
    public String removeReservation(@PathVariable Long id) {
        reservationService.removeReservation(id);
        return REDIRECT_EMPLOYEE_PANEL;
    }

    @PostMapping("/cancel/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void cancelReservation(@PathVariable Long id) {
        reservationService.cancelReservation(id);
    }

    @PostMapping("/cancelByClient/{id}")
    @ResponseBody
    public boolean cancelReservationByClient(@PathVariable Long id) {
        return reservationService.cancelReservationByClient(id);
    }

    @GetMapping("/all")
    @ResponseBody
    public Iterable<Reservation> getReservations() {
        return reservationService.getAllReservations();
    }

    @GetMapping("/{id}")
    @ResponseBody
    public Reservation getReservation(@PathVariable Long id) {
        return reservationService.getReservationById(id);
    }

    @GetMapping("/client/{id}")
    @ResponseBody
    public Iterable<Reservation> getReservationsByClientId(@PathVariable Long id) {
        return reservationService.getReservationsByClientId(id);
    }

    @GetMapping("/car/{id}")
    @ResponseBody
    public Iterable<Reservation> getReservationsByCarId(@PathVariable Long id) {
        return reservationService.getReservationsByCarId(id);
    }

    @GetMapping("/car/{startReservationDate}/{finishReservationDate}")
    @ResponseBody
    public Iterable<Long> getCarIdsByBookingDuration(@PathVariable String startReservationDate, @PathVariable String finishReservationDate) {
        return reservationService.getCarIdsByBookingDuration(startReservationDate, finishReservationDate);
    }

    @GetMapping("/{startReservationDate}/{finishReservationDate}")
    @ResponseBody
    public Iterable<Reservation> getCarReservationByBookingDuration(@PathVariable String startReservationDate, @PathVariable String finishReservationDate) {
        return reservationService.getReservationsByBookingDuration(startReservationDate, finishReservationDate);
    }

    @GetMapping("/all-new-view")
    public String getNewReservationsByStartDateView(Model model) {
        model.addAttribute("reservations", reservationService.getNewReservationsOrderByStartReservationDate());
        return NEW_RESERVATIONS_VIEW;
    }

    @GetMapping("/all-view")
    public String getReservations(Model model) {
        model.addAttribute("reservations", reservationService.getAllReservations());
        return RESERVATIONS_VIEW;
    }

    @GetMapping("/edit-view")
    public String getReservationToView(@RequestParam(name = "id") Long id, @RequestParam(name = "lang", defaultValue = "en") String lang, Model model) {
        model.addAttribute("reservation", reservationService.getReservationById(id));
        model.addAttribute("reservationCosts", reservationService.getBasicReservationCosts(id, lang));
        return RESERVATION_VIEW;
    }

    @GetMapping("/costs")
    @ResponseBody
    public List<String> getReservationCosts(@RequestParam(name = "id") Long id, @RequestParam(name = "lang", defaultValue = "en") String lang) {
        return reservationService.getBasicReservationCosts(id, lang);
    }

}
