package pl.polaninp.rentcarservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.polaninp.rentcarservice.model.CarReturn;
import pl.polaninp.rentcarservice.service.CarReturnService;

@Controller
@RequestMapping("/return")
public class CarReturnController {

    private final CarReturnService carReturnService;
    private static final String RETURNS_VIEW = "employee_returns";
    private static final String RETURN_VIEW = "employee_return";

    @Autowired
    public CarReturnController(CarReturnService carReturnService) {
        this.carReturnService = carReturnService;
    }

    @PostMapping("/add")
    @ResponseStatus(HttpStatus.CREATED)
    public void addReturn(@RequestParam(name = "date") String date, @RequestParam(name = "employeeId") Long employeeId, @RequestParam(name = "rentalId") Long rentalId, @RequestParam(name = "remark", required = false, defaultValue = "") String remark) {
        carReturnService.addReturn(date, employeeId, rentalId, remark);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public CarReturn getReturn(@PathVariable Long id) {
        return carReturnService.getReturnById(id);
    }

    @GetMapping("/all")
    @ResponseStatus(HttpStatus.OK)
    public Iterable<CarReturn> getReturns() {
        return carReturnService.getReturns();
    }

    @GetMapping("/remove/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void removeReturn(@PathVariable Long id) {
        carReturnService.removeReturnById(id);
    }

    @GetMapping("/all-view")
    public String returnsView(Model model) {
        model.addAttribute("carReturns", carReturnService.getReturns());
        return RETURNS_VIEW;
    }

    @GetMapping("/edit-view/{id}")
    public String getReturnToView(@PathVariable Long id, Model model) {
        model.addAttribute("carReturn", carReturnService.getReturnById(id));
        return RETURN_VIEW;
    }

}
