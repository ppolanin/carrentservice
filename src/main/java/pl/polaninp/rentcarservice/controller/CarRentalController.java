package pl.polaninp.rentcarservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.polaninp.rentcarservice.model.CarRental;
import pl.polaninp.rentcarservice.service.CarRentalService;

@Controller
@RequestMapping("/rental")
public class CarRentalController {

    private final CarRentalService carRentalService;
    private static final String RENTAL_VIEW = "employee_rental";
    private static final String RENTALS_VIEW = "employee_rentals";

    @Autowired
    public CarRentalController(CarRentalService carRentalService) {
        this.carRentalService = carRentalService;
    }

    @PostMapping("/add")
    @ResponseStatus(HttpStatus.CREATED)
    public void addRental(@RequestParam(name = "date") String date, @RequestParam(name = "employeeId") Long employeeId, @RequestParam(name = "reservationId") Long reservationId, @RequestParam(name = "remark", required = false, defaultValue = "") String remark) {
        carRentalService.addRental(date, employeeId, reservationId, remark);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public CarRental getRental(@PathVariable Long id) {
        return carRentalService.getRentalById(id);
    }

    @GetMapping("/all")
    @ResponseStatus(HttpStatus.OK)
    public Iterable<CarRental> getRentals() {
        return carRentalService.getRentals();
    }

    @GetMapping("/remove/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void removeRental(@PathVariable Long id) {
        carRentalService.removeRentalById(id);
    }

    @GetMapping("/all-view")
    public String rentalsView(Model model) {
        model.addAttribute("carRentals", carRentalService.getRentals());
        return RENTALS_VIEW;
    }

    @GetMapping("/edit-view/{id}")
    public String getRentalToView(@PathVariable Long id, Model model) {
        model.addAttribute("carRental", carRentalService.getRentalById(id));
        return RENTAL_VIEW;
    }

}
