package pl.polaninp.rentcarservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.polaninp.rentcarservice.model.Client;
import pl.polaninp.rentcarservice.model.PasswordDTO;
import pl.polaninp.rentcarservice.service.ClientService;

import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("/client")
public class ClientController {

    private final ClientService clientService;
    private static final String UPDATE_CLIENT_FORM_VIEW = "client_update_data";
    private static final String CLIENT_DATA_VIEW = "client_data";
    private static final String CLIENT_DATA_ADMIN_VIEW = "admin_clients_panel";

    @Autowired
    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping("/all")
    @ResponseBody
    public Iterable<Client> getClients() {
        return clientService.getClients();
    }

    @GetMapping("/all-test")
    public String getClientsTest(Model model) {
        model.addAttribute("clients", clientService.getClients());
        return CLIENT_DATA_ADMIN_VIEW;
    }

    @GetMapping("{id}")
    @ResponseBody
    public Client getClient(@PathVariable Long id) {
        return clientService.getClientById(id);
    }

    @GetMapping("{name}/{surname}/{username}")
    @ResponseBody
    public boolean existsClientByNameAndSurnameAndUsername(@PathVariable String name, @PathVariable String surname, @PathVariable String username) {
        return clientService.existsClientByNameAndSurnameAndUsername(name, surname, username);
    }

    @PostMapping("/login-authentication")
    @ResponseBody
    public Long login(Authentication authentication) {
        return clientService.authentication(authentication);
    }

    @GetMapping("/authentication")
    @ResponseBody
    public Long authentication(Authentication authentication) {
        return clientService.authentication(authentication);
    }

    @GetMapping("/register/{username}")
    @ResponseBody
    public boolean existsUsername(@PathVariable String username) {
        return clientService.existsUsername(username);
    }

    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    public void addClient(@RequestBody Client client) {
        clientService.addClient(client);
    }

    @PostMapping("/register/all")
    @ResponseStatus(HttpStatus.CREATED)
    public void addClients(@RequestBody Collection<Client> clients) {
        clientService.addClients(clients);
    }

    @GetMapping("/remove/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void removeClient(@PathVariable Long id) {
        clientService.removeClientById(id);
    }

    @GetMapping("/update-form/{id}")
    public String updateClientFormView(@PathVariable Long id, Model model) {
        model.addAttribute("client", clientService.getClientById(id));
        return UPDATE_CLIENT_FORM_VIEW;
    }

    @PostMapping("/update/{id}")
    public String updateClient(@Valid Client client, BindingResult results, Authentication authentication) {
        if (results.hasFieldErrors()) {
            return UPDATE_CLIENT_FORM_VIEW;
        } else {
            clientService.updateClient(client);
            UserDetails userDetails = (UserDetails) authentication.getPrincipal();
            if (userDetails.getUsername().equals(client.getUsername())) {
                return CLIENT_DATA_VIEW;
            }
        }
        return CLIENT_DATA_ADMIN_VIEW;
    }

    @PostMapping("/changePassword")
    @ResponseBody
    public boolean changePassword(Authentication authentication, @RequestBody PasswordDTO passwordDTO) {
        return clientService.changePassword(authentication, passwordDTO);
    }

    @GetMapping("/view")
    public String getView(Authentication authentication) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        for (GrantedAuthority a : userDetails.getAuthorities()) {
            if (a.getAuthority().equals("ROLE_ADMIN")) {
                return CLIENT_DATA_ADMIN_VIEW;
            }
        }
        return CLIENT_DATA_VIEW;
    }

}
