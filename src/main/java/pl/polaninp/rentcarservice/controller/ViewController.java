package pl.polaninp.rentcarservice.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.polaninp.rentcarservice.model.Employee;

@Controller
public class ViewController {

    @GetMapping("/client/login")
    public String loginView() {
        return "login_form";
    }

    @GetMapping("/main")
    public String mainView() {
        return "index";
    }

    @GetMapping("/client/account")
    public String clientAccountView() {
        return "client_account";
    }

    @GetMapping("/client/reservations")
    public String clientReservationsView() {
        return "client_reservations";
    }

    @GetMapping("/client/data")
    public String clientDataView() {
        return "client_data";
    }

    @GetMapping("/register")
    public String registerView() {
        return "registration_form";
    }

    @GetMapping("/admin/panel")
    public String adminView() {
        return "admin_panel";
    }

    @GetMapping("/admin/clients-panel")
    public String adminClientPanelView() {
        return "admin_clients_panel.html";
    }

    @GetMapping("/admin/cars-panel")
    public String adminCarPanelView() {
        return "admin_cars_panel";
    }

    @GetMapping("/admin/add-employee")
    public String adminAddEmployeeView(Employee employee, Model model) {
        model.addAttribute("employee", employee);
        return "admin_add-employee";
    }

    @GetMapping("/admin/location-panel")
    public String locationView() {
        return "admin_location_panel";
    }

    @GetMapping(value = "/admin/login")
    public String adminLoginView() {
        return "admin_form";
    }

    @GetMapping("/employee/login")
    public String employeeLoginView() {
        return "employee_form";
    }

    @GetMapping("/employee/panel")
    public String employeeAccountView() {
        return "employee_panel";
    }

}
