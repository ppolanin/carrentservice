package pl.polaninp.rentcarservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.polaninp.rentcarservice.model.Location;
import pl.polaninp.rentcarservice.service.LocationServiceImpl;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;

@Controller
@RequestMapping("/location")
public class LocationController {

    private final LocationServiceImpl locationService;
    private static final String ADD_LOCATION_FORM_VIEW = "location_create";
    private static final String UPDATE_LOCATION_FORM_VIEW = "location_update";
    private static final String ADMIN_LOCATION_PANEL_VIEW = "admin_location_panel";
    private static final String MAIN_VIEW = "index";

    @Autowired
    public LocationController(LocationServiceImpl locationService) {
        this.locationService = locationService;
    }

    @GetMapping("/add")
    public String addLocationView(Location location) {
        return ADD_LOCATION_FORM_VIEW;
    }

    @PostMapping("/add")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String addLocation(@Valid Location location, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ADD_LOCATION_FORM_VIEW;
        }
        locationService.addLocation(location);
        return ADMIN_LOCATION_PANEL_VIEW;
    }

    @PostMapping("/add/all")
    @ResponseStatus(HttpStatus.OK)
    public void addLocations(@RequestBody Collection<@Valid Location> locations) {
        locationService.addLocations(locations);
    }

    @GetMapping("/update-form/{id}")
    public String updateLocationView(@PathVariable Long id, Model model) {
        model.addAttribute("location", locationService.getLocationById(id));
        return UPDATE_LOCATION_FORM_VIEW;
    }

    @PostMapping("/update/{id}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String updateLocation(@Valid Location location, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return UPDATE_LOCATION_FORM_VIEW;
        }
        locationService.updateLocation(location);
        return ADMIN_LOCATION_PANEL_VIEW;
    }

    @GetMapping("/remove/{id}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void removeLocation(@PathVariable Long id) {
        locationService.removeLocation(id);
    }

    @GetMapping("/get/{id}")
    @ResponseBody
    public Location getLocationById(@PathVariable Long id) {
        return locationService.getLocationById(id);
    }

    @GetMapping("/getByDescription/{description}")
    @ResponseBody
    public Location getLocationByDescription(@PathVariable String description) {
        return locationService.getLocationByDescription(description);
    }

    @GetMapping("/getAll")
    @ResponseBody
    public List<Location> getLocations() {
        return locationService.getLocations();
    }

}
