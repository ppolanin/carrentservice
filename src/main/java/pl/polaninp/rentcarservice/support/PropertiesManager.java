package pl.polaninp.rentcarservice.support;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Optional;
import java.util.Properties;

public class PropertiesManager {

    public PropertiesManager() {
    }

    public Optional<String> getAttributeByKey(String path, String key) throws IOException {
        Properties properties = new Properties();
        properties.load(new FileReader(path));
        return Optional.ofNullable(properties.getProperty(key));
    }

    public void saveAttribute(String path, String key, String value) throws IOException {
        Properties properties = new Properties();
        properties.load(new FileReader(path));
        properties.put(key, value);
        properties.store(new FileWriter(path), "");
    }

    public void saveAttributeAsKey(String path, String value) throws IOException {
        String key = createKeyFromValue(value);
        Properties properties = new Properties();
        properties.load(new FileReader(path));
        properties.setProperty(key, value);
        properties.store(new FileWriter(path), "");
    }

    public String createKeyFromValue(String value) {
        String key = value.toUpperCase();
        if (key.contains(" ")) {
            key = key.replace(" ", "_");
        }
        return key;
    }

}
