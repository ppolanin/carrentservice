package pl.polaninp.rentcarservice.model;

import com.fasterxml.jackson.annotation.JsonValue;

public enum CarRentalStatus {
    NEW("nowe"), CONFIRMED("potwierdzone");

    final String description;

    CarRentalStatus(String description) {
        this.description = description;
    }

    @JsonValue
    public String getDescription() {
        return description;
    }

}
