package pl.polaninp.rentcarservice.model;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum CarBodyType {
    CABRIOLET("cabriolet", "kabriolet"),
    COUPE("coupe", "coupe"),
    HATCHBACK("hatchback", "hatchback"),
    SEDAN("sedan","sedan"),
    SUV("suv", "suv"),
    MINIVAN("minivan", "minivan"),
    PICKUP("pickup", "pickup"),
    OFF_ROAD("4x4", "terenowy 4x4"),
    ESTATE_WAGON("estate wagon", "kombi");

    private final String enDescription;
    private final String plDescription;
    private final String value;

    CarBodyType(String enDescription, String plDescription) {
        this.enDescription = enDescription;
        this.plDescription = plDescription;
        this.value = this.toString();
    }

    public String getPlDescription() {
        return plDescription;
    }

    public String getEnDescription() {
        return enDescription;
    }

    public String getValue() {
        return value;
    }
}
