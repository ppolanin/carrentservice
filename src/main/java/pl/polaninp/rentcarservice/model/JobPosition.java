package pl.polaninp.rentcarservice.model;

import com.fasterxml.jackson.annotation.JsonValue;

public enum JobPosition {
    EMPLOYEE("pracownik"), MANAGER("menedżer");

    private final String position;

    JobPosition(String position) {
        this.position = position;
    }

    @JsonValue
    public String getPosition() {
        return position;
    }
}
