package pl.polaninp.rentcarservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.stream.Collectors;

@Entity
public class Client implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotEmpty
    @NotNull
    @Pattern(regexp = "^[^\\-|\\s][\\p{L}\\-\\s]{1,24}$(?<!-|\\s)")
    private String name;
    @NotEmpty
    @NotNull
    @Pattern(regexp = "^[^\\-|\\s][\\p{L}\\-\\s]{1,24}$(?<!-|\\s)")
    private String surname;
    @NotEmpty
    @NotNull
    @Pattern(regexp = "^[a-zA-Z\\.\\_\\-\\d]{2,24}$")
    private String username;
    @NotEmpty
    @NotNull
    @Pattern(regexp = "(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*-_])(?=\\S+$).{8,}")
    private String password;
    @Valid
    @OneToOne(mappedBy = "client", cascade = CascadeType.ALL, orphanRemoval = true)
    ClientContactDetails contactDetails;
    @ElementCollection(fetch = FetchType.EAGER)
    private Collection<String> roles = new HashSet<>();
    public final static String ROLE_CLIENT = "ROLE_CLIENT";

    public Client(@NotEmpty @NotNull String name, @NotEmpty @NotNull String surname, @NotEmpty @NotNull String username, @NotEmpty @NotNull String password) {
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.password = password;
    }

    public Client() {
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRoles(Collection<String> roles) {
        this.roles = roles;
    }

    public void setContactDetails(ClientContactDetails contactDetails) {
        if (contactDetails == null) {
            if (this.contactDetails != null) {
                this.contactDetails.setClient(null);
            }
        } else {
            contactDetails.setClient(this);
        }
        this.contactDetails = contactDetails;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public ContactDetails getContactDetails() {
        return contactDetails;
    }

    @JsonIgnore
    public Collection<String> getRoles() {
        return roles;
    }

    @JsonIgnore
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles.stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
    }

    @JsonProperty
    @JsonIgnore
    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Client)) return false;
        Client client = (Client) o;
        return Objects.equals(id, client.id) &&
                name.equals(client.name) &&
                surname.equals(client.surname) &&
                username.equals(client.username) &&
                password.equals(client.password) &&
                contactDetails.equals(client.contactDetails);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, surname, username, password, contactDetails);
    }

    @Override
    public String toString() {
        StringBuilder clientStringBuilder = new StringBuilder("client [id: ");
        clientStringBuilder.append(id)
                .append(" name: ").append(name)
                .append(" surname: ").append(surname)
                .append(" username: ").append(username)
                .append(" password: ").append(password)
                .append(" roles: ").append(roles);
        if (contactDetails == null) {
            return clientStringBuilder.append("]").toString();
        } else {
            return clientStringBuilder.append(" ").append(contactDetails.toString()).append("]").toString();
        }
    }
}
