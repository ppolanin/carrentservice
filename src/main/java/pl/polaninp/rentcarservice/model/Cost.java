package pl.polaninp.rentcarservice.model;

public interface Cost {
    CostIdentifier getCostIdentifier();
    double getCost();
}
