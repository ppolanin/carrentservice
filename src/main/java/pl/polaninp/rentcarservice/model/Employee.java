package pl.polaninp.rentcarservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.stream.Collectors;

@Entity
public class Employee implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    @Pattern(regexp = "^[^\\-|\\s][\\p{L}\\-\\s]{1,24}$(?<!-|\\s)")
    private String name;
    @NotNull
    @Pattern(regexp = "^[^\\-|\\s][\\p{L}\\-\\s]{1,24}$(?<!-|\\s)")
    private String surname;
    @Enumerated(EnumType.STRING)
    @NotNull
    private JobPosition jobPosition;
    private String username;
    @NotNull
    @Pattern(regexp = "(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*-_])(?=\\S+$).{8,}")
    private String password;
    @Valid
    @OneToOne(mappedBy = "employee", cascade = CascadeType.ALL, orphanRemoval = true)
    private EmployeeContactDetails contactDetails;
    @ElementCollection(fetch = FetchType.EAGER)
    Collection<String> roles = new HashSet<>();
    public static final String ROLE_EMPLOYEE = "ROLE_EMPLOYEE";

    public Employee(@NotNull @Pattern(regexp = "^[^\\-|\\s][\\p{L}\\-\\s]{1,24}$(?<!-|\\s)") String name, @NotNull @Pattern(regexp = "^[^\\-|\\s][\\p{L}\\-\\s]{1,24}$(?<!-|\\s)") String surname, @NotNull JobPosition jobPosition, @NotNull @Pattern(regexp = "(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*-_])(?=\\S+$).{8,}") String password) {
        this.name = name;
        this.surname = surname;
        this.jobPosition = jobPosition;
        this.password = password;
    }

    public Employee() {
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setJobPosition(JobPosition jobPosition) {
        this.jobPosition = jobPosition;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setContactDetails(EmployeeContactDetails contactDetails) {
        if (contactDetails == null) {
            if (this.contactDetails != null) {
                this.contactDetails.setEmployee(null);
            }
        } else {
            contactDetails.setEmployee(this);
        }
        this.contactDetails = contactDetails;
    }

    public void setRoles(Collection<String> roles) {
        this.roles = roles;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public JobPosition getJobPosition() {
        return jobPosition;
    }

    public EmployeeContactDetails getContactDetails() {
        return contactDetails;
    }

    @JsonIgnore
    public Collection<String> getRoles() {
        return roles;
    }

    @JsonIgnore
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles.stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
    }

    @JsonProperty
    @JsonIgnore
    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Employee)) return false;
        Employee employee = (Employee) o;
        return Objects.equals(id, employee.id) &&
                name.equals(employee.name) &&
                surname.equals(employee.surname) &&
                jobPosition == employee.jobPosition &&
                username.equals(employee.username) &&
                password.equals(employee.password) &&
                contactDetails.equals(employee.contactDetails);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, surname, jobPosition, username, password, contactDetails);
    }

    @Override
    public String toString() {
        StringBuilder employeeStringBuilder = new StringBuilder("employee [id: ");
        employeeStringBuilder.append(id)
                .append(" name: ").append(name)
                .append(" surname: ").append(surname)
                .append(" job position: ").append(jobPosition)
                .append(" username: ").append(username)
                .append(" password: ").append(password)
                .append(" roles: ").append(roles);
        if (contactDetails == null) {
            return employeeStringBuilder.append("]").toString();
        } else {
            return employeeStringBuilder.append(" ").append(contactDetails.toString()).append("]").toString();
        }
    }
}
