package pl.polaninp.rentcarservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

@Entity
public class Reservation implements Cost {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    private LocalDateTime reservationDate;
    @ManyToOne(fetch = FetchType.EAGER)
    @NotNull
    @JoinColumn(name = "client_id")
    private Client client;
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE})
    @NotNull
    @JoinColumn(name = "car_id")
    private Car car;
    @NotNull
    private LocalDateTime reservationFromDate;
    @NotNull
    private LocalDateTime reservationToDate;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "p_car_location_id")
    @NotNull
    private Location pickupCarLocation;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "r_car_location_id")
    @NotNull
    private Location returnCarLocation;
    @NotNull
    private LocalDateTime carAccessibilityDate;
    private double cost;
    private double costs;
    @NotNull
    @Enumerated(EnumType.STRING)
    private ReservationStatus status;
    @Transient
    private final CostCalculator costCalculator = new CostCalculator();
    @Transient
    private final CostIdentifier carReservationCostIdentifier = CostIdentifier.CAR_RESERVATION;
    private static final Logger LOGGER = LoggerFactory.getLogger(Reservation.class);

    public Reservation(@NotNull LocalDateTime reservationDate, @NotNull Client client, @NotNull Car car, @NotNull LocalDateTime reservationFromDate, @NotNull Location pickupCarLocation, @NotNull LocalDateTime reservationToDate, @NotNull Location returnCarLocation, @NotNull ReservationStatus status) {
        this.reservationDate = reservationDate;
        this.client = client;
        this.car = car;
        this.reservationFromDate = reservationFromDate;
        this.pickupCarLocation = pickupCarLocation;
        this.reservationToDate = reservationToDate;
        this.returnCarLocation = returnCarLocation;
        this.carAccessibilityDate = setCarAccessibilityDate();
        this.cost = calculateReservationFee();
        this.status = status;
        calculateCosts();
    }

    public Reservation(@NotNull Car car, @NotNull LocalDateTime reservationFromDate, @NotNull Location pickupCarLocation, @NotNull LocalDateTime reservationToDate, @NotNull Location returnCarLocation) {
        this.car = car;
        this.reservationFromDate = reservationFromDate;
        this.pickupCarLocation = pickupCarLocation;
        this.reservationToDate = reservationToDate;
        this.returnCarLocation = returnCarLocation;
        this.carAccessibilityDate = setCarAccessibilityDate();
        this.cost = calculateReservationFee();
        calculateCosts();
    }

    public Reservation() {
    }

    public Long getId() {
        return id;
    }

    public LocalDateTime getReservationDate() {
        return reservationDate;
    }

    public void setReservationDate(LocalDateTime reservationDate) {
        this.reservationDate = reservationDate;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public LocalDateTime getReservationFromDate() {
        return reservationFromDate;
    }

    public void setReservationFromDate(LocalDateTime reservationFromDate) {
        this.reservationFromDate = reservationFromDate;
    }

    public LocalDateTime getReservationToDate() {
        return reservationToDate;
    }

    public void setReservationToDate(LocalDateTime reservationToDate) {
        this.reservationToDate = reservationToDate;
    }

    @Override
    public double getCost() {
        return cost;
    }

    public double getCosts() {
        return costs;
    }

    public ReservationStatus getStatus() {
        return status;
    }

    public void setStatus(ReservationStatus status) {
        this.status = status;
    }

    public int getDays() {
        return reservationToDate.minus(reservationFromDate.getLong(ChronoField.DAY_OF_YEAR), ChronoUnit.DAYS).get(ChronoField.DAY_OF_YEAR);
    }

    private double calculateReservationFee() {
        return Math.round(getDays() * car.getDailyFee() * 100.00) / 100.00;
    }

    private void calculateCosts() {
        if (pickupCarLocation.equals(returnCarLocation)) {
            try {
                returnCarLocation = (Location) pickupCarLocation.clone();
            } catch (CloneNotSupportedException exception) {
                LOGGER.error(exception.getMessage(), exception);
            }
        }
        pickupCarLocation.setLocationType(LocationType.PICK_UP);
        returnCarLocation.setLocationType(LocationType.RETURN);
        costCalculator.addCost(this);
        costCalculator.addCost(pickupCarLocation);
        costCalculator.addCost(returnCarLocation);
        this.costs = costCalculator.sumCosts();
    }

    public void calculateExtraCosts(ExtraCost... extraCosts) {
        for (ExtraCost ec : extraCosts) {
            this.costCalculator.addCost(ec);
        }
        this.costs = costCalculator.sumCosts();
    }

    private LocalDateTime setCarAccessibilityDate() {
        return reservationToDate.plusDays(1).minusMinutes(1);
    }

    public LocalDateTime getCarAccessibilityDate() {
        return carAccessibilityDate;
    }

    public Location getPickupCarLocation() {
        return pickupCarLocation;
    }

    public void setPickupCarLocation(Location pickupCarLocation) {
        this.pickupCarLocation = pickupCarLocation;
    }

    public Location getReturnCarLocation() {
        return returnCarLocation;
    }

    public void setReturnCarLocation(Location returnCarLocation) {
        this.returnCarLocation = returnCarLocation;
    }

    @JsonIgnore
    public CostCalculator getCostCalculator() {
        calculateCosts();
        return costCalculator;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Reservation)) return false;
        Reservation reservation = (Reservation) o;
        return Double.compare(reservation.cost, cost) == 0 &&
                Objects.equals(id, reservation.id) &&
                Objects.equals(reservationDate, reservation.reservationDate) &&
                Objects.equals(client, reservation.client) &&
                Objects.equals(car, reservation.car) &&
                Objects.equals(reservationFromDate, reservation.reservationFromDate) &&
                Objects.equals(reservationToDate, reservation.reservationToDate) &&
                Objects.equals(pickupCarLocation, reservation.pickupCarLocation) &&
                Objects.equals(returnCarLocation, reservation.returnCarLocation) &&
                Objects.equals(carAccessibilityDate, reservation.carAccessibilityDate) &&
                status == reservation.status &&
                Objects.equals(costCalculator, reservation.costCalculator);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, reservationDate, client, car, reservationFromDate, reservationToDate, pickupCarLocation, returnCarLocation, carAccessibilityDate, cost, status, costCalculator);
    }

    @Override
    public String toString() {
        StringBuilder reservationStringBuilder = new StringBuilder("reservation [id: ");
        return reservationStringBuilder.append(id)
                .append(" reservation date: ").append(reservationDate)
                .append(" status: ").append(status)
                .append(" from date: ").append(reservationFromDate)
                .append(" to date: ").append(reservationToDate)
                .append(" pick up car location: ").append(pickupCarLocation)
                .append(" return car location: ").append(returnCarLocation)
                .append(" costs: ").append(costCalculator.sumCosts())
                .append(" ").append(client)
                .append(" ").append(car)
                .append(" car accessibility date: ").append(carAccessibilityDate)
                .append("]").toString();
    }

    @Override
    public CostIdentifier getCostIdentifier() {
        return carReservationCostIdentifier;
    }

}
