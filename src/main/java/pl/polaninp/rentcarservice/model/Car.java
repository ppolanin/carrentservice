package pl.polaninp.rentcarservice.model;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    @NotEmpty
    private String brand;
    @NotNull
    @NotEmpty
    private String model;
    @NotNull
    @Enumerated(EnumType.STRING)
    private CarBodyType bodyType;
    private int yearOfProduction;
    @NotNull
    @NotEmpty
    private String color;
    private int mileage;
    @NotNull
    @Enumerated(EnumType.STRING)
    private CarStatus carStatus;
    private double dailyFee;

    public Car(@NotNull @NotEmpty String brand, @NotNull @NotEmpty String model, @NotNull CarBodyType bodyType, int yearOfProduction, @NotNull @NotEmpty String color, double dailyFee) {
        this.brand = brand;
        this.model = model;
        this.bodyType = bodyType;
        this.yearOfProduction = yearOfProduction;
        this.color = color;
        this.dailyFee = dailyFee;
    }

    public Car(@NotNull @NotEmpty String brand, @NotNull @NotEmpty String model, CarBodyType bodyType, int yearOfProduction, @NotNull @NotEmpty String color, int mileage, CarStatus carStatus, double dailyFee) {
        this.brand = brand;
        this.model = model;
        this.bodyType = bodyType;
        this.yearOfProduction = yearOfProduction;
        this.color = color;
        this.mileage = mileage;
        this.carStatus = carStatus;
        this.dailyFee = dailyFee;
    }

    public Car() {
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setBodyType(CarBodyType bodyType) {
        this.bodyType = bodyType;
    }

    public void setYearOfProduction(int yearOfProduction) {
        this.yearOfProduction = yearOfProduction;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setDailyFee(double dailyFee) {
        this.dailyFee = dailyFee;
    }

    public void setMileage(int mileage) {
        this.mileage = mileage;
    }

    public void setCarStatus(CarStatus carStatus) {
        this.carStatus = carStatus;
    }

    public Long getId() {
        return id;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public CarBodyType getBodyType() {
        return bodyType;
    }

    public int getYearOfProduction() {
        return yearOfProduction;
    }

    public String getColor() {
        return color;
    }

    public Integer getMileage() {
        return mileage;
    }

    public CarStatus getCarStatus() {
        return carStatus;
    }

    public double getDailyFee() {
        return dailyFee;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Car)) return false;
        Car car = (Car) o;
        return yearOfProduction == car.yearOfProduction &&
                mileage == car.mileage &&
                Double.compare(car.dailyFee, dailyFee) == 0 &&
                Objects.equals(id, car.id) &&
                brand.equals(car.brand) &&
                model.equals(car.model) &&
                bodyType == car.bodyType &&
                color.equals(car.color) &&
                carStatus == car.carStatus;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, brand, model, bodyType, yearOfProduction, color, mileage, carStatus, dailyFee);
    }

    @Override
    public String toString() {
        StringBuilder carStringBuilder = new StringBuilder("car [id: ");
        return carStringBuilder.append(id)
                .append(" brand: ").append(brand)
                .append(" model: ").append(model)
                .append(" car body: ").append(bodyType)
                .append(" year of production: ").append(yearOfProduction)
                .append(" color: ").append(color)
                .append(" daily fee: ").append(dailyFee)
                .append(" mileage: ").append(mileage)
                .append(" car status: ").append(carStatus)
                .append("]").toString();
    }
}
