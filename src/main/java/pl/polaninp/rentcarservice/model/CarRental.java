package pl.polaninp.rentcarservice.model;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
public class CarRental {
    @Id
    private Long id;
    @NotNull
    private LocalDateTime rentalDate;
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id")
    private Employee employee;
    @NotNull
    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.REMOVE})
    @MapsId
    @JoinColumn(name = "id", nullable = false, unique = true)
    private Reservation reservation;
    @NotNull
    private String remark;
    @NotNull
    @Enumerated(EnumType.STRING)
    private CarRentalStatus status;

    public CarRental(@NotNull LocalDateTime rentalDate, @NotNull Employee employee, @NotNull Reservation reservation, @NotNull String remark, @NotNull CarRentalStatus status) {
        this.rentalDate = rentalDate;
        this.employee = employee;
        this.reservation = reservation;
        this.remark = remark;
        this.status = status;
    }

    public CarRental() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getRentalDate() {
        return rentalDate;
    }

    public void setRentalDate(LocalDateTime rentalDate) {
        this.rentalDate = rentalDate;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Reservation getReservation() {
        return reservation;
    }

    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public CarRentalStatus getStatus() {
        return status;
    }

    public void setStatus(CarRentalStatus status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CarRental)) return false;
        CarRental carRental = (CarRental) o;
        return Objects.equals(id, carRental.id) &&
                rentalDate.equals(carRental.rentalDate) &&
                employee.equals(carRental.employee) &&
                reservation.equals(carRental.reservation) &&
                remark.equals(carRental.remark) &&
                status == carRental.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, rentalDate, employee, reservation, remark, status);
    }

    @Override
    public String toString() {
        StringBuilder rentalStringBuilder = new StringBuilder("car rental [id: ");
        return rentalStringBuilder.append(id)
                .append(" rental date: ").append(rentalDate)
                .append(" status: ").append(status)
                .append(" ").append(employee)
                .append(" ").append(reservation)
                .append(" remark: ").append(remark)
                .append("]").toString();
    }
}
