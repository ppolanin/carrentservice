package pl.polaninp.rentcarservice.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Objects;

@MappedSuperclass
public abstract class ContactDetails implements Cloneable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;
    @Pattern(regexp = "(?!-|\\s)[\\p{L}-\\s]{2,100}(?<!-|\\s)")
    protected String country;
    @Pattern(regexp = "(?!-|\\s)[\\p{L}-\\s]{2,100}(?<!-|\\s)")
    protected String city;
    @Pattern(regexp = "(?!-|\\s|\\.)[\\p{L}0-9-\\s\\.]{2,100}(?<!-|\\s|\\.)")
    protected String street;
    @Pattern(regexp = "(?!-|\\s|/|\\\\)[\\p{L}0-9-\\s/\\\\]{1,20}(?<!-|\\s|/\\\\)")
    protected String addressNumber;
    @Pattern(regexp = "(?!-|\\s|/|\\\\)[\\p{L}0-9-\\s/\\\\]{1,20}(?<!-|\\s|/\\\\)")
    protected String zipCode;
    @Pattern(regexp = "^\\+\\d{6,15}$")
    protected String landlinePhoneNumber;

    public ContactDetails(@NotNull String country, @NotNull String city, @NotNull String street, @NotNull String addressNumber, @NotNull String zipCode, @NotNull String landlinePhoneNumber) {
        this.country = country;
        this.city = city;
        this.street = street;
        this.addressNumber = addressNumber;
        this.zipCode = zipCode;
        this.landlinePhoneNumber = landlinePhoneNumber;
    }

    public ContactDetails() {
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setAddressNumber(String addressNumber) {
        this.addressNumber = addressNumber;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public void setLandlinePhoneNumber(String landlinePhoneNumber) {
        this.landlinePhoneNumber = landlinePhoneNumber;
    }

    public Long getId() {
        return id;
    }

    public String getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    public String getStreet() {
        return street;
    }

    public String getAddressNumber() {
        return addressNumber;
    }

    public String getZipCode() {
        return zipCode;
    }

    public String getLandlinePhoneNumber() {
        return landlinePhoneNumber;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ContactDetails)) return false;
        ContactDetails contactDetails = (ContactDetails) o;
        return Objects.equals(id, contactDetails.id) &&
                country.equals(contactDetails.country) &&
                city.equals(contactDetails.city) &&
                street.equals(contactDetails.street) &&
                addressNumber.equals(contactDetails.addressNumber) &&
                zipCode.equals(contactDetails.zipCode) &&
                landlinePhoneNumber.equals(contactDetails.landlinePhoneNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, country, city, street, addressNumber, zipCode, landlinePhoneNumber);
    }

    @Override
    public String toString() {
        StringBuilder contactDetailsStringBuilder = new StringBuilder("contact details [id: ");
        return
                contactDetailsStringBuilder.append(id)
                .append(" country: ").append(country)
                .append(" city: ").append(city)
                .append(" street: ").append(street)
                .append(" address number: ").append(addressNumber)
                .append(" zip code: ").append(zipCode)
                .append(" landline phone number: ").append(landlinePhoneNumber)
                .append("]").toString();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

}
