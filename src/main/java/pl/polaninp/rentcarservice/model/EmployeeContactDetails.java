package pl.polaninp.rentcarservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Objects;

@Entity(name = "employee_contact_details")
public class EmployeeContactDetails extends ContactDetails {

    @NotNull
    @Pattern(regexp = ".+@[a-z0-9]+\\.[a-z]+")
    private String email;
    @Pattern(regexp = "^\\+\\d{6,15}$")
    private String privateMobilePhoneNumber;
    @Pattern(regexp = "^\\+\\d{6,15}$")
    private String serviceMobilePhoneNumber;
    @OneToOne(fetch = FetchType.LAZY)
    @MapsId
    @JoinColumn(name = "id", unique = true, nullable = false)
    @JsonIgnore
    private Employee employee;

    public EmployeeContactDetails(@NotNull String country, @NotNull String city, @NotNull String street, @NotNull String addressNumber, @NotNull String zipCode, @NotNull String landlinePhoneNumber, @NotNull @Pattern(regexp = ".+@[a-z0-9]+\\.[a-z]+") String email, @NotNull @Pattern(regexp = "\\+\\d{6,15}") String privateMobilePhoneNumber, @NotNull @Pattern(regexp = "\\+\\d{6,15}") String serviceMobilePhoneNumber) {
        super(country, city, street, addressNumber, zipCode, landlinePhoneNumber);
        this.email = email;
        this.privateMobilePhoneNumber = privateMobilePhoneNumber;
        this.serviceMobilePhoneNumber = serviceMobilePhoneNumber;
    }

    public EmployeeContactDetails() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPrivateMobilePhoneNumber() {
        return privateMobilePhoneNumber;
    }

    public void setPrivateMobilePhoneNumber(String privateMobilePhoneNumber) {
        this.privateMobilePhoneNumber = privateMobilePhoneNumber;
    }

    public String getServiceMobilePhoneNumber() {
        return serviceMobilePhoneNumber;
    }

    public void setServiceMobilePhoneNumber(String serviceMobilePhoneNumber) {
        this.serviceMobilePhoneNumber = serviceMobilePhoneNumber;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EmployeeContactDetails)) return false;
        if (!super.equals(o)) return false;
        EmployeeContactDetails employeeContactDetails = (EmployeeContactDetails) o;
        return email.equals(employeeContactDetails.email) &&
                Objects.equals(privateMobilePhoneNumber, employeeContactDetails.privateMobilePhoneNumber) &&
                Objects.equals(serviceMobilePhoneNumber, employeeContactDetails.serviceMobilePhoneNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), email, privateMobilePhoneNumber, serviceMobilePhoneNumber);
    }

    @Override
    public String toString() {
        StringBuilder employeeContactDetailsStringBuilder = new StringBuilder("employee contact details [id:");
        return
                employeeContactDetailsStringBuilder.append(id)
                        .append(" country: ").append(country)
                        .append(" city: ").append(city)
                        .append(" street: ").append(street)
                        .append(" building number: ").append(addressNumber)
                        .append(" zip code: ").append(zipCode)
                        .append(" landline phone number: ").append(landlinePhoneNumber)
                        .append(" email: ").append(email)
                        .append(" private mobile phone number: ").append(privateMobilePhoneNumber)
                        .append(" service mobile phone number: ").append(serviceMobilePhoneNumber)
                        .append("]")
                        .toString();
    }
}
