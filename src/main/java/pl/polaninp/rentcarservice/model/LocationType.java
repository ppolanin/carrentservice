package pl.polaninp.rentcarservice.model;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum LocationType {

    PICK_UP("pick up car location", "lokalizacja wypożyczenia pojazdu"), RETURN("return car location", "lokalizacja zwrotu pojazdu");

    private final String enDescription;
    private final String plDescription;

    LocationType(String enDescription, String plDescription) {
        this.enDescription = enDescription;
        this.plDescription = plDescription;
    }

    public String getEnDescription() {
        return enDescription;
    }

    public String getPlDescription() {
        return plDescription;
    }
}