package pl.polaninp.rentcarservice.model;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum CarStatus {
    AVAILABLE("available", "dostępny"),
    UNAVAILABLE("unavailable", "niedostępny");

    private final String enStatus;
    private final String plStatus;
    private final String value;

    CarStatus(String enStatus, String plStatus) {
        this.enStatus = enStatus;
        this.plStatus = plStatus;
        this.value = this.name();
    }

    public String getValue() {
        return value;
    }

    public String getEnStatus() {
        return enStatus;
    }

    public String getPlStatus() {
        return plStatus;
    }
}
