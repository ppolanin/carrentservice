package pl.polaninp.rentcarservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Objects;

@Entity
public class Location implements Cloneable, ExtraCost {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Pattern(regexp = "(?!-|\\s|\\.)[\\p{L}0-9-\\s.]{2,100}(?<!-|\\s|\\.)")
    private String name;
    private String description;
    private double distance;
    private double cost;
    @Valid
    @OneToOne(mappedBy = "location", cascade = CascadeType.ALL, orphanRemoval = true)
    private LocationContactDetails contactDetails;
    @Transient
    private LocationType locationType;

    @Transient
    private static final CostIdentifier costIdentifier = CostIdentifier.LOCATION;
    private static final double CAR_DEPRECATION_FACTOR = 0.8358;

    public Location(@Pattern(regexp = "(?!-|\\s|\\.)[a-zA-Z0-9-\\s.]{2,100}(?<!-|\\s|\\.)") String name, double distance, @NotNull LocationType locationType, LocationContactDetails contactDetails) {
        this.name = name;
        this.distance = distance;
        this.contactDetails = contactDetails;
        this.description = createDescription();
        this.cost = calculateCost();
        validLocationType(locationType);
    }

    public Location(@Pattern(regexp = "(?!-|\\s|\\.)[a-zA-Z0-9-\\s.]{2,100}(?<!-|\\s|\\.)") String name, double distance, @NotNull LocationType locationType) {
        this.name = name;
        this.distance = distance;
        this.description = createDescription();
        this.cost = calculateCost();
        validLocationType(locationType);
    }

    public Location() {
        this.description = createDescription();
    }

    private double calculateCost() {
        return Math.round(CAR_DEPRECATION_FACTOR * Math.abs(distance) * 100.00) / 100.00;
    }

    private void validLocationType(LocationType locationType) {
        if (locationType != null) {
            this.locationType = locationType;
        } else {
            throw new IllegalArgumentException("LocationType should have not null value");
        }
    }

    private String createDescription() {
        if (name != null) {
            if (contactDetails != null) {
                return name.concat(" - ").concat(contactDetails.country).concat(", ").concat(contactDetails.city).concat(", ").concat(contactDetails.street).concat(" ").concat(contactDetails.addressNumber).concat(", ").concat(contactDetails.zipCode);
            } else {
                return name;
            }
        }
        return "";
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
        setDescription();
    }

    public void setDescription() {
        this.description = createDescription();
    }

    public void setDistance(double distance) {
        this.distance = distance;
        setCost();
    }

    public void setCost() {
        this.cost = calculateCost();
    }

    public void setContactDetails(LocationContactDetails contactDetails) {
        if (contactDetails == null) {
            if (this.contactDetails != null) {
                this.contactDetails.setLocation(null);
            }
        } else {
            contactDetails.setLocation(this);
        }
        this.contactDetails = contactDetails;
        setDescription();
    }

    public void setLocationType(LocationType locationType) {
        this.locationType = locationType;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public double getDistance() {
        return distance;
    }

    public LocationContactDetails getContactDetails() {
        return contactDetails;
    }

    @JsonIgnore
    public LocationType getLocationType() {
        return locationType;
    }

    @Override
    public CostIdentifier getCostIdentifier() {
        return costIdentifier;
    }

    @Override
    public double getCost() {
        return cost;
    }

    @JsonIgnore
    @Override
    public String getDescriptionName(String lang) {
        String description;
        switch (lang) {
            case "en":
                description = locationType.getEnDescription();
                break;
            case "pl":
                description = locationType.getPlDescription();
                break;
            default:
                throw new IllegalArgumentException("Unrecognized language abbreviation");
        }
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Location)) return false;
        Location location = (Location) o;
        return Double.compare(location.distance, distance) == 0 &&
                Double.compare(location.cost, cost) == 0 &&
                Objects.equals(id, location.id) &&
                Objects.equals(name, location.name) &&
                Objects.equals(description, location.description) &&
                Objects.equals(contactDetails, location.contactDetails) &&
                locationType == location.locationType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, distance, cost, contactDetails, locationType);
    }

    @Override
    public String toString() {
        String stringLocation = "location [id: " + id +
                " name: " + name +
                " description: " + description +
                " distance: " + distance +
                " cost identifier: " + costIdentifier.toString() +
                " cost: " + cost;
        if (locationType == null && contactDetails == null) {
            return stringLocation + "]";
        } else if (locationType != null && contactDetails == null) {
            return stringLocation + " type: " + locationType.toString() + "]";
        } else if (locationType == null) {
            return stringLocation + " " + contactDetails.toString() + "]";
        }
        return stringLocation + " type: " + locationType.toString() + " " + contactDetails.toString() + "]";
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Location clonedLocation = (Location) super.clone();
        if (contactDetails != null) {
            clonedLocation.contactDetails = (LocationContactDetails) this.contactDetails.clone();
        }
        return clonedLocation;
    }
}
