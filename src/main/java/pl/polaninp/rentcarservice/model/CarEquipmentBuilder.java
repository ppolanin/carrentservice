package pl.polaninp.rentcarservice.model;

interface CarEquipmentBuilder {

    CarEquipment build();

    CarEquipmentBuilder setPrice(double price);

    CarEquipmentBuilder setYearOfProduction(int yearOfProduction);

    CarEquipmentBuilder setItemNumber(String itemNumber);

    CarEquipmentBuilder setEnDescription(String enDescription);

    CarEquipmentBuilder setPlDescription(String plDescription);

}
