package pl.polaninp.rentcarservice.model;

import pl.polaninp.rentcarservice.exception.AttributeNotFoundException;
import pl.polaninp.rentcarservice.support.PropertiesManager;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.Objects;

public class CarEquipment implements ExtraCost {

    @NotNull
    private String name;
    @NotNull
    private String model;
    private double price;
    @NotNull
    private String manufacturerName;
    private int yearOfProduction;
    private String itemNumber;
    private String enDescription;
    private String plDescription;
    private static final CostIdentifier CAR_EQUIPMENT_COST = CostIdentifier.CAR_EQUIPMENT;
    private final String pathToDefaultCarEquipmentDictionary = "src/main/resources/en_car_equipment_dictionary.properties";
    private final String pathToPlCarEquipmentDictionary = "src/main/resources/pl_car_equipment_dictionary.properties";

    private void saveNameInDictionaryFile(String name) {
        PropertiesManager propertiesManager = new PropertiesManager();
        try {
            propertiesManager.saveAttributeAsKey(pathToDefaultCarEquipmentDictionary, name);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setName(String name) {
        this.name = name;
        saveNameInDictionaryFile(name);
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public void setYearOfProduction(int yearOfProduction) {
        this.yearOfProduction = yearOfProduction;
    }

    public void setItemNumber(String itemNumber) {
        this.itemNumber = itemNumber;
    }

    public void setEnDescription(String enDescription) {
        this.enDescription = enDescription;
    }

    public void setPlDescription(String plDescription) {
        this.plDescription = plDescription;
    }

    public String getName() {
        return name;
    }

    public String getModel() {
        return model;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public int getYearOfProduction() {
        return yearOfProduction;
    }

    public String getItemNumber() {
        return itemNumber;
    }

    public String getEnDescription() {
        return enDescription;
    }

    public String getPlDescription() {
        return plDescription;
    }

    @Override
    public String getDescriptionName(String lang) {
        PropertiesManager propertiesManager = new PropertiesManager();
        String description;
        switch (lang) {
            case "en":
                description = name.concat(" ").concat(manufacturerName).concat(" ").concat(model);
                break;
            case "pl":
                String plCarEquipmentName;
                try {
                    plCarEquipmentName = propertiesManager.getAttributeByKey(pathToPlCarEquipmentDictionary, propertiesManager.createKeyFromValue(name)).orElseThrow(() -> new AttributeNotFoundException("Attribute key: " + name + " not found in file: " + pathToPlCarEquipmentDictionary));
                    description = plCarEquipmentName.concat(" ").concat(manufacturerName).concat(" ").concat(model);
                    break;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            default:
                throw new IllegalArgumentException("Unrecognized language abbreviation");
        }
        return description;
    }

    @Override
    public CostIdentifier getCostIdentifier() {
        return CAR_EQUIPMENT_COST;
    }

    @Override
    public double getCost() {
        return price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CarEquipment)) return false;
        CarEquipment carEquipment = (CarEquipment) o;
        return Double.compare(carEquipment.price, price) == 0 &&
                yearOfProduction == carEquipment.yearOfProduction &&
                Objects.equals(name, carEquipment.name) &&
                Objects.equals(model, carEquipment.model) &&
                Objects.equals(manufacturerName, carEquipment.manufacturerName) &&
                Objects.equals(itemNumber, carEquipment.itemNumber) &&
                Objects.equals(enDescription, carEquipment.enDescription) &&
                Objects.equals(plDescription, carEquipment.plDescription);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, model, price, manufacturerName, yearOfProduction, itemNumber, enDescription, plDescription);
    }

    @Override
    public String toString() {
        return "Car equipment " +
                "[name: " + name +
                " model: " + model +
                " price: " + price +
                " manufacturer name: " + manufacturerName +
                " year Of production: " + yearOfProduction +
                " item number: " + itemNumber +
                " english description: " + enDescription +
                " polish description: " + plDescription +
                ']';
    }
}
