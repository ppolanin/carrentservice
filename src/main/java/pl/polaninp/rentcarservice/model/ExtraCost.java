package pl.polaninp.rentcarservice.model;

public interface ExtraCost extends Cost {
    String getDescriptionName(String lang);
}
