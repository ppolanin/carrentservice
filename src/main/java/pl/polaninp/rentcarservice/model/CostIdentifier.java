package pl.polaninp.rentcarservice.model;

import com.fasterxml.jackson.annotation.*;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum CostIdentifier {

    LOCATION("location", "car location cost", "koszt lokalizacji pojazdu"), CAR_EQUIPMENT("equipment", "car equipment cost", "koszt wyposażenia pojazdu"), CAR_RESERVATION("reservation", "car reservation cost", "koszt rezerwacji pojazdu");

    private final String enDescription;
    private final String plDescription;
    private final String type;

    CostIdentifier(String type, String enDescription, String plDescription) {
        this.type = type;
        this.enDescription = enDescription;
        this.plDescription = plDescription;
    }

    public String getEnDescription() {
        return enDescription;
    }

    public String getPlDescription() {
        return plDescription;
    }

    public String getType() {
        return type;
    }

}
