package pl.polaninp.rentcarservice.model;

public class CarEquipmentBuilderImpl implements CarEquipmentBuilder {

    private final CarEquipment carEquipment;

    public CarEquipmentBuilderImpl(String name, String manufacturerName, String model) {
        carEquipment = new CarEquipment();
        carEquipment.setName(name);
        carEquipment.setManufacturerName(manufacturerName);
        carEquipment.setModel(model);
    }

    @Override
    public CarEquipment build() {
        return carEquipment;
    }

    @Override
    public CarEquipmentBuilder setPrice(double price) {
        carEquipment.setPrice(price);
        return this;
    }

    @Override
    public CarEquipmentBuilder setYearOfProduction(int yearOfProduction) {
        carEquipment.setYearOfProduction(yearOfProduction);
        return this;
    }

    @Override
    public CarEquipmentBuilder setItemNumber(String itemNumber) {
        carEquipment.setItemNumber(itemNumber);
        return this;
    }

    @Override
    public CarEquipmentBuilder setEnDescription(String enDescription) {
        carEquipment.setEnDescription(enDescription);
        return this;
    }

    @Override
    public CarEquipmentBuilder setPlDescription(String plDescription) {
        carEquipment.setPlDescription(plDescription);
        return this;
    }
}
