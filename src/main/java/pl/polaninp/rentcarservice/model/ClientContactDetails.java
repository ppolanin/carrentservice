package pl.polaninp.rentcarservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Objects;

@Entity(name = "client_contact_details")
public class ClientContactDetails extends ContactDetails {

    @NotNull
    @Pattern(regexp = ".+@[a-z0-9]+\\.[a-z]+")
    private String email;
    @Pattern(regexp = "^\\+\\d{6,15}$")
    private String mobilePhoneNumber;
    @OneToOne(fetch = FetchType.LAZY)
    @MapsId
    @JoinColumn(name = "id", unique = true, nullable = false)
    @JsonIgnore
    private Client client;

    public ClientContactDetails(@NotNull String country, @NotNull String city, @NotNull String street, @NotNull String addressNumber, @NotNull String zipCode, @NotNull String landlinePhoneNumber, @NotNull @Pattern(regexp = ".+@[a-z0-9]+\\.[a-z]+") String email, @Pattern(regexp = "^\\+\\d{6,15}$") String mobilePhoneNumber) {
        super(country, city, street, addressNumber, zipCode, landlinePhoneNumber);
        this.email = email;
        this.mobilePhoneNumber = mobilePhoneNumber;
    }

    public ClientContactDetails() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobilePhoneNumber() {
        return mobilePhoneNumber;
    }

    public void setMobilePhoneNumber(String mobilePhoneNumber) {
        this.mobilePhoneNumber = mobilePhoneNumber;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ClientContactDetails)) return false;
        if (!super.equals(o)) return false;
        ClientContactDetails clientContactDetails = (ClientContactDetails) o;
        return Objects.equals(email, clientContactDetails.email) &&
                Objects.equals(mobilePhoneNumber, clientContactDetails.mobilePhoneNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), email, mobilePhoneNumber);
    }

    @Override
    public String toString() {
        StringBuilder clientContactDetailsStringBuilder = new StringBuilder("contact details [id: ");
        return
                clientContactDetailsStringBuilder.append(id)
                        .append(" country: ").append(country)
                        .append(" city: ").append(city)
                        .append(" street: ").append(street)
                        .append(" building number: ").append(addressNumber)
                        .append(" zip code: ").append(zipCode)
                        .append(" email: ").append(email)
                        .append(" landline phone number: ").append(landlinePhoneNumber)
                        .append(" mobile phone number: ").append(mobilePhoneNumber)
                        .append("]")
                        .toString();
    }

}
