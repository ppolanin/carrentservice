package pl.polaninp.rentcarservice.model;

import com.fasterxml.jackson.annotation.JsonValue;

public enum ReservationStatus {
    NEW("nowa"), CONFIRMED("potwierdzona"), CANCELLED("anulowana");

    final String description;

    ReservationStatus(String description) {
        this.description = description;
    }

    @JsonValue
    public String getDescription() {
        return description;
    }

}
