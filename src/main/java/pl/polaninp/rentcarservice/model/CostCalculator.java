package pl.polaninp.rentcarservice.model;

import java.util.LinkedHashMap;
import java.util.Map;

public class CostCalculator {

    private final Map<String, Cost> costs;

    public CostCalculator() {
        costs = new LinkedHashMap<>();
    }

    public void addCost(Cost cost) {
        if (CostIdentifier.LOCATION.equals(cost.getCostIdentifier()) || CostIdentifier.CAR_EQUIPMENT.equals(cost.getCostIdentifier())) {
            ExtraCost extraCost = (ExtraCost) cost;
            String keyEquipmentCost = extraCost.getCostIdentifier().getType() + ": " + extraCost.getDescriptionName("en");
            costs.put(keyEquipmentCost, cost);
        } else {
            costs.put(cost.getCostIdentifier().getType(), cost);
        }
    }

    public void removeCost(String costIdentifierKey) {
        costs.remove(costIdentifierKey);
    }

    public Cost getCost(String costIdentifierKey) {
        return costs.get(costIdentifierKey);
    }

    public Map<String, Cost> getCosts() {
        return costs;
    }

    public void clearContent() {
        costs.clear();
    }

    public double sumCosts() {
        return Math.round(costs.values().stream().mapToDouble(Cost::getCost).sum() * 100.00) / 100.00;
    }

}
