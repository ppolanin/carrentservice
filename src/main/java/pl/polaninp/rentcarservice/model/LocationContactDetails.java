package pl.polaninp.rentcarservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity(name = "location_contact_details")
public class LocationContactDetails extends ContactDetails {

    @OneToOne(fetch = FetchType.LAZY)
    @MapsId(value = "id")
    @JoinColumn(name = "id")
    @JsonIgnore
    private Location location;

    public LocationContactDetails(@NotNull String country, @NotNull String city, @NotNull String street, @NotNull String addressNumber, @NotNull String zipCode, @NotNull String landlinePhoneNumber) {
        super(country, city, street, addressNumber, zipCode, landlinePhoneNumber);
    }

    public LocationContactDetails() {
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

}
