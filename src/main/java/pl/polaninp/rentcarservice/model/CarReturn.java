package pl.polaninp.rentcarservice.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.time.temporal.ChronoField;
import java.util.Objects;

@Entity
public class CarReturn {
    @Id
    private Long id;
    @NotNull
    private LocalDateTime returnDate;
    @NotNull
    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.REMOVE})
    @MapsId
    @JoinColumn(name = "id", unique = true, nullable = false)
    private CarRental carRental;
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id")
    private Employee employee;
    private double totalFee;
    @NotNull
    private String remark;

    public CarReturn(@NotNull LocalDateTime returnDate, @NotNull CarRental carRental, @NotNull Employee employee, @NotNull String remark) {
        this.returnDate = returnDate;
        this.carRental = carRental;
        this.employee = employee;
        this.totalFee = calculateTotalFee();
        this.remark = remark;
    }

    public CarReturn() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(LocalDateTime returnDate) {
        this.returnDate = returnDate;
    }

    public CarRental getCarRental() {
        return carRental;
    }

    public void setCarRental(CarRental carRental) {
        this.carRental = carRental;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public double getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(double totalFee) {
        this.totalFee = totalFee;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public double calculateTotalFee() {
        LocalDateTime endReservationDate = carRental.getReservation().getReservationToDate();
        int dayDifference = returnDate.get(ChronoField.DAY_OF_YEAR) - endReservationDate.get(ChronoField.DAY_OF_YEAR);
        double reservationCosts = carRental.getReservation().getCosts();
        if (dayDifference == 0) {
            return reservationCosts;
        } else {
            double totalFee = reservationCosts + dayDifference * carRental.getReservation().getCar().getDailyFee();
            return Math.round(totalFee * 100.00) / 100.00;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CarReturn)) return false;
        CarReturn carReturn = (CarReturn) o;
        return Double.compare(carReturn.totalFee, totalFee) == 0 &&
                Objects.equals(id, carReturn.id) &&
                returnDate.equals(carReturn.returnDate) &&
                carRental.equals(carReturn.carRental) &&
                employee.equals(carReturn.employee) &&
                remark.equals(carReturn.remark);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, returnDate, carRental, employee, totalFee, remark);
    }

    @Override
    public String toString() {
        StringBuilder returnStringBuilder = new StringBuilder("car return [id: ");
        return returnStringBuilder.append(id)
                .append(" return date: ").append(returnDate)
                .append(" total fee: ").append(totalFee)
                .append(" remark: ").append(remark)
                .append(" ").append(carRental)
                .append(" ").append(employee)
                .append("]").toString();
    }
}
