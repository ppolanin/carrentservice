package pl.polaninp.rentcarservice.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UrlAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    private static final String CLIENT_ROLE = "ROLE_CLIENT";
    private static final String ADMIN_ROLE = "ROLE_ADMIN";
    private static final String EMPLOYEE_ROLE = "ROLE_EMPLOYEE";
    private final RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
    private static final Logger LOGGER = LoggerFactory.getLogger(UrlAuthenticationSuccessHandler.class);

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) {
        authentication.getAuthorities().forEach(authority -> {
            if (authority.getAuthority().equals(CLIENT_ROLE)) {
                try {
                    redirectStrategy.sendRedirect(httpServletRequest, httpServletResponse, "/client");
                    LOGGER.info(CLIENT_ROLE);
                } catch (IOException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            } else if (authority.getAuthority().equals(ADMIN_ROLE)) {
                try {
                    redirectStrategy.sendRedirect(httpServletRequest, httpServletResponse, "/admin");
                } catch (IOException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            } else if (authority.getAuthority().equals(EMPLOYEE_ROLE)) {
                try {
                    redirectStrategy.sendRedirect(httpServletRequest, httpServletResponse, "/employee-account");
                } catch (IOException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
        });
    }
}
