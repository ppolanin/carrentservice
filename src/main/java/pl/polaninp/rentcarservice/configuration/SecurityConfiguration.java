package pl.polaninp.rentcarservice.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import pl.polaninp.rentcarservice.service.UserDetailsServiceImplForClients;
import pl.polaninp.rentcarservice.service.UserDetailsServiceImplForEmployees;

@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfiguration {

    @Bean
    public static PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(4);
    }

    @Configuration
    @Order(1)
    public static class ClientSecurityConfiguration extends WebSecurityConfigurerAdapter {

        private final UserDetailsServiceImplForClients userDetailsServiceForClients;

        @Autowired
        public ClientSecurityConfiguration(UserDetailsServiceImplForClients userDetailsServiceForClients) {
            this.userDetailsServiceForClients = userDetailsServiceForClients;
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http
                    .antMatcher("/client/**")
                    .csrf().disable()
                    .httpBasic()
                    .authenticationEntryPoint(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED))
                    .and()
                    .authorizeRequests()
                    .antMatchers("/client/account", "/client/reservations", "/client/data").authenticated()
                    .anyRequest().permitAll()
                    .and()
                    .formLogin()
                    .loginPage("/client/login")
                    .defaultSuccessUrl("/client/account")
                    .and()
                    .logout()
                    .logoutUrl("/client/account/logout")
                    .logoutSuccessUrl("/main")
                    .invalidateHttpSession(true)
                    .clearAuthentication(true);
        }

        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth.userDetailsService(userDetailsServiceForClients);
        }

    }

    @Configuration
    @Order(2)
    public static class EmployeeSecurityConfiguration extends WebSecurityConfigurerAdapter {

        @Autowired
        private UserDetailsServiceImplForEmployees userDetailsServiceImplForEmployees;

        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth.userDetailsService(userDetailsServiceImplForEmployees);
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http
                    .antMatcher("/employee/**")
                    .authorizeRequests()
                    .antMatchers("/employee/login").permitAll()
                    .anyRequest().authenticated()
                    .and()
                    .csrf().disable()
                    .httpBasic()
                    .and()
                    .formLogin()
                    .loginPage("/employee/login")
                    .defaultSuccessUrl("/employee/panel")
                    .and()
                    .logout()
                    .logoutUrl("/employee/panel/logout")
                    .logoutSuccessUrl("/main")
                    .clearAuthentication(true)
                    .invalidateHttpSession(true);
        }

    }

    @Configuration
    @Order(3)
    public static class AdminSecurityConfiguration extends WebSecurityConfigurerAdapter {

        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth.inMemoryAuthentication()
                    .withUser("Admin")
                    .password(passwordEncoder().encode("Admin"))
                    .roles("ADMIN");
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http
                    .antMatcher("/admin/**")
                    .authorizeRequests()
                    .antMatchers("/admin/login").permitAll()
                    .anyRequest().authenticated()
                    .and()
                    .csrf().disable()
                    .httpBasic()
                    .and()
                    .formLogin()
                    .loginPage("/admin/login")
                    .defaultSuccessUrl("/admin/panel")
                    .and()
                    .logout()
                    .logoutUrl("/admin/panel/logout")
                    .logoutSuccessUrl("/main")
                    .invalidateHttpSession(true)
                    .clearAuthentication(true);
        }
    }

}
