package pl.polaninp.rentcarservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.polaninp.rentcarservice.repository.ClientRepository;

@Service
public class UserDetailsServiceImplForClients implements UserDetailsService {

    private final ClientRepository clientRepository;

    @Autowired
    public UserDetailsServiceImplForClients(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String s) {
        return clientRepository.findClientByUsername(s).orElseThrow(() -> new UsernameNotFoundException("Client username not found"));
    }

}
