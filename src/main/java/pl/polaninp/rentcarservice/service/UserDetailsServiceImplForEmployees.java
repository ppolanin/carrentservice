package pl.polaninp.rentcarservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.polaninp.rentcarservice.repository.EmployeeRepository;

@Service
public class UserDetailsServiceImplForEmployees implements UserDetailsService {

    private final EmployeeRepository employeeRepository;

    @Autowired
    public UserDetailsServiceImplForEmployees(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        return employeeRepository.findEmployeeByUsername(username).orElseThrow(() -> new UsernameNotFoundException("Employee username not found"));
    }
}
