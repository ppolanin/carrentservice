package pl.polaninp.rentcarservice.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.polaninp.rentcarservice.controller.EmployeeController;
import pl.polaninp.rentcarservice.model.Employee;
import pl.polaninp.rentcarservice.repository.EmployeeRepository;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Collections;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;
    private final PasswordEncoder passwordEncoder;
    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeController.class);

    @Autowired
    public EmployeeServiceImpl(EmployeeRepository employeeRepository, PasswordEncoder passwordEncoder) {
        this.employeeRepository = employeeRepository;
        this.passwordEncoder = passwordEncoder;
    }

    private String createUsernameByNameAndSurname(@NotNull @NotEmpty String name, @NotNull @NotEmpty String surname) {
        return name.substring(0, 1).toLowerCase() + surname.toLowerCase();
    }

    private String createEncodePassword(@NotNull @NotEmpty String password) {
        return passwordEncoder.encode(password);
    }

    private Employee createUsernameAndPasswordAndRole(@NotNull Employee employee) {
        employee.setUsername(createUsernameByNameAndSurname(employee.getName(), employee.getSurname()));
        employee.setPassword(createEncodePassword(employee.getPassword()));
        employee.setRoles(Collections.singleton(Employee.ROLE_EMPLOYEE));
        return employee;
    }

    @Override
    public void addEmployee(@NotNull Employee employee) {
        Employee persistedEmployee = employeeRepository.save(createUsernameAndPasswordAndRole(employee));
        LOGGER.info("New " + persistedEmployee);
    }

    @Override
    public void addEmployees(Collection<Employee> employees) {
        employees.forEach(e -> createUsernameAndPasswordAndRole(e));
        employeeRepository.saveAll(employees);
    }

    @Override
    public void updateEmployee(@NotNull Employee employee) {
        employee.setUsername(createUsernameByNameAndSurname(employee.getName(), employee.getSurname()));
        employee.setRoles(Collections.singleton(Employee.ROLE_EMPLOYEE));
        if (employee.getContactDetails() != null) {
            employee.getContactDetails().setId(employee.getId());
        }
        Employee employeeFromDB = getEmployeeById(employee.getId());
        if (!employeeFromDB.equals(employee)) {
            if (!employeeFromDB.getPassword().equals(employee.getPassword())) {
                employee.setPassword(createEncodePassword(employee.getPassword()));
            }
            Employee updatedEmployee = employeeRepository.save(employee);
            LOGGER.info("Updated " + updatedEmployee.toString());
        }
    }

    @Override
    public Iterable<Employee> getEmployees() {
        return employeeRepository.findAll();
    }

    @Override
    public Employee getEmployeeById(@NotNull Long id) {
        return employeeRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Employee with " + id + " not found"));
    }

    @Override
    public void removeEmployeeById(@NotNull Long id) {
        Employee employeeToRemove = getEmployeeById(id);
        employeeRepository.delete(employeeToRemove);
        LOGGER.info("Removed " + employeeToRemove.toString());
    }

    @Override
    public Long authentication(Authentication authentication) {
        if (authentication != null) {
            UserDetails userDetails = (UserDetails) authentication.getPrincipal();
            return employeeRepository.getClientIdByUsernameAndPassword(userDetails.getUsername(), userDetails.getPassword());
        }
        return null;
    }

}
