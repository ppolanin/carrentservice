package pl.polaninp.rentcarservice.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import pl.polaninp.rentcarservice.model.*;
import pl.polaninp.rentcarservice.repository.ReservationRepository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ReservationServiceImpl implements ReservationService {

    @Autowired
    private ClientService clientService;
    @Autowired
    private CarService carService;
    @Autowired
    private LocationService locationService;
    @Autowired
    private ReservationRepository reservationRepository;
    private static final String DATE_TIME_PATTERN = "dd.MM.yyyy HH:mm";
    private final Logger LOGGER = LoggerFactory.getLogger(ReservationServiceImpl.class);
    private final int ACCEPTABLE_NUMBER_OF_DAYS_TO_CANCEL_RESERVATION = 3;

    @Scheduled(cron = "00 00 * * * *")
    private void cancelExpiredReservations() {
        LOGGER.info("Cancel expired reservations...");
        LocalDateTime currentDateTime = LocalDateTime.now();
        List<Reservation> cancelledReservations = reservationRepository.findAll().stream()
                .filter(r -> r.getReservationFromDate().isBefore(currentDateTime) && r.getStatus().equals(ReservationStatus.NEW))
                .collect(Collectors.toList());
        cancelledReservations.forEach(r -> r.setStatus(ReservationStatus.CANCELLED));
        reservationRepository.saveAll(cancelledReservations);
    }

    @Override
    public void createReservation(Long carId, Long clientId, String reservationFromDate, Long pickupCarLocationId, String reservationToDate, Long returnCarLocationId) {
        Car car = carService.getCarById(carId);
        Location pickupCarLocation = locationService.getLocationById(pickupCarLocationId);
        Location returnCarLocation = locationService.getLocationById(returnCarLocationId);
        Reservation reservation = reservationRepository.save(new Reservation(LocalDateTime.now().truncatedTo(ChronoUnit.MINUTES), clientService.getClientById(clientId), car, LocalDateTime.parse(reservationFromDate, DateTimeFormatter.ofPattern(DATE_TIME_PATTERN)), pickupCarLocation, LocalDateTime.parse(reservationToDate, DateTimeFormatter.ofPattern(DATE_TIME_PATTERN)), returnCarLocation, ReservationStatus.NEW));
        LOGGER.info("New " + reservation.toString());
    }

    @Override
    public Reservation createInitialReservation(Long carId, String reservationFromDate, Long pickupCarLocationId, String reservationToDate, Long returnCarLocationId) {
        Car car = carService.getCarById(carId);
        Location pickupCarLocation = locationService.getLocationById(pickupCarLocationId);
        Location returnCarLocation = locationService.getLocationById(returnCarLocationId);
        Reservation initialReservation = new Reservation(car, LocalDateTime.parse(reservationFromDate, DateTimeFormatter.ofPattern(DATE_TIME_PATTERN)), pickupCarLocation, LocalDateTime.parse(reservationToDate, DateTimeFormatter.ofPattern(DATE_TIME_PATTERN)), returnCarLocation);
        LOGGER.info("New initial " + initialReservation.toString());
        return initialReservation;
    }

    @Override
    public void removeReservation(Long id) {
        Reservation reservation = getReservationById(id);
        reservationRepository.deleteById(id);
        LOGGER.info("Removed " + reservation.toString());
    }

    @Override
    public void cancelReservation(Long id) {
        Reservation reservation = getReservationById(id);
        reservation.setStatus(ReservationStatus.CANCELLED);
        reservationRepository.save(reservation);
    }

    @Override
    public boolean cancelReservationByClient(Long reservationId) {
        Reservation reservation = getReservationById(reservationId);
        LocalDateTime reservationStartDate = reservation.getReservationFromDate();
        LocalDateTime currentDate = LocalDateTime.now();
        int daysDifference = reservationStartDate.get(ChronoField.DAY_OF_YEAR) - currentDate.get(ChronoField.DAY_OF_YEAR);
        if (daysDifference >= ACCEPTABLE_NUMBER_OF_DAYS_TO_CANCEL_RESERVATION) {
            reservation.setStatus(ReservationStatus.CANCELLED);
            reservationRepository.save(reservation);
            return true;
        }
        return false;
    }

    @Override
    public List<Reservation> getAllReservations() {
        return reservationRepository.findAll();
    }

    @Override
    public Reservation getReservationById(Long reservationId) {
        return reservationRepository.findById(reservationId).orElseThrow(() -> new IllegalArgumentException("Incorrect reservation id"));
    }

    @Override
    public Iterable<Reservation> getReservationsByClientId(Long clientId) {
        return reservationRepository.getReservationsByClientId(clientId);
    }

    @Override
    public Iterable<Reservation> getReservationsByCarId(Long carId) {
        return reservationRepository.getReservationByCarId(carId);
    }

    @Override
    public Iterable<Reservation> getReservationsByBookingDuration(String reservationFromDate, String reservationToDate) {
        return reservationRepository.getReservationsByBookingDuration(LocalDateTime.parse(reservationFromDate, DateTimeFormatter.ofPattern(DATE_TIME_PATTERN)), LocalDateTime.parse(reservationToDate, DateTimeFormatter.ofPattern(DATE_TIME_PATTERN)));
    }

    @Override
    public Iterable<Long> getCarIdsByBookingDuration(String reservationFromDate, String reservationToDate) {
        return reservationRepository.getCarIdsByBookingDuration(LocalDateTime.parse(reservationFromDate, DateTimeFormatter.ofPattern(DATE_TIME_PATTERN)), LocalDateTime.parse(reservationToDate, DateTimeFormatter.ofPattern(DATE_TIME_PATTERN)));
    }

    @Override
    public Iterable<Reservation> getNewReservationsOrderByStartReservationDate() {
        return reservationRepository.getReservationsByStatusAndOrderByStartReservationDate(ReservationStatus.NEW);
    }

    @Override
    public List<String> getBasicReservationCosts(Long id, String lang) {
        Reservation reservation = getReservationById(id);
        CostCalculator reservationCostCalculator = reservation.getCostCalculator();
        ArrayList<String> exportedReservationCosts = new ArrayList<>();
        for (Map.Entry<String, Cost> c : reservationCostCalculator.getCosts().entrySet()) {
            String description;
            switch (lang) {
                case "pl":
                    if (CostIdentifier.LOCATION.equals(c.getValue().getCostIdentifier())) {
                        ExtraCost extraCost = (ExtraCost) c.getValue();
                        description = extraCost.getDescriptionName(lang);
                    } else {
                        description = c.getValue().getCostIdentifier().getPlDescription();
                    }
                    break;
                case "en":
                    if (CostIdentifier.LOCATION.equals(c.getValue().getCostIdentifier())) {
                        ExtraCost extraCost = (ExtraCost) c.getValue();
                        description = extraCost.getDescriptionName(lang);
                    } else {
                        description = c.getValue().getCostIdentifier().getEnDescription();
                    }
                    break;
                default:
                    throw new IllegalArgumentException("Unrecognized language abbreviation");
            }
            exportedReservationCosts.add(description + ": " + c.getValue().getCost());
        }
        return exportedReservationCosts;
    }

    @Override
    public void addReservationExtraCost(Long id, ExtraCost cost) {
        Reservation reservation = getReservationById(id);
        reservation.calculateExtraCosts(cost);
    }
}
