package pl.polaninp.rentcarservice.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import pl.polaninp.rentcarservice.model.CarRental;
import pl.polaninp.rentcarservice.model.CarRentalStatus;
import pl.polaninp.rentcarservice.model.CarReturn;
import pl.polaninp.rentcarservice.repository.CarReturnRepository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
public class CarReturnServiceImpl implements CarReturnService {

    private final CarReturnRepository carReturnRepository;
    private final EmployeeService employeeService;
    private final CarRentalService carRentalService;
    private static final String DATE_TIME_PATTERN = "dd.MM.yyyy HH:mm";
    private static final Logger LOGGER = LoggerFactory.getLogger(CarRentalServiceImpl.class);

    @Autowired
    public CarReturnServiceImpl(CarReturnRepository carReturnRepository, EmployeeService employeeService, CarRentalService carRentalService) {
        this.carReturnRepository = carReturnRepository;
        this.employeeService = employeeService;
        this.carRentalService = carRentalService;
    }

    @Override
    public void addReturn(String date, Long employeeId, Long rentalId,  String remark) {
        LocalDateTime returnDate = LocalDateTime.parse(date, DateTimeFormatter.ofPattern(DATE_TIME_PATTERN));
        CarRental carRental = carRentalService.getRentalById(rentalId);
        carRental.setStatus(CarRentalStatus.CONFIRMED);
        CarReturn carReturn = new CarReturn(returnDate, carRentalService.getRentalById(rentalId), employeeService.getEmployeeById(employeeId), remark);
        carReturnRepository.save(carReturn);
        LOGGER.info("New " + carReturn.toString());
    }

    @Override
    public void removeReturnById(Long id) {
        CarReturn returnById = getReturnById(id);
        carReturnRepository.delete(returnById);
        LOGGER.info("Removed " + returnById.toString());
    }

    @Override
    public CarReturn getReturnById(Long id) {
        return carReturnRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Incorrect return id: " + id));
    }

    @Override
    public Iterable<CarReturn> getReturns() {
        Sort sort = Sort.by("returnDate").descending();
        return carReturnRepository.findAll(sort);
    }
}
