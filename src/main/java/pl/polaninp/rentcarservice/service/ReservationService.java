package pl.polaninp.rentcarservice.service;

import pl.polaninp.rentcarservice.model.ExtraCost;
import pl.polaninp.rentcarservice.model.Reservation;

import java.util.List;

public interface ReservationService {

    void createReservation(Long carId, Long clientId, String reservationFromDate, Long pickupCarLocationId, String reservationToDate, Long returnCarLocationId);

    Reservation createInitialReservation(Long carId, String reservationFromDate, Long pickupCarLocationId, String reservationToDate, Long returnCarLocationId);

    void removeReservation(Long id);

    void cancelReservation(Long id);

    boolean cancelReservationByClient(Long reservationId);

    List<Reservation> getAllReservations();

    Reservation getReservationById(Long reservationId);

    Iterable<Reservation> getReservationsByClientId(Long clientId);

    Iterable<Reservation> getReservationsByCarId(Long carId);

    Iterable<Reservation> getReservationsByBookingDuration(String reservationFromDate, String reservationToDate);

    Iterable<Long> getCarIdsByBookingDuration(String reservationFromDate, String reservationToDate);

    Iterable<Reservation> getNewReservationsOrderByStartReservationDate();

    List<String> getBasicReservationCosts(Long id, String lang);

    void addReservationExtraCost(Long id, ExtraCost extraCost);
}
