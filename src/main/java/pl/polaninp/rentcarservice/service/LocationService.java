package pl.polaninp.rentcarservice.service;

import pl.polaninp.rentcarservice.model.Location;

import java.util.Collection;
import java.util.List;

public interface LocationService {
    void addLocation(Location location);

    void addLocations(Collection<Location> locations);

    void updateLocation(Location location);

    void removeLocation(Long id);

    Location getLocationById(Long id);

    Location getLocationByDescription(String description);

    List<Location> getLocations();
}
