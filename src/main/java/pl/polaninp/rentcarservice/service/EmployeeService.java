package pl.polaninp.rentcarservice.service;

import org.springframework.security.core.Authentication;
import pl.polaninp.rentcarservice.model.Employee;

import java.util.Collection;

public interface EmployeeService {
    void addEmployee(Employee employee);

    void addEmployees(Collection<Employee> employees);

    void updateEmployee(Employee employee);

    Iterable<Employee> getEmployees();

    Employee getEmployeeById(Long id);

    void removeEmployeeById(Long id);

    Long authentication(Authentication authentication);
}
