package pl.polaninp.rentcarservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.polaninp.rentcarservice.model.Car;
import pl.polaninp.rentcarservice.model.CarBodyType;
import pl.polaninp.rentcarservice.model.CarStatus;
import pl.polaninp.rentcarservice.repository.CarRepository;

import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class CarServiceImpl implements CarService {

    @Autowired
    private CarRepository carRepository;
    @Autowired
    private ReservationService reservationService;

    public CarServiceImpl() {
    }

    @Override
    public void addCar(@NotNull Car car) {
        carRepository.save(car);
    }

    @Override
    public Car getCarById(@NotNull Long id) {
        return carRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid car id: " + id));
    }

    @Override
    public void updateCar(@NotNull Long id, @NotNull Car car) {
        Car carToUpdate = carRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid car id: " + id));
        car.setId(carToUpdate.getId());
        carRepository.save(car);
    }

    @Override
    public void removeCarById(@NotNull Long id) {
        carRepository.deleteById(id);
    }

    @Override
    public void addCars(@NotNull Iterable<Car> cars) {
        carRepository.saveAll(cars);
    }

    @Override
    public Iterable<Car> getAllCars() {
        return carRepository.findAll();
    }

    @Override
    public Iterable<Car> getCarsByBrand(@NotNull String brand) {
        return carRepository.findCarsByBrand(brand);
    }

    @Override
    public Iterable<Car> getCarsByBrandAndStatus(@NotNull String brand, @NotNull CarStatus status) {
        return carRepository.findCarsByBrandAndCarStatus(brand, status);
    }

    @Override
    public Iterable<Car> getCarsByBodyTypeAndStatus(@NotNull CarBodyType bodyType, @NotNull CarStatus status) {
        return carRepository.findCarsByBodyTypeAndCarStatus(bodyType, status);
    }

    @Override
    public Iterable<Car> getCarsByBrandAndBodyTypeAndStatus(@NotNull String brand, @NotNull CarBodyType bodyType, @NotNull CarStatus status) {
        return carRepository.findCarsByBrandAndBodyTypeAndCarStatus(brand, bodyType, status);
    }

    @Override
    public Iterable<Car> getCarsByStatus(@NotNull CarStatus status) {
        return carRepository.findCarsByCarStatus(status);
    }

    @Override
    public List<Car> getAvailableCarsByBookingPeriod(@NotNull String startDate, @NotNull String finishDate) {
        List<Long> reservedCarIds = StreamSupport.stream(reservationService.getCarIdsByBookingDuration(startDate, finishDate).spliterator(), false)
                .collect(Collectors.toList());
        Iterable<Car> allCars = carRepository.findAll();
        List<Car> filteredCars = StreamSupport.stream(allCars.spliterator(), false)
                .filter(car -> !car.getCarStatus().equals(CarStatus.UNAVAILABLE))
                .collect(Collectors.toList());
        if (reservedCarIds.size() != 0) {
            for (Long reservedCarId : reservedCarIds) {
                for (int i = 0; i < filteredCars.size(); i++) {
                    if (reservedCarId.equals(filteredCars.get(i).getId())) {
                        filteredCars.remove(i);
                    }
                }
            }
        }
        return filteredCars;
    }

    @Override
    public Iterable<Car> getAvailableCarsByBookingPeriodAndBrand(@NotNull String startDate, @NotNull String finishDate, @NotNull String brand) {
        List<Car> availableCarsByBookingPeriod = getAvailableCarsByBookingPeriod(startDate, finishDate);
        return availableCarsByBookingPeriod.stream()
                .filter(car -> car.getBrand().toLowerCase().equals(brand.toLowerCase()))
                .collect(Collectors.toList());
    }

    @Override
    public Iterable<Car> getAvailableCarsByBookingPeriodAndBodyType(@NotNull String startDate, @NotNull String finishDate, @NotNull CarBodyType bodyType) {
        List<Car> availableCarsByBookingPeriod = getAvailableCarsByBookingPeriod(startDate, finishDate);
        return availableCarsByBookingPeriod.stream()
                .filter(car -> car.getBodyType().equals(bodyType))
                .collect(Collectors.toList());
    }

    @Override
    public Iterable<Car> getAvailableCarsByBookingPeriodAndBrandAndBodyType(@NotNull String startDate, @NotNull String finishDate, @NotNull String brand, @NotNull CarBodyType bodyType) {
        List<Car> availableCarsByBookingPeriod = getAvailableCarsByBookingPeriod(startDate, finishDate);
        return availableCarsByBookingPeriod.stream()
                .filter(car -> car.getBrand().toLowerCase().equals(brand.toLowerCase())
                        && car.getBodyType().equals(bodyType))
                .collect(Collectors.toList());
    }

    @Override
    public CarBodyType[] getCarBodyTypes(String type) {
        Comparator<CarBodyType> comparator;
        switch (type) {
            case "plDescription":
                comparator = Comparator.comparing(CarBodyType::getPlDescription, String::compareTo);
                break;
            case "enDescription":
                comparator = Comparator.comparing(CarBodyType::getEnDescription, String::compareTo);
                break;
            default:
                comparator = Comparator.comparing(CarBodyType::toString, String::compareTo);
        }
        CarBodyType[] carBodyTypes = CarBodyType.values();
        Arrays.sort(carBodyTypes, comparator);
        return carBodyTypes;
    }

    @Override
    public CarStatus[] getCarStatus(String langStatus) {
        Comparator<CarStatus> comparator;
        switch (langStatus) {
            case "enStatus":
                comparator = Comparator.comparing(CarStatus::getEnStatus, String::compareTo);
                break;
            case "plStatus":
                comparator = Comparator.comparing(CarStatus::getPlStatus, String::compareTo);
                break;
            default:
                comparator = Comparator.comparing(CarStatus::getValue, String::compareTo);
        }
        CarStatus[] carStatuses = CarStatus.values();
        Arrays.sort(carStatuses, comparator);
        return carStatuses;
    }

}
