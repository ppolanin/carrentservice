package pl.polaninp.rentcarservice.service;

import pl.polaninp.rentcarservice.model.CarReturn;

public interface CarReturnService {
    void addReturn(String date, Long employeeId, Long rentalId,  String remark);

    void removeReturnById(Long id);

    CarReturn getReturnById(Long id);

    Iterable<CarReturn> getReturns();
}
