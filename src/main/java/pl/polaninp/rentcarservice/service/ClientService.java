package pl.polaninp.rentcarservice.service;

import org.springframework.security.core.Authentication;
import pl.polaninp.rentcarservice.model.Client;
import pl.polaninp.rentcarservice.model.PasswordDTO;

import java.util.Collection;

public interface ClientService {

    void addClient(Client client);

    void addClients(Collection<Client> clients);

    Client getClientById(Long id);

    Client getClientByUsername(String username);

    void updateClient(Client client);

    void removeClientById(Long id);

    Iterable<Client> getClients();

    boolean existsClientByNameAndSurnameAndUsername(String name, String surname, String username);

    boolean existsUsername(String username);

    Long getClientIdByUsernameAndPassword(String username, String password);

    Long authentication(Authentication authentication);

    boolean changePassword(Authentication authentication, PasswordDTO passwordDTO);

}
