package pl.polaninp.rentcarservice.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.polaninp.rentcarservice.model.*;
import pl.polaninp.rentcarservice.repository.CarRentalRepository;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
public class CarRentalServiceImpl implements CarRentalService {

    @Autowired
    private CarRentalRepository carRentalRepository;
    @Autowired
    private EmployeeServiceImpl employeeService;
    @Autowired
    private ReservationService reservationService;
    private static final String DATE_TIME_PATTERN = "dd.MM.yyyy HH:mm";
    private static final Logger LOGGER = LoggerFactory.getLogger(CarRentalServiceImpl.class);

    @Override
    public void addRental(@NotNull String date, @NotNull Long employeeId, @NotNull Long reservationId, @NotNull String remark) {
        LocalDateTime rentalDate = LocalDateTime.parse(date, DateTimeFormatter.ofPattern(DATE_TIME_PATTERN));
        Reservation reservation = reservationService.getReservationById(reservationId);
        reservation.setStatus(ReservationStatus.CONFIRMED);
        CarRental carRental = carRentalRepository.save(new CarRental(rentalDate, employeeService.getEmployeeById(employeeId), reservation, remark, CarRentalStatus.NEW));
        LOGGER.info("New " + carRental.toString());
    }

    @Override
    public void removeRentalById(Long id) {
        CarRental carRental = getRentalById(id);
        carRentalRepository.delete(carRental);
        LOGGER.info("Removed " + carRental.toString());
    }

    @Override
    public CarRental getRentalById(Long id) {
        return carRentalRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Incorrect rental id: " + id));
    }

    @Override
    public Iterable<CarRental> getRentals() {
        return carRentalRepository.findAll();
    }
}
