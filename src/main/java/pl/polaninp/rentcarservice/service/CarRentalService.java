package pl.polaninp.rentcarservice.service;

import pl.polaninp.rentcarservice.model.CarRental;

public interface CarRentalService {
    void addRental(String date, Long employeeId, Long reservationId, String remark);

    void removeRentalById(Long id);

    CarRental getRentalById(Long id);

    Iterable<CarRental> getRentals();
}
