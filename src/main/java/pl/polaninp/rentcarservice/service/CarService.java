package pl.polaninp.rentcarservice.service;

import pl.polaninp.rentcarservice.model.Car;
import pl.polaninp.rentcarservice.model.CarBodyType;
import pl.polaninp.rentcarservice.model.CarStatus;

public interface CarService {

    void addCar(Car car);

    Car getCarById(Long id);

    void updateCar(Long id, Car car);

    void removeCarById(Long id);

    void addCars(Iterable<Car> cars);

    Iterable<Car> getAllCars();

    Iterable<Car> getCarsByBrand(String brand);

    Iterable<Car> getCarsByStatus(CarStatus status);

    Iterable<Car> getCarsByBrandAndStatus(String brand, CarStatus status);

    Iterable<Car> getCarsByBodyTypeAndStatus(CarBodyType bodyType, CarStatus status);

    Iterable<Car> getCarsByBrandAndBodyTypeAndStatus(String brand, CarBodyType bodyType, CarStatus status);

    Iterable<Car> getAvailableCarsByBookingPeriod(String startDate, String finishDate);

    Iterable<Car> getAvailableCarsByBookingPeriodAndBrand(String startDate, String finishDate, String brand);

    Iterable<Car> getAvailableCarsByBookingPeriodAndBodyType(String startDate, String finishDate, CarBodyType bodyType);

    Iterable<Car> getAvailableCarsByBookingPeriodAndBrandAndBodyType(String startDate, String finishDate, String brand, CarBodyType bodyType);

    CarBodyType[] getCarBodyTypes(String type);

    CarStatus[] getCarStatus(String langStatus);
}
