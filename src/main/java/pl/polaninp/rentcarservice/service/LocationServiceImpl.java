package pl.polaninp.rentcarservice.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import pl.polaninp.rentcarservice.model.Location;
import pl.polaninp.rentcarservice.repository.LocationRepository;

import java.util.Collection;
import java.util.List;

@Service
public class LocationServiceImpl implements LocationService {

    private final LocationRepository locationRepository;
    private static final Logger LOGGER = LoggerFactory.getLogger(LocationServiceImpl.class);

    @Autowired
    public LocationServiceImpl(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    @Override
    public void addLocation(Location location) {
        locationRepository.save(location);
        LOGGER.info(location.toString() + " was saved");
    }

    @Override
    public void addLocations(Collection<Location> locations) {
        locationRepository.saveAll(locations);
    }

    @Override
    public void updateLocation(Location location) {
        Location locationFromDB = getLocationById(location.getId());
        if(location.getContactDetails().getId() == null) {
            location.getContactDetails().setId(location.getId());
        }
        if(!locationFromDB.equals(location)) {
            locationRepository.save(location);
            LOGGER.info(locationFromDB.toString() + " was updated");
        }
        locationFromDB = null;
        location = null;
    }

    @Override
    public void removeLocation(Long id) {
        Location locationById = getLocationById(id);
        locationRepository.delete(locationById);
        LOGGER.info(locationById.toString() + " was removed from DB");
        locationById = null;
    }

    @Override
    public Location getLocationById(Long id) {
        return locationRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Location with id: " + id + " not found"));
    }

    @Override
    public Location getLocationByDescription(String description) {
        return locationRepository.getLocationByDescription(description);
    }

    @Override
    public List<Location> getLocations() {
        return locationRepository.findAll(Sort.by(Sort.Direction.ASC, "description"));
    }
}
