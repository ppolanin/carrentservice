package pl.polaninp.rentcarservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.polaninp.rentcarservice.model.Client;
import pl.polaninp.rentcarservice.model.PasswordDTO;
import pl.polaninp.rentcarservice.repository.ClientRepository;

import java.util.Collection;
import java.util.Collections;

@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    ClientRepository clientRepository;
    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public void addClient(Client client) {
        client.setPassword(passwordEncoder.encode(client.getPassword()));
        client.setRoles(Collections.singleton(Client.ROLE_CLIENT));
        clientRepository.save(client);
    }

    @Override
    public void addClients(Collection<Client> clients) {
        clients.forEach(c -> {
            c.setPassword(passwordEncoder.encode(c.getPassword()));
            c.setRoles(Collections.singleton(Client.ROLE_CLIENT));
        });
        clientRepository.saveAll(clients);
    }

    @Override
    public Client getClientById(Long id) {
        return clientRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid client id: " + id));
    }

    @Override
    public Client getClientByUsername(String username) {
        return clientRepository.findClientByUsername(username).orElseThrow(() -> new IllegalArgumentException("No found username: " + username));
    }

    @Override
    public void updateClient(Client client) {
        client.setRoles(Collections.singleton(Client.ROLE_CLIENT));
        if (client.getContactDetails() != null) {
            client.getContactDetails().setId(client.getId());
        }
        Client clientFromDB = getClientById(client.getId());
        if (!client.getPassword().equals(clientFromDB.getPassword())) {
            client.setPassword(passwordEncoder.encode(client.getPassword()));
        }
        if (!clientFromDB.equals(client)) {
            clientRepository.save(client);
        }
    }

    @Override
    public void removeClientById(Long id) {
        clientRepository.deleteById(id);
    }

    @Override
    public Iterable<Client> getClients() {
        return clientRepository.findAll();
    }

    @Override
    public boolean existsClientByNameAndSurnameAndUsername(String name, String surname, String username) {
        return clientRepository.existsClientByNameAndSurnameAndUsername(name, surname, username);
    }

    @Override
    public boolean existsUsername(String username) {
        return clientRepository.existsClientByUsername(username);
    }

    @Override
    public Long getClientIdByUsernameAndPassword(String username, String password) {
        return clientRepository.getClientIdByUsernameAndPassword(username, password);
    }

    @Override
    public Long authentication(Authentication authentication) {
        if (authentication != null) {
            UserDetails userDetails = (UserDetails) authentication.getPrincipal();
            return clientRepository.getClientIdByUsernameAndPassword(userDetails.getUsername(), userDetails.getPassword());
        }
        return null;
    }

    @Override
    public boolean changePassword(Authentication authentication, PasswordDTO passwordDTO) {
        Client client = getClientById(authentication(authentication));
        if (passwordEncoder.matches(passwordDTO.getOldPassword(), client.getPassword())) {
            client.setPassword(passwordEncoder.encode(passwordDTO.getNewPassword()));
            clientRepository.save(client);
            authentication = new UsernamePasswordAuthenticationToken(client, null, client.getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return true;
        }
        return false;
    }
}
