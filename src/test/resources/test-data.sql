INSERT INTO car (brand, model, bodyType, carStatus, dailyFee, yearOfProduction, mileage, color)
VALUES('Opel', 'Corsa', 'HATCHBACK', 'AVAILABLE', 80.0, 2015, 50000, 'green'),
      ('Nissan', 'Juke', 'SUV', 'AVAILABLE', 180.0, 2017, 30000, 'black'),
      ('Mazda', '2', 'HATCHBACK', 'AVAILABLE', 150.0, 2016, 10600, 'red'),
      ('Toyota', 'Avensis', 'ESTATE_WAGON', 'UNAVAILABLE', 150.0, 2018, 14000, 'blue'),
      ('Kia', 'Ceed', 'HATCHBACK', 'AVAILABLE', 150.0, 2017, 25050, 'white'),
      ('Ford', 'Focus', 'MINIVAN', 'AVAILABLE', 200.0, 2015, 32500, 'red'),
      ('Volvo', 'XC90', 'OFF_ROAD', 'AVAILABLE', 250.0, 2018, 8000, 'red'),
      ('Peugeot', 'RCZ', 'COUPE', 'UNAVAILABLE', 150.0, 2015, 31000,'white'),
      ('Fiat', '500', 'HATCHBACK', 'AVAILABLE', 200.0, 2019, 5000,'yellow'),
      ('Mazda', '5', 'MINIVAN', 'AVAILABLE', 200.0, 2015, 32500,'red');

INSERT INTO client(name, surname, username, password)
VALUES('John', 'Grey', 'john-grey', 'password'),
      ('Ann', 'Miracle', 'miracle', 'password'),
      ('Lisa', 'Flores', 'lisa_flores', 'password'),
      ('Mark', 'Snow', 'snow_funkmaster', 'password'),
      ('Louis', 'McQuinn', 'lmcquinn', 'password');

INSERT INTO client_contact_details(country, city, street, addressNumber, zipCode, email, landlinePhoneNumber, mobilePhoneNumber)
VALUES('England', 'Birmingham', 'Solihull Road', '1ab', 'B11 3AD', 'john-grey@gmail.com', '+44111111111', '+441111111111'),
('England', 'Bristol', 'Bristol Drive', '10a', 'CH66 2HR', 'miracle@yahoo.com', '+44222222222', '+442222222222'),
('England', 'Southampton', 'Clovelly Road', '101d', 'SO14 0AT', 'lisa_flores@gmail.com', '+44333333333', '+443333333333'),
('England', 'Plymouth', 'Cornwall Street', '45abc', 'PL1 1LP', 'snow_funkmaster@yahoo.com', '+44444444444', '+444444444444'),
('Wales', 'Cardiff', 'Ferntree Drive', '12b', 'CF3 0AA', 'lmcquinn@gmail.com', '+44555555555', '+445555555555');

INSERT INTO employee(name, surname, username, password, jobPosition)
VALUES('Ann', 'Smith', 'asmith', 'Password111', 'EMPLOYEE'),
      ('Mark', 'Washington', 'mwashington', 'Password222', 'EMPLOYEE'),
      ('Cathy', 'Summer', 'csummer', 'Password333', 'MANAGER');

INSERT INTO employee_contact_details(country, city, street, addressNumber, zipCode, email, landlinePhoneNumber, serviceMobilePhoneNumber, privateMobilePhoneNumber)
VALUES('England', 'Manchester', 'Bradley Street', '15c', 'M1 1EH', 'asmith@xxx.com', '+44666111111', '+447771111111', '+446666666666'),
('England', 'Leeds', 'East Parade', '25abc', 'LS1 2BH', 'mwashington@xxx.com', '+44666111112', '+447772222222', '+447777777777'),
('England', 'Liverpool', 'Brick Street', '105f', 'L1 0BN', 'csummer@xxx.com', '+44666111113', '+447773333333', '+448888888888');

INSERT INTO location(cost, costIdentifier, description, distance, name)
VALUES (2.76,'LOCATION','Silesia City Center - Poland, Katowice, Chorzowska 107, 40-101',3.3,'Silesia City Center'),
(28.00,'LOCATION','International Airport Katowice in Pyrzowice - Poland, Ożarowice, Wolności 90, 42-625',33.5,'International Airport Katowice in Pyrzowice'),
(58.59,'LOCATION','John Paul II International Airport Krakow-Balice Ltd. - Poland, Balice, Kapitana Mieczysława Medweckiego 1, 32-083',70.1,'John Paul II International Airport Krakow-Balice Ltd.'),
(244.05,'LOCATION','CHOPIN Airport - Poland, Warszawa, Żwirki i Wigury 1, 00-906',292,'CHOPIN Airport'),
(3.34,'LOCATION','Katowice Railway Station - Poland, Katowice, Plac Marii i Lecha Kaczyńskich 2, 40-098',4,'Katowice Railway Station'),
(244.89,'LOCATION','Central Warsaw Railway Station - Poland, Warszawa, Aleje Jerozolimskie 54, 00-024',293,'Central Warsaw Railway Station'),
(249.9,'LOCATION','East Warsaw Railway Station - Poland, Warszawa, Kijowska 20, 03-743',299,'East Warsaw Railway Station'),
(243.22,'LOCATION','West Warsaw Railway Station - Poland, Warszawa, Aleje Jerozolimskie 142, 02-305',291,'West Warsaw Railway Station'),
(24.74,'LOCATION','Gliwice Railway Station - Poland, Gliwice, Bohaterów Getta Warszawskiego 12, 44-100',29.6,'Gliwice Railway Station'),
(84.00,'LOCATION','Krakow Railway Station Main Station - POLSKA, Krakow, PAWIA 5A, 31-155',100.5,'Krakow Railway Station Main Station'),
(168.00,'LOCATION','Wrocław Railway Station Main Station - Poland, Wrocław, Piłsudskiego 105, 50-085',201,'Wrocław Railway Station Main Station'),
(9.61,'LOCATION','Railway Station Sosnowiec - Poland, Sosnowiec, 3 Maja 16, 40-001',11.5,'Railway Station Sosnowiec'),
(60.68,'LOCATION','Railway Station Częstochowa - Poland, Częstochowa, Aleja Wolności 21, 42-200',72.6,'Railway Station Częstochowa');

INSERT INTO location_contact_details (addressNumber, city, country, landlinePhoneNumber, street, zipCode)
VALUES ('107','Katowice','Polska','+48326050000','Chorzowska','40-101'),
('90','Ożarowice','Polska','+48323927000','Wolności','42-625'),
('1','Balice','Polska','+48122955800','Kapitana Mieczysława Medweckiego','32-083'),
('1','Warszawa','Polska','+48226504220','Żwirki i Wigury','00-906'),
('2','Katowice','Polska','+48322593808','Plac Marii i Lecha Kaczyńskich','40-098'),
('54','Warszawa','Polska','+48000000000','Aleje Jerozolimskie','00-024'),
('20','Warszawa','Polska','+48000000000','Kijowska','03-743'),
('142','Warszawa','Polska','+48000000000','Aleje Jerozolimskie','02-305'),
('12','Gliwice','Polska','+48000000000','Bohaterów Getta Warszawskiego','44-100'),
('5A','Krakow','Polska','+48111111111','PAWIA','31-155'),
('105','Wrocław','Polska','+48000000000','Piłsudskiego','50-085'),
('16','Sosnowiec','Polska','+48000000000','3 Maja','40-001'),
('21','Częstochowa','Polska','+48000000000','Aleja Wolności','42-200');