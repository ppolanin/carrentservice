DROP TABLE IF EXISTS car;
DROP TABLE IF EXISTS client;
DROP TABLE IF EXISTS client_contact_details;
DROP TABLE IF EXISTS client_roles;
DROP TABLE IF EXISTS employee;
DROP TABLE IF EXISTS employee_contact_details;
DROP TABLE IF EXISTS employee_roles;
DROP TABLE IF EXISTS reservation;
DROP TABLE IF EXISTS rental;
DROP TABLE IF EXISTS return;
DROP TABLE IF EXISTS location;
DROP TABLE IF EXISTS location_contact_details;

CREATE TABLE car (
  id               BIGINT NOT NULL AUTO_INCREMENT,
  brand            VARCHAR(255) NOT NULL,
  model            VARCHAR(255) NOT NULL,
  bodyType         VARCHAR(255) NOT NULL,
  carStatus        VARCHAR(255) NOT NULL,
  dailyFee         DOUBLE NOT NULL,
  yearOfProduction INT NOT NULL,
  mileage          INT NOT NULL,
  color            VARCHAR(255) NOT NULL,
  PRIMARY KEY(id)
);

CREATE TABLE client (
  id BIGINT NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  surname VARCHAR(255) NOT NULL,
  username VARCHAR(255) NOT NULL,
  password VARCHAR(255) NOT NULL,
  PRIMARY KEY(id)
);

CREATE TABLE client_contact_details (
  id BIGINT NOT NULL AUTO_INCREMENT,
  country VARCHAR(255),
  city VARCHAR(255),
  street VARCHAR(255),
  addressNumber VARCHAR(255),
  zipCode VARCHAR(255),
  email VARCHAR(255),
  landlinePhoneNumber VARCHAR(255),
  mobilePhoneNumber VARCHAR(255),
  PRIMARY KEY (id)
);

CREATE TABLE client_roles(
  client_id BIGINT NOT NULL,
  roles     VARCHAR(255)
);

CREATE TABLE employee (
  id BIGINT NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  surname VARCHAR(255) NOT NULL,
  username VARCHAR(255) NOT NULL,
  password VARCHAR(255) NOT NULL,
  jobPosition VARCHAR(255) NOT NULL,
  PRIMARY KEY(id)
);

CREATE TABLE employee_contact_details (
  id BIGINT NOT NULL AUTO_INCREMENT,
  country VARCHAR(255),
  city VARCHAR(255),
  street VARCHAR(255),
  addressNumber VARCHAR(255),
  zipCode VARCHAR(255),
  email VARCHAR(255),
  landlinePhoneNumber VARCHAR(255),
  privateMobilePhoneNumber VARCHAR(255),
  serviceMobilePhoneNumber VARCHAR(255),
  PRIMARY KEY (id)
);

CREATE TABLE employee_roles(
  employee_id BIGINT NOT NULL,
  roles       VARCHAR(255)
);

CREATE TABLE reservation (
  id BIGINT NOT NULL AUTO_INCREMENT,
  car_id BIGINT NOT NULL,
  client_id BIGINT NOT NULL,
  reservationDate DATETIME NOT NULL,
  reservationFromDate DATETIME NOT NULL,
  reservationToDate DATETIME NOT NULL,
  p_car_location_id BIGINT NOT NULL,
  r_car_location_id BIGINT NOT NULL,
  carAccessibilityDate DATETIME NOT NULL,
  cost DOUBLE NOT NULL,
  costs DOUBLE NOT NULL,
  status VARCHAR(255) NOT NULL,
  PRIMARY KEY(id)
);

CREATE TABLE rental (
  id BIGINT NOT NULL,
  rental_date DATETIME NOT NULL,
  employee_id BIGINT NOT NULL,
  remark VARCHAR(255) NOT NULL,
  status VARCHAR(255) NOT NULL
);

CREATE TABLE return (
  id BIGINT NOT NULL,
  return_date DATETIME NOT NULL,
  employee_id BIGINT NOT NULL,
  total_fee DOUBLE NOT NULL,
  remark VARCHAR(255) NOT NULL
);

CREATE TABLE location (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) DEFAULT NULL,
  `description` VARCHAR(255) DEFAULT NULL,
  `distance` DOUBLE NOT NULL,
  `cost` DOUBLE NOT NULL,
  `costIdentifier` VARCHAR(255) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE location_contact_details (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `country` VARCHAR(255) DEFAULT NULL,
  `city` VARCHAR(255) DEFAULT NULL,
  `street` VARCHAR(255) DEFAULT NULL,
  `addressNumber` VARCHAR(255) DEFAULT NULL,
  `zipCode` VARCHAR(255) DEFAULT NULL,
  `landlinePhoneNumber` VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (id)
);
