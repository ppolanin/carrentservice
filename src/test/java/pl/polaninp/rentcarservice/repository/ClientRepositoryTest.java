package pl.polaninp.rentcarservice.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import pl.polaninp.rentcarservice.model.Client;
import pl.polaninp.rentcarservice.model.ClientContactDetails;


import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class ClientRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private ClientRepository clientRepository;

    private String name;
    private String surname;
    private String username;
    private String password;

    @Before
    public void setup() {
        name = "John";
        surname = "Grey";
        username = "john.grey";
        password = "Password_123";

        Client client = new Client(name, surname, username, password);
        ClientContactDetails contactDetails = new ClientContactDetails("England", "Birmingham", "Solihull Road", "1ab", "B11 3AD", "+44111111111", "john-grey@gmail.com", "+441111111111");
        client.setContactDetails(contactDetails);
        testEntityManager.persistAndFlush(client);
    }

    @Test
    public void shouldReturnTrueWhenClientUsernameExists() {
        //When
        boolean result = clientRepository.existsClientByUsername(username);
        //Then
        assertThat(result).isTrue();
    }

    @Test
    public void shouldReturnFalseWhenClientUsernameIsWrong() {
        //Given
        String username = "user";
        //When
        boolean result = clientRepository.existsClientByUsername(username);
        //Then
        assertThat(result).isFalse();
    }

    @Test
    public void shouldReturnFalseWhenClientUsernameIsNull() {
        //When
        boolean result = clientRepository.existsClientByUsername(null);
        //Then
        assertThat(result).isFalse();
    }

    @Test
    public void shouldReturnTrueWhenClientDetailsAreAvailable() {
        //When
        boolean result = clientRepository.existsClientByNameAndSurnameAndUsername(name, surname, username);
        //Then
        assertThat(result).isTrue();
    }

    @Test
    public void shouldReturnFalseWhenClientDetailsAreWrong() {
        //Given
        String name = "name";
        String surname = "surname";
        String username = "username";
        //When
        boolean result = clientRepository.existsClientByNameAndSurnameAndUsername(name, surname, username);
        //Then
        assertThat(result).isFalse();
    }

    @Test
    public void shouldReturnIdWhenUsernameAndPasswordAreValid() {
        //When
        Long clientId = clientRepository.getClientIdByUsernameAndPassword(username, password);
        //Then
        assertThat(clientId).isEqualTo(1);
    }

    @Test
    public void shouldReturnIdWhenUsernameAndPasswordAreInvalid() {
        //When
        Long clientId = clientRepository.getClientIdByUsernameAndPassword("user", "user_password");
        //Then
        assertThat(clientId).isNull();
    }

}
