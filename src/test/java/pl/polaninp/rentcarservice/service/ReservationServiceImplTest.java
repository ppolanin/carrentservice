package pl.polaninp.rentcarservice.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;
import pl.polaninp.rentcarservice.configuration.DbTestUtil;
import pl.polaninp.rentcarservice.configuration.TestPersistenceConfiguration;
import pl.polaninp.rentcarservice.configuration.TestServiceBeanConfiguration;
import pl.polaninp.rentcarservice.model.Reservation;
import pl.polaninp.rentcarservice.model.ReservationStatus;

import javax.sql.DataSource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = {TestServiceBeanConfiguration.class, TestPersistenceConfiguration.class, DbTestUtil.class})
@ActiveProfiles("dev")
public class ReservationServiceImplTest {

    @Autowired
    DataSource dataSource;

    @Autowired
    ReservationService reservationService;
    private final DateTimeFormatter dateTimeFormatter =  DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");

    @Before
    public void setup() {
        DbTestUtil.resetAutoIncrementColumn(dataSource,   "reservation");
    }

    @Test
    @Transactional
    public void addReservationTest() {
        //Given
        Long carId = 9L;
        Long clientId = 1L;
        Long pickupCarLocationId = 1L;
        Long returnCarLocationId = 5L;
        String startReservationDate = "30.04.2020 12:00";
        String endReservationDate = "03.05.2020 12:00";
        //When
        reservationService.createReservation(carId, clientId, startReservationDate, pickupCarLocationId, endReservationDate, returnCarLocationId);
        Reservation reservation = reservationService.getReservationById(1L);
        //Then
        assertThat(reservation.getId()).isEqualTo(1L);
        assertThat(reservation.getReservationDate()).isEqualToIgnoringSeconds(LocalDateTime.now().truncatedTo(ChronoUnit.MINUTES));
        assertThat(reservation.getReservationFromDate()).isEqualTo(LocalDateTime.of(2020, 4, 30, 12, 0));
        assertThat(reservation.getReservationToDate()).isEqualTo(LocalDateTime.of(2020, 5, 3, 12, 0));
        assertThat(reservation.getCarAccessibilityDate()).isEqualTo(LocalDateTime.of(2020, 5, 4, 11, 59));
        assertThat(reservation.getDays()).isEqualTo(3);
        assertThat(reservation.getClient().getId()).isEqualTo(clientId);
        assertThat(reservation.getCar().getId()).isEqualTo(carId);
        assertThat(reservation.getCost()).isEqualTo(reservation.getCar().getDailyFee() * 3);
    }

    @Test
    @Transactional
    public void shouldReturnTrueWhenClientCancelReservationAtLeastThreeDaysBeforeRentalACar() {
        //Given
        LocalDateTime startReservationDate = LocalDateTime.now().plusDays(3);
        LocalDateTime endReservationDate = startReservationDate.plusDays(7);
        Long pickupCarLocationId = 4L;
        Long returnCarLocationId = 6L;
        reservationService.createReservation(4L, 5L, startReservationDate.format(dateTimeFormatter), pickupCarLocationId, endReservationDate.format(dateTimeFormatter), returnCarLocationId);
        //When
        Reservation reservation = reservationService.getReservationById(1L);
        boolean result = reservationService.cancelReservationByClient(reservation.getId());
        //Then
        assertThat(result).isTrue();
        assertThat(reservation.getStatus()).isEqualTo(ReservationStatus.CANCELLED);
    }

    @Test
    @Transactional
    public void shouldReturnFalseWhenClientCancelReservationInADayOfRentalACar() {
        //Given
        LocalDateTime startReservationDate = LocalDateTime.now().plusHours(5);
        LocalDateTime endReservationDate = startReservationDate.plusDays(5);
        Long pickupCarLocationId = 4L;
        Long returnCarLocationId = 6L;
        reservationService.createReservation(6L, 4L, startReservationDate.format(dateTimeFormatter), pickupCarLocationId, endReservationDate.format(dateTimeFormatter), returnCarLocationId);
        //When
        Reservation reservation = reservationService.getReservationById(1L);
        //Then
        boolean result = reservationService.cancelReservationByClient(reservation.getId());
        assertThat(result).isFalse();
        assertThat(reservation.getStatus()).isEqualTo(ReservationStatus.NEW);
    }

}
