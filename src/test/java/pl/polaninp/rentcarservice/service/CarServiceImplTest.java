package pl.polaninp.rentcarservice.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;
import pl.polaninp.rentcarservice.configuration.TestPersistenceConfiguration;
import pl.polaninp.rentcarservice.configuration.TestServiceBeanConfiguration;
import pl.polaninp.rentcarservice.model.*;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = {TestPersistenceConfiguration.class, TestServiceBeanConfiguration.class})
@ActiveProfiles("dev")
public class CarServiceImplTest {

    @Autowired
    private CarService carService;
    @Autowired
    private ReservationService reservationService;
    @Autowired
    private ClientService clientService;
    @Autowired
    private LocationService locationService;

    @Test
    @Transactional
    public void getAllCarsTest() {
        //When
        Iterable<Car> allCars = carService.getAllCars();
        //Then
        assertThat(allCars).isNotNull();
        assertThat(allCars).hasSize(10);
    }

    @Test
    @Transactional
    public void addCarTest() {
        //Given
        Car car = new Car("Ford", "Mondeo", CarBodyType.ESTATE_WAGON, 2018, "black", 2000, CarStatus.AVAILABLE, 150);
        //When
        carService.addCar(car);
        Iterable<Car> allCars = carService.getAllCars();
        //Then
        assertThat(allCars).isNotNull();
        assertThat(allCars).hasSize(11);
        assertThat(allCars).contains(car);
    }

    @Test
    @Transactional
    public void shouldReturnAllAvailableCarsWhenSearchingPeriodIsDayAfterBookingDate() {
        //Given
        String startReservationDate = "21.04.2020 08:00";
        String endReservationDate = "26.04.2020 08:00";
        String searchingStartDate = "27.04.2020 08:00";
        String searchingEndDate = "30.04.2020 08:00";
        Long clientId = 3L;
        Long carId = 3L;
        Long pickupCarLocationId = 4L;
        Long returnCarLocationId = 6L;
        //When
        Car car1 = carService.getCarById(1L);
        Car car2 = carService.getCarById(2L);
        Car car3 = carService.getCarById(3L);
        Car car5 = carService.getCarById(5L);
        Car car6 = carService.getCarById(6L);
        Car car7 = carService.getCarById(7L);
        Car car9 = carService.getCarById(9L);
        Car car10 = carService.getCarById(10L);
        reservationService.createReservation(carId, clientId, startReservationDate, pickupCarLocationId, endReservationDate, returnCarLocationId);
        Iterable<Car> availableCarsByBookingPeriod = carService.getAvailableCarsByBookingPeriod(searchingStartDate, searchingEndDate);
        //Then
        assertThat(availableCarsByBookingPeriod).isNotNull();
        assertThat(availableCarsByBookingPeriod).hasSize(8);
        assertThat(availableCarsByBookingPeriod).contains(car1, car2, car3, car5, car6, car7, car9, car10);
    }

    @Test
    @Transactional
    public void shouldReturnAllAvailableCarsWhenSearchingPeriodIsBeforeBookingDate() {
        //Given
        String startReservationDate = "21.04.2020 10:00";
        String endReservationDate = "27.04.2020 10:00";
        String searchingStartDate = "18.04.2020 08:00";
        String searchingEndDate = "21.04.2020 09:59";
        Long clientId = 2L;
        Long carId = 6L;
        Long pickupCarLocationId = 1L;
        Long returnCarLocationId = 5L;
        //When
        Car car1 = carService.getCarById(1L);
        Car car2 = carService.getCarById(2L);
        Car car3 = carService.getCarById(3L);
        Car car5 = carService.getCarById(5L);
        Car car6 = carService.getCarById(6L);
        Car car7 = carService.getCarById(7L);
        Car car9 = carService.getCarById(9L);
        Car car10 = carService.getCarById(10L);
        reservationService.createReservation(carId, clientId, startReservationDate, pickupCarLocationId, endReservationDate, returnCarLocationId);
        Iterable<Car> availableCarsByBookingPeriod = carService.getAvailableCarsByBookingPeriod(searchingStartDate, searchingEndDate);
        //Then
        assertThat(availableCarsByBookingPeriod).isNotNull();
        assertThat(availableCarsByBookingPeriod).hasSize(8);
        assertThat(availableCarsByBookingPeriod).contains(car1, car2, car3, car5, car6, car7, car9, car10);
    }

    @Test
    @Transactional
    public void shouldReturnAvailableCarsWithoutReservedOneWhenSearchingPeriodIsTheSameAsBookingDate() {
        //Given
        String startReservationDate = "01.05.2020 08:00";
        String endReservationDate = "08.05.2020 08:00";
        Long clientId = 2L;
        Long carId = 6L;
        Long pickupCarLocationId = 3L;
        Long returnCarLocationId = 2L;
        //When
        Car car1 = carService.getCarById(1L);
        Car car2 = carService.getCarById(2L);
        Car car3 = carService.getCarById(3L);
        Car car5 = carService.getCarById(5L);
        Car car6 = carService.getCarById(6L);
        Car car7 = carService.getCarById(7L);
        Car car9 = carService.getCarById(9L);
        Car car10 = carService.getCarById(10L);
        reservationService.createReservation(carId, clientId, startReservationDate, pickupCarLocationId, endReservationDate, returnCarLocationId);
        Iterable<Car> availableCarsByBookingPeriod = carService.getAvailableCarsByBookingPeriod(startReservationDate, endReservationDate);
        //Then
        assertThat(availableCarsByBookingPeriod).isNotNull();
        assertThat(availableCarsByBookingPeriod).hasSize(7);
        assertThat(availableCarsByBookingPeriod).contains(car1, car2, car3, car5, car7, car9, car10);
        assertThat(availableCarsByBookingPeriod).doesNotContain(car6);
    }


    @Test
    @Transactional
    public void shouldReturnLimitedNumberOfAvailableCarsAfterMultiReservations() {
        //Given
        String startReservationDate1 = "21.04.2020 08:00";
        String endReservationDate1 = "22.04.2020 23:59";
        String startReservationDate2 = "15.05.2020 10:00";
        String endReservationDate2 = "20.05.2020 10:00";
        String startReservationDate3 = "01.06.2020 12:00";
        String endReservationDate3 = "11.06.2020 12:00";
        String startReservationDate4 = "05.06.2020 08:00";
        String endReservationDate4 = "08.06.2020 09:00";
        Long client1Id = 1L;
        Long client2Id = 2L;
        Long client3Id = 3L;
        Long reservedCar1Id = 1L;
        Long reservedCar2Id = 2L;
        Long reservedCar3Id = 3L;
        Long reservedCar4Id = 10L;
        Long pickupCarLocationId1 = 1L;
        Long returnCarLocationId1 = 5L;
        Long pickupCarLocationId2 = 4L;
        Long returnCarLocationId2 = 8L;
        Long pickupCarLocationId3 = 2L;
        Long returnCarLocationId3 = 3L;
        Long pickupCarLocationId4 = 5L;
        Long returnCarLocationId4 = 12L;
        //When
        Car reservedCar1 = carService.getCarById(reservedCar1Id);
        Car reservedCar2 = carService.getCarById(reservedCar2Id);
        Car reservedCar3 = carService.getCarById(reservedCar3Id);
        Car reservedCar4 = carService.getCarById(reservedCar4Id);
        Car availableCar1 = carService.getCarById(5L);
        Car availableCar2 = carService.getCarById(6L);
        Car availableCar3 = carService.getCarById(7L);
        Car availableCar4 = carService.getCarById(9L);
        reservationService.createReservation(reservedCar1Id, client1Id, startReservationDate1, pickupCarLocationId1, endReservationDate1, returnCarLocationId1);
        reservationService.createReservation(reservedCar2Id, client2Id, startReservationDate2, pickupCarLocationId2, endReservationDate2, returnCarLocationId2);
        reservationService.createReservation(reservedCar3Id, client3Id, startReservationDate3, pickupCarLocationId3, endReservationDate3, returnCarLocationId3);
        reservationService.createReservation(reservedCar4Id, client1Id, startReservationDate4, pickupCarLocationId4, endReservationDate4, returnCarLocationId4);
        Iterable<Car> availableCarsByBookingPeriod = carService.getAvailableCarsByBookingPeriod(startReservationDate1, endReservationDate4);
        //Then
        assertThat(availableCarsByBookingPeriod).isNotNull();
        assertThat(availableCarsByBookingPeriod).hasSize(4);
        assertThat(availableCarsByBookingPeriod).doesNotContain(reservedCar1, reservedCar2, reservedCar3, reservedCar4);
        assertThat(availableCarsByBookingPeriod).contains(availableCar1, availableCar2, availableCar3, availableCar4);
    }

    @Test
    @Transactional
    public void shouldReturnAvailableCarsByBookingPeriodAndBrand() {
        //Given
        String carBrand = "Mazda";
        String startReservationDate = "01.05.2020 10:00";
        String endReservationDate = "07.05.2020 10:00";
        String searchingStartDate = "04.05.2020 08:00";
        String searchingEndDate = "06.05.2020 08:00";
        Car mazda2 = carService.getCarById(3L);
        Car mazda5 = carService.getCarById(10L);
        Long clientId = 5L;
        Long pickupCarLocationId = 2L;
        Long returnCarLocationId = 2L;
        //When
        reservationService.createReservation(mazda5.getId(), clientId, startReservationDate, pickupCarLocationId, endReservationDate, returnCarLocationId);
        Iterable<Car> availableCarsByBookingPeriodAndBrand = carService.getAvailableCarsByBookingPeriodAndBrand(searchingStartDate, searchingEndDate, carBrand);
        //Then
        assertThat(availableCarsByBookingPeriodAndBrand).isNotNull();
        assertThat(availableCarsByBookingPeriodAndBrand).hasSize(1);
        assertThat(availableCarsByBookingPeriodAndBrand).contains(mazda2);
    }

    @Test
    @Transactional
    public void shouldReturnAvailableCarsByBookingPeriodAndBodyType() {
        //Given
        String startReservationDate = "01.05.2020 10:00";
        String endReservationDate = "07.05.2020 10:00";
        String searchingStartDate = "04.05.2020 08:00";
        String searchingEndDate = "06.05.2020 08:00";
        Car reservedCar = carService.getCarById(5L);
        Car availableHatchBackCar1 = carService.getCarById(1L);
        Car availableHatchBackCar2 = carService.getCarById(3L);
        Car availableHatchBackCar3 = carService.getCarById(9L);
        Long clientId = 4L;
        Long pickupCarLocationId = 5L;
        Long returnCarLocationId = 12L;
        //When
        reservationService.createReservation(reservedCar.getId(), clientId, startReservationDate, pickupCarLocationId, endReservationDate, returnCarLocationId);
        Iterable<Car> availableCarsByBookingPeriodAndBodyType = carService.getAvailableCarsByBookingPeriodAndBodyType(searchingStartDate, searchingEndDate, CarBodyType.HATCHBACK);
        //Then
        assertThat(availableCarsByBookingPeriodAndBodyType).isNotNull();
        assertThat(availableCarsByBookingPeriodAndBodyType).hasSize(3);
        assertThat(availableCarsByBookingPeriodAndBodyType).contains(availableHatchBackCar1, availableHatchBackCar2, availableHatchBackCar3);
        assertThat(availableCarsByBookingPeriodAndBodyType).doesNotContain(reservedCar);
    }

    @Test
    @Transactional
    public void shouldReturnAvailableCarsByBookingPeriodAndBrandAndBodyType() {
        //Given
        String startReservationDate = "01.05.2020 10:00";
        String endReservationDate = "05.05.2020 10:00";
        String searchingStartDate = "02.05.2020 08:00";
        String searchingEndDate = "05.05.2020 10:00";
        String carBrand = "MAZDA";
        Car mazda5 = carService.getCarById(10L);
        Car mazda2 = carService.getCarById(3L);
        Long clientId = 3L;
        Long pickupCarLocationId = 3L;
        Long returnCarLocationId = 4L;
        //When
        reservationService.createReservation(mazda5.getId(), clientId, startReservationDate, pickupCarLocationId, endReservationDate, returnCarLocationId);
        Iterable<Car> availableCarsByBookingPeriodAndBrandAndBodyType = carService.getAvailableCarsByBookingPeriodAndBrandAndBodyType(searchingStartDate, searchingEndDate, carBrand, CarBodyType.HATCHBACK);
        //Then
        assertThat(availableCarsByBookingPeriodAndBrandAndBodyType).isNotNull();
        assertThat(availableCarsByBookingPeriodAndBrandAndBodyType).hasSize(1);
        assertThat(availableCarsByBookingPeriodAndBrandAndBodyType).containsOnly(mazda2);
    }

}
