package pl.polaninp.rentcarservice.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import pl.polaninp.rentcarservice.configuration.SecurityConfiguration;
import pl.polaninp.rentcarservice.configuration.TestControllerConfiguration;
import pl.polaninp.rentcarservice.configuration.TestPersistenceConfiguration;
import pl.polaninp.rentcarservice.configuration.TestServiceBeanConfiguration;
import pl.polaninp.rentcarservice.model.Employee;
import pl.polaninp.rentcarservice.model.JobPosition;
import pl.polaninp.rentcarservice.service.EmployeeService;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {TestPersistenceConfiguration.class, TestServiceBeanConfiguration.class, SecurityConfiguration.class, TestControllerConfiguration.class})
@ActiveProfiles("dev")
public class EmployeeControllerTest {

    @Autowired
    FilterChainProxy springSecurityFilterChain;
    @Autowired
    private EmployeeService employeeService;
    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(new EmployeeController(employeeService))
                .apply(SecurityMockMvcConfigurers.springSecurity(springSecurityFilterChain))
                .build();
    }

    @Test
    @Transactional
    @WithMockUser(username = "Admin", password = "Admin", roles = "ADMIN")
    public void shouldRedirectToEmployeesViewWhenEmployeeDataAreValid() throws Exception {
        mockMvc.perform(post("/admin/employee/add")
                .param("name", "Joe")
                .param("surname", "Strong")
                .param("password", "Password123")
                .param("jobPosition", "EMPLOYEE"))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(model().attributeExists("employees"))
                .andExpect(model().hasNoErrors())
                .andExpect(view().name("redirect:/admin/employees-panel"));
    }

    @Test
    @Transactional
    @WithMockUser(username = "Admin", password = "Admin", roles = "ADMIN")
    public void shouldReturnEmployeeFormViewWhenEmployeeDataHasErrors() throws Exception {
        mockMvc.perform(post("/admin/employee/add")
                .param("name", "Joe1")
                .param("surname", "Str!ong")
                .param("password", "password")
                .param("jobPosition", "EMPLOYEE"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("employee"))
                .andExpect(model().attributeHasFieldErrorCode("employee", "name", "Pattern"))
                .andExpect(model().attributeHasFieldErrorCode("employee", "surname", "Pattern"))
                .andExpect(model().attributeHasFieldErrorCode("employee", "password", "Pattern"))
                .andExpect(model().errorCount(3))
                .andExpect(view().name("admin_add-employee"));
    }

    @Test
    @WithMockUser(username = "Admin", password = "Admin", roles = "ADMIN")
    public void shouldReturnEmployeesInJsonFormat() throws Exception {
        Employee employeeWithIdEquals1 = employeeService.getEmployeeById(1L);
        Employee employeeWithIdEquals3 = employeeService.getEmployeeById(3L);
        mockMvc.perform(get("/admin/employee/all").contentType(MediaType.APPLICATION_JSON_UTF8))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].id", is(employeeWithIdEquals1.getId().intValue())))
                .andExpect(jsonPath("$[0].name", is(employeeWithIdEquals1.getName())))
                .andExpect(jsonPath("$[0].surname", is(employeeWithIdEquals1.getSurname())))
                .andExpect(jsonPath("$[0].username", is(employeeWithIdEquals1.getUsername())))
                .andExpect(jsonPath("$[0].jobPosition", is(employeeWithIdEquals1.getJobPosition().getPosition())))
                .andExpect(jsonPath("$[2].id", is(employeeWithIdEquals3.getId().intValue())))
                .andExpect(jsonPath("$[2].name", is(employeeWithIdEquals3.getName())))
                .andExpect(jsonPath("$[2].surname", is(employeeWithIdEquals3.getSurname())))
                .andExpect(jsonPath("$[2].username", is(employeeWithIdEquals3.getUsername())))
                .andExpect(jsonPath("$[2].jobPosition", is(employeeWithIdEquals3.getJobPosition().getPosition())));
    }

    @Test
    @WithMockUser(username = "Admin", password = "Admin", roles = "ADMIN")
    public void shouldReturnEmployeeWithSpecificIdToFormView() throws Exception {
        Long employeeId = 2L;
        Employee employee = employeeService.getEmployeeById(employeeId);
        mockMvc.perform(get("/admin/employee/update-form/{id}", employeeId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("employee"))
                .andExpect(model().size(1))
                .andExpect(model().attribute("employee", allOf(
                        hasProperty("id", is(employee.getId())),
                        hasProperty("name", is(employee.getName())),
                        hasProperty("surname", is(employee.getSurname())),
                        hasProperty("username", is(employee.getUsername())),
                        hasProperty("jobPosition", is(employee.getJobPosition())),
                        hasProperty("password", is(employee.getPassword()))
                )))
                .andExpect(view().name("admin_update-employee"));
    }

    @Test
    @Transactional
    @WithMockUser(username = "Admin", password = "Admin", roles = "ADMIN")
    public void shouldReturnEmployeesPanelViewWhenEmployeeAreSuccessfulUpdated() throws Exception {
        Employee employee = employeeService.getEmployeeById(1L);
        employee.setSurname("Smith-Jones");
        employee.setJobPosition(JobPosition.MANAGER);
        mockMvc.perform(post("/admin/employee/update/{id}", 1L)
                .param("id", employee.getId().toString())
                .param("name", employee.getName())
                .param("surname", employee.getSurname())
                .param("username", employee.getUsername())
                .param("jobPosition", employee.getJobPosition().toString())
                .param("password", employee.getPassword()))
                .andDo(print())
                .andExpect(model().attributeExists("employee"))
                .andExpect(status().isFound())
                .andExpect(model().hasNoErrors())
                .andExpect(model().attribute("employee", allOf(
                        hasProperty("id", is(1L)),
                        hasProperty("name", is("Ann")),
                        hasProperty("surname", is("Smith-Jones")),
                        hasProperty("username", is("asmith-jones")),
                        hasProperty("jobPosition", is(JobPosition.MANAGER))
                )))
                .andExpect(view().name("redirect:/admin/employees-panel"));
    }

    @Test
    @Transactional
    @WithMockUser(username = "Admin", password = "Admin", roles = "ADMIN")
    public void shouldReturnEmployeeUpdateFormViewWhenEmployeeDataHasErrors() throws Exception {
        Employee employee = employeeService.getEmployeeById(1L);
        employee.setName(" Ann Marie");
        employee.setSurname("Smith?Jones");
        employee.setPassword("");
        mockMvc.perform(post("/admin/employee/update/{id}", 1L)
                .param("id", employee.getId().toString())
                .param("name", employee.getName())
                .param("surname", employee.getSurname())
                .param("username", employee.getUsername())
                .param("jobPosition", employee.getJobPosition().toString())
                .param("password", employee.getPassword()))
                .andDo(print())
                .andExpect(model().attributeExists("employee"))
                .andExpect(status().isOk())
                .andExpect(model().errorCount(3))
                .andExpect(model().attributeHasFieldErrorCode("employee", "name", "Pattern"))
                .andExpect(model().attributeHasFieldErrorCode("employee", "surname", "Pattern"))
                .andExpect(model().attributeHasFieldErrorCode("employee", "password", "Pattern"))
                .andExpect(view().name("admin_update-employee"));
    }

    @Test
    @Transactional
    @WithMockUser(username = "Admin", password = "Admin", roles = "ADMIN")
    public void shouldReturnPanelViewAfterRemovingEmployeeDataById() throws Exception {
        mockMvc.perform(get("/admin/employee/remove/{id}", 2L))
                .andDo(print())
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/admin/employees-panel"));
    }

    @Test
    @WithUserDetails(value = "mwashington", userDetailsServiceBeanName = "userDetailsServiceImplForEmployees")
    @WithMockUser(username = "Admin", password = "Admin", roles = "ADMIN")
    public void shouldReturnIdWhenEmployeeIsLoggedIn() throws Exception {
        mockMvc.perform(get("/employee/authentication"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string("2"));
    }

}
