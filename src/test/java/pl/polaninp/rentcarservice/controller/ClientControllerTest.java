package pl.polaninp.rentcarservice.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import pl.polaninp.rentcarservice.configuration.SecurityConfiguration;
import pl.polaninp.rentcarservice.configuration.TestControllerConfiguration;
import pl.polaninp.rentcarservice.configuration.TestPersistenceConfiguration;
import pl.polaninp.rentcarservice.configuration.TestServiceBeanConfiguration;
import pl.polaninp.rentcarservice.model.Client;
import pl.polaninp.rentcarservice.service.ClientService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {TestPersistenceConfiguration.class, TestServiceBeanConfiguration.class, SecurityConfiguration.class, TestControllerConfiguration.class})
@ActiveProfiles("dev")
public class ClientControllerTest {

    @Autowired
    FilterChainProxy springSecurityFilterChain;
    private MockMvc mockMvc;
    @Autowired
    private ClientService clientService;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(new ClientController(clientService))
                .apply(SecurityMockMvcConfigurers.springSecurity(springSecurityFilterChain))
                .build();
    }

    @Test
    public void shouldReturnClientWhenIdIsValid() throws Exception {
        Long clientId = 3L;
        Client lisaFlowers = clientService.getClientById(clientId);
        mockMvc.perform(get("/client/{id}", clientId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.id", is(lisaFlowers.getId().intValue())))
                .andExpect(jsonPath("$.name", is(lisaFlowers.getName())))
                .andExpect(jsonPath("$.surname", is(lisaFlowers.getSurname())))
                .andExpect(jsonPath("$.username", is(lisaFlowers.getUsername())));
    }

    @Test
    public void shouldReturnClients() throws Exception {
        Client clientWithIdEqualsOne = clientService.getClientById(1L);
        Client clientWithIdEqualsFive = clientService.getClientById(5L);
        mockMvc.perform(get("/client/all"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(5)))
                .andExpect(jsonPath("$[0].id", is(clientWithIdEqualsOne.getId().intValue())))
                .andExpect(jsonPath("$[0].name", is(clientWithIdEqualsOne.getName())))
                .andExpect(jsonPath("$[0].surname", is(clientWithIdEqualsOne.getSurname())))
                .andExpect(jsonPath("$[0].username", is(clientWithIdEqualsOne.getUsername())))
                .andExpect(jsonPath("$[4].id", is(clientWithIdEqualsFive.getId().intValue())))
                .andExpect(jsonPath("$[4].name", is(clientWithIdEqualsFive.getName())))
                .andExpect(jsonPath("$[4].surname", is(clientWithIdEqualsFive.getSurname())))
                .andExpect(jsonPath("$[4].username", is(clientWithIdEqualsFive.getUsername())));
    }

    @Test
    public void shouldReturnTrueWhenClientDataAreValid() throws Exception {
        mockMvc.perform(get("/client/{name}/{surname}/{username}", "Mark", "Snow", "snow_funkmaster"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string("true"));
    }

    @Test
    public void shouldReturnFalseWhenClientDataAreInvalid() throws Exception {
        mockMvc.perform(get("/client/{name}/{surname}/{username}", "Louis", "Frantic", "frantic-louis"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string("false"));
    }

    @Test
    @WithUserDetails(value = "lmcquinn", userDetailsServiceBeanName = "userDetailsServiceImplForClients")
    public void shouldReturnIdWhenClientExists() throws Exception {
        mockMvc.perform(post("/client/login-authentication"))
                .andExpect(status().isOk())
                .andExpect(content().string("5"));
    }

    @Test
    public void shouldReturnTrueWhenUsernameExists() throws Exception {
        mockMvc.perform(get("/client/register/{username}", "john-grey"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string("true"));
    }

    @Test
    public void shouldReturnFalseWhenUsernameExists() throws Exception {
        mockMvc.perform(get("/client/register/{username}", "user"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string("false"));
    }

    @Test
    @Transactional
    public void shouldReturnCreatedStatusAfterRegisteringClient() throws Exception {
        String clientString = "{\"name\": \"John\", \"surname\": \"Doe\", \"username\": \"johndoe\", \"password\": \"password\", \"contactDetails\": {\"email\": \"john.doe@yahoo.com\"}}";
        mockMvc.perform(post("/client/register").content(clientString).contentType(MediaType.APPLICATION_JSON_UTF8))
                .andDo(print())
                .andExpect(status().isCreated());
    }

}