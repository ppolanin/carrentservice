package pl.polaninp.rentcarservice.model;

import org.junit.Before;
import org.junit.Test;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

public class EmployeeTest {

    private Validator validator;
    private Employee employee;

    @Before
    public void setDefaultValues() {
        employee = new Employee("John", "Doe", JobPosition.MANAGER, "JohnDoe123");
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = validatorFactory.getValidator();
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionWhenJobPositionIsNotValid() {
        employee.setJobPosition(JobPosition.valueOf("INVALID"));
    }

}
