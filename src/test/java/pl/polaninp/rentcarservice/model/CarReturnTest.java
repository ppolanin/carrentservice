package pl.polaninp.rentcarservice.model;

import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

public class CarReturnTest {

    private Car car;
    private Client client;
    private Employee employee;
    private Location pickupCarLocation;
    private Location returnCarLocation;

    @Before
    public void setup() {
        car = new Car("Mazda", "2", CarBodyType.HATCHBACK, 2015, "black", 100);
        client = new Client("John", "Doe", "john_doe", "password");
        employee = new Employee("Mark", "Smith", JobPosition.EMPLOYEE, "password");
        pickupCarLocation = new Location("CHOPIN Airport", 292, LocationType.PICK_UP);
        returnCarLocation = new Location("Central Warsaw Railway Station", 293, LocationType.RETURN);
    }

    @Test
    public void shouldReturnOnlyReservationFeeWhenCarIsReturnOnTime() {
        //Given
        LocalDateTime startReservationDate = LocalDateTime.of(2020, 3, 10, 8, 0);
        LocalDateTime endReservationDate = LocalDateTime.of(2020, 3, 15, 14, 0);
        Reservation carReservation = new Reservation(LocalDateTime.of(2020, 3, 1, 10, 0), client, car, startReservationDate, pickupCarLocation, endReservationDate, returnCarLocation, ReservationStatus.NEW);
        CarRental carRental = new CarRental(startReservationDate, employee, carReservation, "", CarRentalStatus.NEW);
        CarReturn carReturn = new CarReturn(endReservationDate, carRental, employee, "");
        //When
        double totalFee = carReturn.getTotalFee();
        //Then
        assertThat(totalFee).isEqualTo(carReservation.getCosts());
    }

    @Test
    public void shouldReturnProperFeeWhenCarIsReturnAfterReservationDuration() {
        //Given
        LocalDateTime startReservationDate = LocalDateTime.of(2020, 3, 10, 8, 0);
        LocalDateTime endReservationDate = LocalDateTime.of(2020, 3, 15, 14, 0);
        LocalDateTime returnDate = LocalDateTime.of(2020, 3, 18, 10, 0);
        Reservation carReservation = new Reservation(LocalDateTime.of(2020, 3, 1, 10, 0), client, car, startReservationDate, pickupCarLocation, endReservationDate, returnCarLocation, ReservationStatus.NEW);
        CarRental carRental = new CarRental(startReservationDate, employee, carReservation, "", CarRentalStatus.NEW);
        CarReturn carReturn = new CarReturn(returnDate, carRental, employee, "");
        //When
        double totalFee = carReturn.getTotalFee();
        //Then
        assertThat(totalFee).isEqualTo(carReservation.getCosts() + 3 * carReservation.getCar().getDailyFee());
    }

    @Test
    public void shouldReturnProperFeeWhenCarIsReturnDuringReservationDuration() {
        //Given
        LocalDateTime startReservationDate = LocalDateTime.of(2020, 3, 10, 8, 0);
        LocalDateTime endReservationDate = LocalDateTime.of(2020, 3, 17, 14, 0);
        LocalDateTime returnDate = LocalDateTime.of(2020, 3, 12, 14, 0);
        Reservation carReservation = new Reservation(LocalDateTime.of(2020, 2, 15, 19, 0), client, car, startReservationDate, pickupCarLocation, endReservationDate, returnCarLocation, ReservationStatus.NEW);
        CarRental carRental = new CarRental(startReservationDate, employee, carReservation, "", CarRentalStatus.NEW);
        //When
        double totalFee = new CarReturn(returnDate, carRental, employee, "").calculateTotalFee();
        //Then
        assertThat(totalFee).isEqualTo(carReservation.getCosts() - 5 * carReservation.getCar().getDailyFee());
    }

}
