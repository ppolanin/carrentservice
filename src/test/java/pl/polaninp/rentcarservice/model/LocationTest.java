package pl.polaninp.rentcarservice.model;

import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import java.util.Set;

import static org.assertj.core.api.Assertions.*;

public class LocationTest {

    private Validator validator;

    @Before
    public void setup() {
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = validatorFactory.getValidator();
    }

    @Test
    public void shouldReturnCorrectRoundedValueWhenDistanceIsHigherThenZero() {
        //Given
        Location location = new Location("John Paul II International Airport Krakow-Balice Ltd.", 100, LocationType.PICK_UP);
        //Then
        assertThat(location.getCost()).isEqualTo(83.58);
    }

    @Test
    public void shouldReturnCorrectRoundedValueWhenDistanceIsLowerThenZero() {
        //Given
        Location location = new Location("Silesia City Center", -24.56, LocationType.PICK_UP);
        //Then
        assertThat(location.getCost()).isEqualTo(20.53);
    }

    @Test
    public void shouldReturnZeroWhenLocationWasCreatedWithNoArgumentConstructor() {
        //Given
        Location location = new Location();
        //Then
        assertThat(location.getCost()).isEqualTo(0);
    }

    @Test
    public void shouldReturnNameWhenContactDetailsAreNull() {
        //Given
        String name = "Katowice Airport";
        Location centralWarsawRailwayStation = new Location(name, 25, LocationType.RETURN);
        //Then
        assertThat(centralWarsawRailwayStation.getDescription().equals(name));
    }

    @Test
    public void shouldReturnFullDescriptionWhenNameAndContactDetailsAreSet() {
        //Given
        String name = "RailWay Station Central Warsaw";
        Location centralWarsawRailwayStation = new Location(name, 352.5, LocationType.PICK_UP);
        String country = "Poland";
        String city = "Warsaw";
        String street = "Aleje Jerozolimskie";
        String addressNumber = "54";
        String zipCode = "00-024";
        String landlinePhoneNumber = "+48224744086";
        LocationContactDetails centralWarsawRailwayStationContactDetails = new LocationContactDetails(country, city, street, addressNumber, zipCode, landlinePhoneNumber);
        centralWarsawRailwayStation.setContactDetails(centralWarsawRailwayStationContactDetails);
        //Then
        assertThat(centralWarsawRailwayStation.getDescription()).isEqualTo(name + " - " + country + ", " + city + ", " + street + " " + addressNumber + ", " + zipCode);
    }

    @Test
    public void shouldReturnEmptyStringWhenLocationWasCreatedWithNoArgumentConstructor() {
        //Given
        Location location = new Location();
        //Then
        assertThat(location.getDescription()).isEqualTo("");
    }

    @Test
    public void shouldReturnNoErrorsWhenNameHasPolishMarks() {
        //Given
        Location location = new Location("Międzynarodowe Lotnisko Kraków-Balice im. Jana Pawła II", 75.0, LocationType.PICK_UP);
        Set<ConstraintViolation<Location>> locationValidation = validator.validate(location);
        //Then
        assertThat(locationValidation.size()).isEqualTo(0);
    }

    @Test
    public void shouldReturnStringConnectedOnlyWithLocationFieldsWhenContactDetailsAreNull() {
        //Given
        Location location = new Location("Railway Station Cracow", 84.1, LocationType.PICK_UP);
        location.setId(1L);
        //Then
        assertThat(location.toString()).isEqualTo("location [id: 1 name: Railway Station Cracow description: Railway Station Cracow distance: 84.1 cost identifier: LOCATION cost: 70.29 type: PICK_UP]");
    }

    @Test
    public void shouldReturnFullStringWhenContactDetailsAreNotNull() {
        //Given
        Location location = new Location("Railway Station Cracow", 84.1, LocationType.RETURN);
        LocationContactDetails locationContactDetails = new LocationContactDetails("Poland", "Cracow", "Pawia", "5a", "31-154", "+48000000000");
        location.setId(1L);
        locationContactDetails.setId(1L);
        location.setContactDetails(locationContactDetails);
        //Then
        assertThat(location.toString()).isEqualTo("location [id: 1 name: Railway Station Cracow description: Railway Station Cracow - Poland, Cracow, Pawia 5a, 31-154 distance: 84.1 cost identifier: LOCATION cost: 70.29 type: RETURN contact details [id: 1 country: Poland city: Cracow street: Pawia address number: 5a zip code: 31-154 landline phone number: +48000000000]]");
    }

    @Test
    public void shouldReturnStringWithoutLocationTypeWhenContactDetailsAreNotNullAndLocationTypeIsNull() {
        //Given
        Location location = new Location();
        location.setId(1L);
        location.setName("Railway Station Cracow");
        location.setDistance(84.1);
        LocationContactDetails locationContactDetails = new LocationContactDetails("Poland", "Cracow", "Pawia", "5a", "31-154", "+48000000000");
        locationContactDetails.setId(1L);
        location.setContactDetails(locationContactDetails);
        //Then
        assertThat(location.toString()).isEqualTo("location [id: 1 name: Railway Station Cracow description: Railway Station Cracow - Poland, Cracow, Pawia 5a, 31-154 distance: 84.1 cost identifier: LOCATION cost: 70.29 contact details [id: 1 country: Poland city: Cracow street: Pawia address number: 5a zip code: 31-154 landline phone number: +48000000000]]");
    }

    @Test
    public void shouldReturnStringWithoutContactDetailsAndLocationTypeWhenAreBothNull() {
        //Given
        Location location = new Location();
        location.setId(1L);
        location.setName("Railway Station Cracow");
        location.setDistance(84.1);
        //Then
        assertThat(location.toString()).isEqualTo("location [id: 1 name: Railway Station Cracow description: Railway Station Cracow distance: 84.1 cost identifier: LOCATION cost: 70.29]");
    }

    @Test
    public void shouldThrowsIllegalArgumentExceptionWhenLocationTypeIsNull() {
        assertThatIllegalArgumentException().isThrownBy(() -> new Location("Wrocław Railway Station", 201, null)).withMessage("LocationType should have not null value");
    }

    @Test
    public void shouldCloneLocationProperlyWhenLocationTypeOfOriginalIsBeingChanged() throws CloneNotSupportedException {
        //Given
        Location location = new Location("Railway Station Częstochowa", 72.6, LocationType.PICK_UP);
        //When
        Location copiedLocation = (Location) location.clone();
        location.setLocationType(LocationType.RETURN);
        //Then
        assertThat(copiedLocation).isNotEqualTo(location);
        assertThat(copiedLocation.getName()).isEqualTo(location.getName());
        assertThat(copiedLocation.getDistance()).isEqualTo(location.getDistance());
        assertThat(copiedLocation.getCost()).isEqualTo(location.getCost());
        assertThat(copiedLocation.getDescription()).isEqualTo(location.getDescription());
        assertThat(copiedLocation.getDescriptionName("en")).isNotEqualTo(location.getDescriptionName("en"));
        assertThat(copiedLocation.getLocationType()).isNotEqualTo(location.getLocationType());
    }

    @Test
    public void shouldCloneLocationProperlyWhenStateOfOriginalIsBeingChanged() throws CloneNotSupportedException {
        //Given
        Location location = new Location("Railway Station Częstochowa", 72.6, LocationType.PICK_UP);
        //When
        Location copiedLocation = (Location) location.clone();
        location.setName("Railway Station Sosnowiec");
        location.setDistance(11.5);
        location.setLocationType(LocationType.RETURN);
        //Then
        assertThat(copiedLocation).isNotEqualTo(location);
        assertThat(copiedLocation.getName()).isNotEqualTo(location.getName());
        assertThat(copiedLocation.getDistance()).isNotEqualTo(location.getDistance());
        assertThat(copiedLocation.getCost()).isNotEqualTo(location.getCost());
        assertThat(copiedLocation.getDescription()).isNotEqualTo(location.getDescription());
        assertThat(copiedLocation.getDescriptionName("en")).isNotEqualTo(location.getDescriptionName("en"));
        assertThat(copiedLocation.getLocationType()).isNotEqualTo(location.getLocationType());
    }

    @Test
    public void shouldCloneLocationAndContactDetailsProperlyWhenStateOfOriginalsIsBeingChanged() throws CloneNotSupportedException {
        //Given
        Location location = new Location("CHOPIN Airport", 292, LocationType.PICK_UP);
        LocationContactDetails locationContactDetails = new LocationContactDetails("Poland", "Warsaw", "Żwirki i Wigury", "1", "00-906", "+48226504220");
        location.setContactDetails(locationContactDetails);
        //When
        Location clonedLocation = (Location) location.clone();
        location.setName("John Paul II International Airport Krakow-Balice Ltd.");
        location.setDistance(70.1);
        location.setLocationType(LocationType.RETURN);
        location.getContactDetails().setCity("Balice");
        location.getContactDetails().setStreet("Kapitana Mieczysława Medweckiego");
        location.getContactDetails().setAddressNumber("1");
        location.getContactDetails().setZipCode("32-083");
        location.getContactDetails().setLandlinePhoneNumber("+48122955800");
        //Then
        assertThat(location).isNotEqualTo(clonedLocation);
        assertThat(location.getName()).isNotEqualTo(clonedLocation.getName());
        assertThat(location.getDistance()).isNotEqualTo(clonedLocation.getDistance());
        assertThat(location.getDescription()).isNotEqualTo(clonedLocation.getDescription());
        assertThat(location.getDescriptionName("en")).isNotEqualTo(clonedLocation.getDescriptionName("en"));
        assertThat(location.getLocationType()).isNotEqualTo(clonedLocation.getLocationType());
        assertThat(location.getCost()).isNotEqualTo(clonedLocation.getCost());
        assertThat(location.getContactDetails().getCountry()).isEqualTo(clonedLocation.getContactDetails().getCountry());
        assertThat(location.getContactDetails().getCity()).isNotEqualTo(clonedLocation.getContactDetails().getCity());
        assertThat(location.getContactDetails().getStreet()).isNotEqualTo(clonedLocation.getContactDetails().getStreet());
        assertThat(location.getContactDetails().getAddressNumber()).isEqualTo(clonedLocation.getContactDetails().getAddressNumber());
        assertThat(location.getContactDetails().getZipCode()).isNotEqualTo(clonedLocation.getContactDetails().getZipCode());
        assertThat(location.getContactDetails().getLandlinePhoneNumber()).isNotEqualTo(clonedLocation.getContactDetails().getLandlinePhoneNumber());
    }
}
