package pl.polaninp.rentcarservice.model;

import org.junit.Test;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

public class ReservationTest {

    @Test
    public void shouldCostCalculatorReturnProperValueOfCostsWhenCostValuesAreCorrect() {
        //Given
        Car car = new Car("Toyota", "Avensis", CarBodyType.ESTATE_WAGON, 2018, "blue", 14000, CarStatus.AVAILABLE, 150.0);
        Client client = new Client();
        Location pickupCarLocation = new Location("Katowice Railway Station", 3.34, LocationType.PICK_UP);
        Location returnCarLocation = new Location("International Airport Katowice in Pyrzowice", 28, LocationType.RETURN);
        LocalDateTime reservationDate = LocalDateTime.now();
        LocalDateTime startReservationDate = reservationDate.plusDays(5);
        LocalDateTime endReservationDate = startReservationDate.plusDays(5);
        Reservation reservation = new Reservation(reservationDate, client, car, startReservationDate, pickupCarLocation, endReservationDate, returnCarLocation, ReservationStatus.NEW);
        double expectedCosts = Math.round((reservation.getCost() + pickupCarLocation.getCost() + returnCarLocation.getCost()) * 100.0) / 100.0;
        //When
        double testedCosts = reservation.getCostCalculator().sumCosts();
        //Then
        assertThat(testedCosts).isEqualTo(expectedCosts);
    }
}