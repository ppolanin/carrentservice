package pl.polaninp.rentcarservice.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import pl.polaninp.rentcarservice.repository.EmployeeRepository;
import pl.polaninp.rentcarservice.repository.LocationRepository;
import pl.polaninp.rentcarservice.service.*;

@Configuration
@ComponentScan(basePackages = "pl.polaninp.rentcarservice.repository")
public class TestServiceBeanConfiguration {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Bean
    public ClientService createClientService() {
        return new ClientServiceImpl();
    }

    @Bean
    public CarService createCarService() {
        return new CarServiceImpl();
    }

    @Bean
    public EmployeeService createEmployeeService() {
        return new EmployeeServiceImpl(employeeRepository, createPasswordEncoder());
    }

    @Bean
    public ReservationService createReservationService() {
        return new ReservationServiceImpl();
    }

    @Bean
    public LocationService createLocationService() {
        return new LocationServiceImpl(locationRepository);
    }

    @Bean
    public PasswordEncoder createPasswordEncoder() {
        return new BCryptPasswordEncoder(4);
    }
}
