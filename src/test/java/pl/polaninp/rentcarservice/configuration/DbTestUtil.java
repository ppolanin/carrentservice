package pl.polaninp.rentcarservice.configuration;

import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

@Configuration
public class DbTestUtil {

    private static final String RESET_COLUMN_ID_STATEMENT = "ALTER TABLE %s ALTER COLUMN id RESTART WITH 1";

    public DbTestUtil() {
    }

    public static void resetAutoIncrementColumn(DataSource dataSource, String... tableNames) {
        try (Connection connection = dataSource.getConnection()) {
            for (String tableName : tableNames) {
                try (Statement statement = connection.createStatement()) {
                    String sqlResetIdColumn = String.format(RESET_COLUMN_ID_STATEMENT, tableName);
                    statement.execute(sqlResetIdColumn);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
