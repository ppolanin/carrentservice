package pl.polaninp.rentcarservice.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import pl.polaninp.rentcarservice.repository.ClientRepository;
import pl.polaninp.rentcarservice.repository.EmployeeRepository;
import pl.polaninp.rentcarservice.service.UserDetailsServiceImplForClients;
import pl.polaninp.rentcarservice.service.UserDetailsServiceImplForEmployees;

@Configuration
@ComponentScan(basePackages = "pl.polaninp.rentcarservice.repository")
public class TestControllerConfiguration {

    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private EmployeeRepository employeeRepository;

    @Primary
    @Bean(name = "userDetailsServiceImplForClients")
    public UserDetailsServiceImplForClients createUserDetailsServiceForClients() {
        return new UserDetailsServiceImplForClients(clientRepository);
    }

    @Bean(name = "userDetailsServiceImplForEmployees")
    public UserDetailsServiceImplForEmployees createUserDetailsServiceForEmployees() {
        return new UserDetailsServiceImplForEmployees(employeeRepository);
    }
}
